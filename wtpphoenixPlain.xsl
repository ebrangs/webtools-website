<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0">
    <xsl:output omit-xml-declaration="yes" />
    <xsl:template match="html">
        <xsl:apply-templates
            select="body/h1"
            mode="banner" />
        <xsl:apply-templates select="body/*" />
    </xsl:template>
    <xsl:template
        match="h1"
        mode="banner">
    </xsl:template>
    <xsl:template match="h1" />
    <xsl:template match="h2">
        <h2 class="bar">
            <xsl:value-of select="." />
        </h2>
    </xsl:template>
    <xsl:template match="ul">
        <ul class="indent">
            <xsl:apply-templates />
        </ul>
    </xsl:template>
    <xsl:template match="*|@*|text()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()" />
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
