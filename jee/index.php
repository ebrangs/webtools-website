<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle = "WTP Java EE Tools";
$pageKeywords = "Eclipse WTP Java EE IDE";
$pageAuthor ="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$xml = DOMDocument::load('index.xml');

$xpath = new DOMXPath($xml);
$titleNode = $xpath->query("/html/head/title")->item(0);
$pageTitle = ($titleNode != null) ? $titleNode->nodeValue : "eclipse.org webtools page";

$xsl = DOMDocument::load('../wtpnova.xsl');

$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$html = $proc->transformToXML($xml);
$html .= $aboutLinks;
$html .= $wtpTopButtons;

if (file_exists("rightcolumn.xml")) {
	$xml = DOMDocument::load('rightcolumn.xml');
	$xsl = DOMDocument::load('../wtpnovaside.xsl');
	$proc->importStylesheet($xsl);
	$html .= $proc->transformToXml($xml);
	$html .= "<style>#midcolumn{width:685px !important;}</style>";
}

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>