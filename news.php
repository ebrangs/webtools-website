<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle = "Eclipse Web Tools Platform Project: News";
$pageKeywords	= "";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');
require_once ($root . '/webtools/newstohtml.php');

# Generate the web page
// Get the XML news feed as html
$news = news_to_html($root . "/webtools/news.xml", "$pageTitle", "/webtools/news/");

$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
		<table>
			<tr>
				<td width="60%">
					<h1>WTP News</h1>
					<div class="wtpsubtitle">$pageTitle</div>
				</td>
				<td>
					<img src="/webtools/images/wtplogosmall.jpg"
						align="middle" height="129" hspace="50" width="207" alt="WTP Logo" 
						usemap="logomap" />
                    <map
                        id="logomap"
                        name="logomap">
                        <area
                            coords="0,0,207,129"
                            href="/webtools/"
                            alt="WTP Home" />
                    </map>
				</td>
			</tr>
		</table>
		<div class="homeitem3col">
		$news
		</div>
	</div>
</div>

EOHTML;
		$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
		?>
