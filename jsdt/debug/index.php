<?php  				
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	

$App = new App();	
$Nav = new Nav();	
$Menu = new Menu();		

include($App->getProjectCommon());

	#*****************************************************************************
	#
	# Copyright (c) 2010 IBM Corporation and others.
 	# All rights reserved. This program and the accompanying materials
 	# are made available under the terms of the Eclipse Public License v1.0
 	# which accompanies this distribution, and is available at
 	# http://www.eclipse.org/legal/epl-v10.html
 	# Contributors:
	#     IBM Corporation - initial implementation
	#
	# Author: 		Ant Team
	# Date:			August 30, 2010
	#
	# Description: This is the new main page for the JSDT Team website
	#
	#****************************************************************************
	
	$pageTitle 		= "JavaScript Development Tools Debug";
	$pageKeywords	= "javascript, JavaScript, JSDT, platform, debugging, debugger, jsdt, breakpoints, about, content, epl, compiler, compile, parse, parser";
	$pageAuthor		= "JSDT Team";
	
	include("../_sideCommon.php");
	#include("_pluginCommon.php");
	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<div class="homeitem3col">
			<h3>Project Overview</h3>
			<p>
				The JSDT debug project brings a variety of JavaScript debugging support to the JSDT using a common
				UI and communicating using a common protocol - JavaScript Debug Interface (JSDI). The common interface 
				for the debugger emulates that of the <a href="http://www.eclipse.org/eclipse/debug/">platform and JDT debugger</a> to allow a 
				smooth transition and ease of use for the JSDT debugger.
			</p>
			<p>
				Currently we have debugging support for <a href="http://www.mozilla.org/rhino/debugger.html">Rhino</a> and an incubation 
				version of <a href="http://getfirebug.com/wiki/index.php/Crossfire">Crossfire</a> / 
				<a href="http://getfirebug.com/">Firebug</a> support.
			</p>
		</div>
		<div class="homeitem3col">
			<h3>JSDT Debug Bundles</h3>
			<p>
				The JSDT debugger is broken down into the following bundles:
				<ul>
					<li><strong>org.eclipse.wst.jsdt.debug.core</strong> - the JSDI specification and debug model elements.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.chrome (incubation)</strong> - contains a <a href="http://code.google.com/p/chromedevtools/wiki/ChromeDevToolsProtocol">ChromeDevTools</a> implementation of the debugger.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.crossfire (incubation)</strong> - contains a <a href="http://getfirebug.com/wiki/index.php/Crossfire">Crossfire</a> implementation of the debugger.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.rhino</strong> - a <a href="http://www.mozilla.org/rhino/debugger.html">Rhino</a> implementation of the debugger.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.rhino.debugger</strong> - the <a href="http://www.mozilla.org/rhino/debugger.html">Rhino</a> debug hooks to connect to the Rhino interpreter.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.rhino.ui</strong> - the UI presentation for <a href="http://www.mozilla.org/rhino/debugger.html">Rhino</a>.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.transport</strong> - the debug protocol transport classes.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.ui</strong> - contains the shared UI presentation for the debugger implementations.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.core.tests</strong> - contains related test cases for the debug core.</li>
					<li><strong>org.eclipse.wst.jsdt.debug.rhino.tests</strong> - contains <a href="http://www.mozilla.org/rhino/debugger.html">Rhino</a> specific tests.</li>
				</ul>
			</p>
			<p>
				A team project set file for JSDT debug can be found <a href="../psf/jsdt-debug.psf">here</a>.
			</p>
		</div>
		<div class="homeitem3col">
			<h3>Development Resources</h3>
			<p>
				All things related to hacking the code and using the tools can be found in this section.
			</p>
			<h4>Rhino Debug Wire Protocol</h4>
			<p>
				To provide support for the Rhino interpreter, we had to create both sides of the coin - the debugger and the client. To
				facilitate communication between the debugger and the client we created an adapted version of v8 (JSON-based) packet
				communication. The complete specification for the RDWP can be found <strong><a href="https://wiki.eclipse.org/JSDT/Debug/Rhino_Debug_Wire_Protocol">here</a></strong>. 
			</p>
			<h4>JavaScript Debug Interface (JSDI)</h4>
			<p>
				The JSDI is used to provide a common way for our models and UI to communicate with a variety of debugger back-ends - write
				one UI + model and provide additional debuggers.
			</p>
		</div>
		<div class="homeitem3col">
			<h3>Up To The Minute</h3>
			<p>
				JSDT Debug is preparing to release version 3.2.1!
			</p>
		</div>
	</div>
	<div id="rightcolumn">
		$commonside
		$commonplugin
	</div>
</div>

EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
