<?php  				
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	

$App = new App();	
$Nav = new Nav();	
$Menu = new Menu();		

include($App->getProjectCommon());

	#*****************************************************************************
	#
	# Copyright (c) 2010 IBM Corporation and others.
 	# All rights reserved. This program and the accompanying materials
 	# are made available under the terms of the Eclipse Public License v1.0
 	# which accompanies this distribution, and is available at
 	# http://www.eclipse.org/legal/epl-v10.html
 	# Contributors:
	#     IBM Corporation - initial implementation
	#
	# Author: 		Ant Team
	# Date:			August 30, 2010
	#
	# Description: This is the new main page for the JSDT Team website
	#
	#****************************************************************************
	
	$pageTitle 		= "JavaScript Development Tools UI";
	$pageKeywords	= "javascript, JavaScript, JSDT, platform, debugging, debugger, jsdt, breakpoints, about, content, epl, compiler, compile, parse, parser";
	$pageAuthor		= "JSDT Team";
	
	include("../_sideCommon.php");
	include("_pluginCommon.php");
	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<h1>$pageTitle</h1>
		<div class="homeitem3col">
			<h3>Project Overview</h3>
			<p>
				
			</p>
		</div>
		<div class="homeitem3col">
			<h3>JSDT UI Bundles</h3>
			<p>
				The JavaScript Development Tools UI is broken down into the following bundles, which 
				include:
				<ol>
					<li><strong>org.eclipse.wst.jsdt.ui</strong> - contains the UI presentation for the JavaScript IDE.</li>
					<li><strong>org.eclipse.wst.jsdt.doc</strong> - contains the help documentation for JSDT.</li>
					<li><strong>org.eclipse.wst.jsdt.feature</strong> - contains the feature definition of JSDT.</li>
					<li><strong>org.eclipse.wst.jsdt.manipulation</strong> - contains refactoring support.</li>
					<li><strong>org.eclipse.wst.jsdt.support.firefox</strong> - contains special support for Mozilla Firefox.</li>
					<li><strong>org.eclipse.wst.jsdt.support.ie</strong> - contains special support for Microsoft Internet Explorer.</li>
					<li><strong>org.eclipse.wst.jsdt.ui.tests</strong> - contains tests for the UI.</li>
				</ol>
			</p>
		</div>
		<div class="homeitem3col">
			<h3>Up To The Minute</h3>
			<p>
				JSDT UI is preparing to release version 3.2.1!
			</p>
		</div>
	</div>
	<div id="rightcolumn">
		$commonside
		$commonplugin
	</div>
</div>

EOHTML;

	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
