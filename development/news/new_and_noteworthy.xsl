<?xml version="1.0" encoding="utf-8"?>

<!-- 
	An XSLT stylesheet that formats the N&N pages to resemble the Eclipse project's N&N look and feel.
	
	Change Log:
	
	2005-06-22: Nitin Dahyabhai <nitind@us.ibm.com>
	- Created.
	2008-09-18: David Carver <d_a_carver@yahoo.com>
	-  Refactored and removed CDATA sections. General cleanup. 
	2012-06-26: Nitin Dahyabhai <nitind@us.ibm.com>
	- Undo Carver's refactoring/cleanup changes as they stop development-time
	  rendering from working with I.E. (a different stylesheet is used
	  for the production web site) 
	
	
	Usage: 1 release per file, 
	<nodoctype/>
	<release name="release or milestone number, like 1.5" root="../../..">
	<greeting>salutations just for this page</greeting>
	<heading>a heading just for this page</heading>
	<component name="component name, such as Structured Source Editing">
	<item title="item title can go here">
		<title>item title can also go here</title>
		<description>description of this new feature, preferrably with cropped .pngs of the UI</description>
	</item>
	</component>
	<footer>a footer just for this page</footer>
	</release>
	
-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fn="http://www.w3.org/2005/02/xpath-functions"
	version="1.0">

	<!-- read the root location and release name into variables -->

	<xsl:template match="release">
		<xsl:variable name="root" select="@root" />
		<xsl:variable name="release" select="@name" />

		<xsl:if test="count(//isfragment) &lt; 1">
		<xsl:text disable-output-escaping="yes"><![CDATA[<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		
		<html>
			<head>
				<link type="text/css"]]></xsl:text>
				
				href="{$root}/development/news/default_style.css" 
				
				<xsl:text disable-output-escaping="yes"><![CDATA[rel="stylesheet" />
				<title>Web Tools Platform <xsl:value-of select="$release" /> News</title>
			</head>
			<body>
			]]></xsl:text></xsl:if>
			<table>
				<tbody valign="top">
					<tr >
						<td align="left">
							<h1>Eclipse Web Tools Platform <xsl:value-of select="$release" /> - New and Noteworthy</h1>
							<blockquote><xsl:apply-templates select="greeting" /></blockquote>
						</td>
						<td align="right" valign="top"><img src="{$root}/images/wtplogosmall.jpg" alt="Small WTP logo"/></td>
					</tr>
				</tbody>
			</table>
			<blockquote><xsl:apply-templates select="header" /></blockquote>
			<table border="0" cellpadding="10" cellspacing="0" width="80%">
				<tbody>
					<xsl:comment>separating row</xsl:comment>
					<tr>
						<td colspan="2">
							<hr />
						</td>
					</tr>
					<xsl:apply-templates select="component"/>
				</tbody>
			</table>
			<blockquote><xsl:apply-templates select="footer" /></blockquote>
			<xsl:if test="count(//header) &lt; 1">
			<p align="center">
				<a
					href="index.html">
					Up
				</a>
			</p>
			</xsl:if>
		<xsl:if test="count(//isfragment) &lt; 1">
		<xsl:text disable-output-escaping="yes"><![CDATA[
		</body></html>]]></xsl:text></xsl:if>
	</xsl:template>

	<xsl:template match="component">
		<tr>
			<td colspan="2">
				<h2>
					<xsl:value-of select="@name" />
				</h2>
			</td>
		</tr>
		<xsl:comment>separating row</xsl:comment>
		<tr>
			<td colspan="2">
				<hr />
			</td>
		</tr>
		<xsl:apply-templates select="item" />
	</xsl:template>

	<xsl:template match="item">
		<xsl:comment>new item</xsl:comment>
		<tr>
			<td align="left" valign="top" width="30%">
				<b>
					<xsl:apply-templates select="title" />
					<xsl:value-of select="@title" />
				</b>
			</td>
			<td valign="top" width="70%">
				<xsl:apply-templates select="description" />
			</td>
		</tr>
		<xsl:comment>separating row</xsl:comment>
		<tr>
			<td colspan="2">
				<hr />
			</td>
		</tr>
	</xsl:template>

	<xsl:template match="greeting">
		<xsl:apply-templates select="*|@*|text()" />
	</xsl:template>
	<xsl:template match="header">
		<xsl:apply-templates select="*|@*|text()" />
	</xsl:template>
	<xsl:template match="footer">
		<xsl:apply-templates select="*|@*|text()" />
	</xsl:template>
	<xsl:template match="title">
		<xsl:apply-templates select="*|@*|text()" />
	</xsl:template>
	<xsl:template match="description">
		<xsl:apply-templates select="*|@*|text()" />
	</xsl:template>

	<xsl:template match="*|@*|text()">
		<xsl:copy>
			<xsl:apply-templates select="*|@*|text()" />
		</xsl:copy>
	</xsl:template>

</xsl:stylesheet>
