<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../new_and_noteworthy.xsl"?>
<release name="2.0 M6" root="../../..">
	<component name="Java EE Tools">
		<item title="Easier publishing of dependencies">
			<description>
				Entries on the Java Build Path can now be added to the
				published structure of J2EE module projects through the
				<b>J2EE Module Dependencies</b>
				properties page.
				<br />
				<br />
				First, add the desired library or classpath container to
				the Java Build Path
				<br />
				<img src="jee/AddingUserLibrary.png"
					alt=" J2EE Module Dependencies page on a web project " />
				<br />
				<br />
				Then select the library so it will be published in the
				module (Web Library dependencies will be published to
				/WEB-INF/lib, J2EE Module dependencies will be published
				to the EAR root)
				<br />
				<img src="jee/WARJ2EEModuleDependencies.png"
					alt=" Module Dependencies property page on a web project " />
				<br />
				<br />
				<img src="jee/EARJ2EEModuleDependencies.png"
					alt=" Module Dependencies property page on an EAR project " />
				<br />
				The resolved contributions from entries published to the
				EAR root will be displayed in the J2EE Module
				Dependencies page for the EAR project as grayed items.
				<br />
				<br />
				Adopters may also make use of corresponding new APIs
				found in the
				<i>org.eclipse.jst.j2ee.classpathdep</i>
				package of the
				<i>org.eclipse.jst.j2ee</i>
				plug-in.
			</description>
		</item>
		<item title="More flexible validation ">
			<description>
				The validation framework now supports Content Types, so
				additional filename added to content types on the
				Content Type preference page will also be validated by
				their respective validators. The J2EE validators already
				leverage the new content type support with other
				validators expected to transition by the next milestone.
				<br />
				<br />
				<img src="jee/validation-contenttypes.png"
					alt="new filenames being added to an existing content type" />
				<br />
				<br />
				For adopters, new APIs have been added to the validation
				framework supporting turning on and off individual
				validators, translating into increased convenience,
				utility, and performance.
			</description>
		</item>
	</component>
	<component name="JSF Tools">
		<item title="Improved HTML DataTable tag variable support">
			<description>
				EL variables declared using the "var" attribute on the
				<b>h:dataTable</b>
				tag are now recognized by the JSF EL framework. The
				amount of additional support varies depending on the
				"value" attribute used to back the variable, the most
				complete being offered for scalar or array objects with
				generics planned for the future.
				<br />
				<br />
				<img src="jee/DataTableVariableSupport.png"
					alt="content assist popup" />
			</description>
		</item>

		<item
			title="JSF Libraries are now regular Classpath Containers">
			<description>
				JSF Libraries now make use of the
				<i>easier publishing of dependencies</i>
				feature also introduced in this milestone, so now
				references to JSF Libraries in a project are made using
				the Java Build Path and made published/exported using
				the J2EE Module Dependencies, and will appear as JDT
				Libraries in the project explorer.
				<i>
					An upgrade of the JSF Library Registry will
					automatically occur when an older workspace is
					opened. The actual references, however, will require
					a manual upgrade.
				</i>
				Information on this can be found at
				<a
					href="https://wiki.eclipse.org/index.php/JSF_Library_Migration">
					https://wiki.eclipse.org/index.php/JSF_Library_Migration
				</a>
				. JSF Library references are still being created at JSF
				Facet installation time, but if you need to add, modify,
				or remove references after project creation, you will
				now be using the Java Build Path and J2EE Module
				Dependencies property pages.
				<br />
				<br />
				Creating a JSF Library Reference JSF Libraries as
				Classpath Containers
				<br />
				<img src="jee/Fig1.png"
					alt="Fig 1. Add Library from Java Build Path" />
				<br />
				<br />
				Add Library from Java Build Path
				<br />
				<img src="jee/Fig1.png"
					alt="Fig. 2 Choose JSF Libraries" />
				<br />
				<br />
				Choose JSF Libraries
				<br />
				<img src="jee/Fig3.png"
					alt="Fig. 3 Select One or More Libraries" />
				<br />
				<br />
				Select One or More Libraries
				<br />
				<img src="jee/Fig4.png"
					alt="Fig. 4 JSF Libraries Added as Classpath Containers to Java Build Path" />
				<br />
				<br />
				JSF Libraries Added as Classpath Containers to Java
				Build Path
				<br />
				<img src="jee/Fig5.png"
					alt="Fig. 5 Select JSF Libraries for Publish and Export" />
				<br />
				Select JSF Libraries for Publish and Export
			</description>
		</item>


		<item title="Custom JSF Servlet Classname">
			<description>
				The JSF servlet classname can now be specified during
				JSF Facet installation.
				<br />
				<br />
				<img src="jee/Fig6.png"
					alt="Fig. 6 JSF Servlet Classname in JSF Facet Installation Wizard" />
			</description>
		</item>
	</component>
	<component name="JSP">
		<item title="Support for Tag Handlers from Tag files">
			<description>
				Custom tags declared by JSP 2.0 tag files are now
				supported for use by the JSP editor as well as
				debugging. Support for editing and validating tag files
				will be added in a future version.
				<br />
				<br />
				<img src="jee/tagdir-usage1.png"
					alt="tagdir attribute in use" />
			</description>
		</item>
		<item title="Hyperlinking to custom tag declarations">
			<description>
				Activating Hyperlinks within custom tag names will open
				the tag library descriptor or tag file that declares the
				custom tag.
				<br />
				<br />
				<img src="jee/tag-hyperlink1.png"
					alt="hyperlink showing on a custom tag" />
			</description>
		</item>
		<item title="Include directive validation">
			<description>
				The JSP Syntax Validator now warns when the JSP fragment
				referred to by an include directive can not be found.
				<br />
				<br />
				<img src="jee/include-validation1.png"
					alt="warning on bad file value" />
			</description>
		</item>
	</component>


</release>
