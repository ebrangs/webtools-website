<?xml-stylesheet type="text/xsl" href="../../wtphome.xsl"?>
<html>
<head>
  <title>Web Tools Platform 1.5 Maintenance Branching Strategy</title>
  <meta name="root" content="../../../.." />
</head>
<body>

<h1>Web Tools Platform 1.5 Maintenance Branching Strategy</h1>
<p align="right"><em>Updated 2006-07-07</em></p>

<h2>Introduction</h2>

<p>
Branch name: <b>R1_5_maintenance</b>
</p>

<p>
The document details the branching strategy for the WTP 1.5 maintenance release.
Plug-ins and features will be branched on demand. This means
you do not need to branch if your fix is common to both the 1.5 and
2.0 (HEAD) stream. You will need to branch if 
you are committing a fix that is only applicable for the 2.0 but not the 1.5 stream,
or vice versa.
</p>

<h2>Deciding which stream your fix is applicable for</h2>

<p>
Before you fix anything, the first thing to think about is which stream your fix is
applicable for. Is it only applicable for the 1.5 maintenance stream, or only for the 2.0 (HEAD) stream, or both?
In general, any bug fixes that go into the 1.5 maintenance stream should also go
into the 2.0 (HEAD) stream. If your fix is a breaking change (ex. change in API), an
enhancement, etc, then it should go only into the 2.0 (HEAD) stream and not the 1.5
maintenance stream.
<br/>
<br/>If your fix is applicable for both the 1.5 maintenance and 2.0 (HEAD) stream, go
<a href="#common">here</a>
<br/>If your fix is only applicable for the 2.0 (HEAD) stream, go
<a href="#HEAD_only">here</a>
</p>

<a id="common"/>
<h2>Fixing a bug that is common to both the 1.5 maintenance and 2.0 HEAD stream</h2>

<p>
So your fix is common to both the 1.5 maintenance stream and the 2.0 (HEAD) stream.
For this scenario, my recommandation is to check out the
1.5 maintenance version of the plug-in/feature, check in the fix and retrofit
the fix back to the 2.0 (HEAD) stream. To check out the correct 1.5 maintenance version
of your plug-in/feature...

<ol>

<li>
First, check if your plug-in/feature has been branched or not. If so, you'll
need to check out the branched version of the plug-in/feature. Otherwise you'll
check out the plug-in/feature from HEAD. I'll use one of the WST plug-in as an
example. In the CVS Repositories view, expand the <b>Branches</b> node and see if the
1.5 maintenance branch is already configured. If it is not configured, read on.
Otherwise, go <a href="#common1">here</a>.
<p><img src="fig1.gif"/></p>
</li>

<li>
To configure the 1.5 maintenance branch, right click on the wst folder
(right click on the jst folder if you are working with jst plug-ins/features) >
Team > Configure Branches and Versions...
</li>

<li>
In the popup dialog, browse for any files in your plug-in/feature. After you do that,
a list of available tag name will show up in the RHS panel.
<p><img src="fig2.gif"/></p>
</li>

<li>
If you don't see the <b>R1_5_maintenance</b> tag, that means your plug-in/feature
is not branched. In this case, you can check out the plug-in/feature from HEAD, check
in your fix and release your changes to both the 1.5.1 and 2.0 map files.
More information on releasing is available <a href="#release_15">here</a>.
</li>

<li>
If you see the <b>R1_5_maintenance</b> tag, that means this plug-in/feature
has already been branched. Click the Deselect All button > check the
<b>R1_5_maintenance</b> tag > click the Add Checked Tag button > OK.
<p><img src="fig3.gif"/></p>
</li>

<li>
<a id="common1"/>
In the CVS Repositories view, expand the Branches node > expand R1_5_maintenance >
browse for your plug-in/feature and check it out to your workspace. You can now
fix and release your changes. One thing to be careful is that since your plug-in/feature
is already branched, the changes that you made to the branched plug-in/feature should
only be released to the 1.5 maintenance stream and 
not the 2.0 (HEAD) stream. <a href="#release_15">Here's how</a> to release to 
the 1.5 maintenance stream. After you fix and release to the 1.5 maintenance stream,
you need to check out the same plug-in/feature from HEAD again and release the
changes to the 2.0 (HEAD) stream. <a href="#release_HEAD">Here's how</a> to release
to the 2.0 (HEAD) stream.
<p><img src="fig4.gif"/></p>
</li>

</ol>

</p>

<a id="#HEAD_only"/>
<h2>Fixing 2.0 only bug</h2>

<p>
Before you commit your 2.0 only bug fix, you must branch your plug-in/feature
(assuming it is not already branched). To check whether a plug-in/feature is
branched or not, follow the instructions outlined in the <a href="#common">
previous section</a>.
</p>

<p>
For 2.0 only bug fixes, the first thing that you should do is check out your
plug-in/feature from HEAD to your workspace. If your plug-in/feature is not yet
branched, branch it now. Here's how...
<ol>
<li>
In your workspace, right click on the plug-in/feature project > Team > Branch...
</li>
<li>
Enter <b>R1_5_maintenance</b> as the branch name > OK.
<p><img src="fig5.gif"/></p>
</li>
<li>
That's it. You have branched your plug-in/feature. The following figures show you 
the difference between a plug-in from HEAD and a plug-in from the 1.5 maintenance 
branch. 
<p>
<img src="fig6.gif"/>
<img src="fig7.gif"/>
</p>
</li>
</ol>
The only thing left to do is to fix your code and <a href="#release_HEAD">release</a>
your changes to the 2.0 (HEAD) stream. Just a reminder that you DO NOT need to
release your changes to the 1.5 maintenance stream.
</p>

<p>
We have created a document to keep track of the current status of WTP 1.5 branching.
The document is located <a href="http://www.eclipse.org/webtools/development/cvs_branches.html">here</a>.
If you have branched a plug-in/feature, please update the document.
</p>

<a id="release_15"/>
<h2>Releasing fixes to the 1.5 maintenance stream</h2>

<p>
<ol>

<li>
In the CVS Repositories view, expand the HEAD node > right click on the releng
project > Configure Branches and Versions...
</li>

<li>
In the LHS panel, select any files. After you do that, a list of available tag name
will show up in the RHS panel.
<p><img src="fig2.gif"/></p>
</li>

<li>
Click the Deselect All button > check the R1_5_maintenance tag > click the 
Add Checked Tag button > OK.
<p><img src="fig3.gif"/></p>
</li>

<li>
In the CVS Repositories view, expand the Branches node > expand R1_5_maintenance > 
right click on the releng project > Check out.
<p><img src="fig8.gif"/></p>
</li>

<li>
You can now release your changes to the newly checked out releng project.
I recommand using different tag names for the 1.5 and 2.0 (HEAD) map files.
</li>

</ol>
</p>

<a id="release_HEAD"/>
<h2>Releasing fixes to the 2.0 HEAD stream</h2>

<p>
Check out the releng project from HEAD and release your changes to it.
I recommand using different tag names for the 1.5 and 2.0 (HEAD) map files.
</p>

<h2>Other</h2>

<p>
Retrofitting/merging changes between the 1.5 maintenance stream and the 2.0 (HEAD)
stream can be time consuming. There is an Eclipse tool available that helps you
retrofit/merge changes between a branch and HEAD. For more information, click
<a href="http://www.eclipse.org/articles/Article-CVS-branching/eclipse_branch.html">here</a>.
</p>

</body>
</html>