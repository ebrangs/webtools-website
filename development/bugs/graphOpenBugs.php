<svg xmlns="http://www.w3.org/2000/svg" height="100%" width="100%">
<?php
	function drawGraph($title, $xmax, $ymax, $xlegend, $ylegend)
	{
		echo "<text style=\"text-anchor:middle\" x=\"50%\" y=\"10%\">$title</text>";
		echo "<line style=\"stroke:rgb(128,128,128);stroke-width:1\" x1=\"15%\" y1=\"15%\" x2=\"15%\" y2=\"85%\"/>";
		echo "<line style=\"stroke:rgb(128,128,128);stroke-width:1\" x1=\"15%\" y1=\"85%\" x2=\"85%\" y2=\"85%\"/>";
		echo "<text style=\"text-anchor:middle\" x=\"50%\" y=\"93%\">$xlegend</text>";
		echo "<text style=\"text-anchor:middle\" x=\"8%\" y=\"50%\">$ylegend</text>";
		$delta = (int)$ymax/10;
		for ($i = 0; $i < $ymax; $i++)
		{
			if ($i % $delta == 0)
			{
				$y = 85 - ($i/$ymax*70);
				echo "<text style=\"text-anchor:end\" x=\"14.5%\" y=\"$y%\">$i</text>";
				echo "<line style=\"stroke:rgb(128,128,128);stroke-width:1\" x1=\"14.5%\" y1=\"$y%\" x2=\"15.5%\" y2=\"$y%\"/>";
			}
		}
	}

	function countTimeIntervals($start, $end)
	{
		$count = 0;
		while ($start < $end)
		{
			$count++;
			$start = date("YmdHis", mktime(substr($start, 8, 2), substr($start, 10, 2), substr($start, 12, 2), substr($start, 4, 2), substr($start, 6, 2) + 7, substr($start, 0, 4)));
		}
		$count++;
		return $count;
	}

	function plot($xmax, $ymax, $width, $height, $xstart, $ystart, $prevx, $prevy, $currx, $curry, $x, $y, $r, $g, $b, $text)
	{
		$x1 = $prevx/$xmax*$width + $xstart;
		$y1 = $ystart - ($prevy/$ymax*$height);
		$x2 = $currx/$xmax*$width + $xstart;
		$y2 = $ystart - ($curry/$ymax*$height);
		echo "<line style=\"stroke:rgb($r,$g,$b);stroke-width:1\" x1=\"$x1%\" y1=\"$y1%\" x2=\"$x2%\" y2=\"$y2%\"/>\n";
		if ($text != null)
		{
			echo "<text fill=\"rgb($r,$g,$b)\" style=\"text-anchor:middle\" x=\"$x2%\" y=\"";
			echo $y2 - 1;
			echo "%\">$text</text>";
		}
		$i = (int)$xmax/10;
		if ($currx == 0 || $currx % $i == 0)
		{
			echo "<text style=\"text-anchor:middle\" x=\"$x2%\" y=\"88%\">";
			echo substr($x, 0, 8);
			echo "</text>";
			echo "<line style=\"stroke:rgb(128,128,128);stroke-width:1\" x1=\"$x2%\" y1=\"84.5%\" x2=\"$x2%\" y2=\"85.5%\"/>";
		}
	}
?>
<?php
	require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";
	
	# Connect to database
	$allBugsQuery = "SELECT
					BUG.bug_id,
					BUG.creation_ts
				FROM 
					bugs AS BUG 
					INNER JOIN products AS PROD ON PROD.id = BUG.product_id 
				WHERE 
					PROD.name='Web Tools' 
				ORDER BY 
					BUG.creation_ts";

	$resolvedBugsQuery = "SELECT
					BUG_ACT.bug_id,
					BUG_ACT.bug_when,
					BUG_ACT.added
				FROM 
					bugs_activity AS BUG_ACT 
					INNER JOIN bugs AS BUG ON BUG.bug_id = BUG_ACT.bug_id
					INNER JOIN products AS PROD ON PROD.id = BUG.product_id 
					INNER JOIN fielddefs AS FIELD ON FIELD.fieldid = BUG_ACT.fieldid 
				WHERE 
					PROD.name='Web Tools' 
					AND FIELD.fieldid=8 
					AND (BUG_ACT.added='RESOLVED' OR BUG_ACT.added='REOPENED')
				ORDER BY 
					BUG_ACT.bug_when";

	$dbc = new DBConnectionBugs();
	$dbh = $dbc->connect();
	$rs = mysql_query($allBugsQuery, $dbh);
	
	if(mysql_errno($dbh) > 0)
	{
		echo "There was an error processing this request".
		
		# For debugging purposes - don't display this stuff in a production page.
		# echo mysql_error($dbh);
		
		# Mysql disconnects automatically, but I like my disconnects to be explicit.
		$dbc->disconnect();
		exit;
	}

	$today = date("YmdHis");
	$cutoff = 20040801000000;
	$prevBugCount = 0;
	$currBugCount = 0;
	$dateCount = 0;
	$xmax = countTimeIntervals($cutoff, $today);
	$ymax = mysql_num_rows($rs);
	drawGraph("New / Assigned / Reopened Bugs", $xmax, $ymax, "Time", "# Bugs");
	$total = array();
	while($myrow = mysql_fetch_assoc($rs))
	{
		$creation_ts = $myrow['creation_ts'];
		$creation_ts = substr($creation_ts, 0, 4) . substr($creation_ts, 5, 2) . substr($creation_ts, 8, 2) . substr($creation_ts, 11, 2) . substr($creation_ts, 14, 2) . substr($creation_ts, 17, 2);
		if ($creation_ts > $cutoff)
		{
			$total[] = $currBugCount;
			plot($xmax, $ymax, 70, 70, 15, 85, $dateCount, $prevBugCount, $dateCount + 1, $currBugCount, $cutoff, $currBugCount, 0, 0, 128, null);
			$prevBugCount = $currBugCount;
			$dateCount++;
			$cutoff = date("YmdHis", mktime(substr($cutoff, 8, 2), substr($cutoff, 10, 2), substr($cutoff, 12, 2), substr($cutoff, 4, 2), substr($cutoff, 6, 2) + 7, substr($cutoff, 0, 4)));
		}
		$currBugCount++;
	}
	$total[] = $currBugCount;
	plot($xmax, $ymax, 70, 70, 15, 85, $dateCount, $prevBugCount, $dateCount + 1, $currBugCount, $cutoff, $currBugCount, 0, 0, 128, "Total: " . $currBugCount);

	$rs = mysql_query($resolvedBugsQuery, $dbh);
	
	if(mysql_errno($dbh) > 0)
	{
		echo "There was an error processing this request".
		
		# For debugging purposes - don't display this stuff in a production page.
		# echo mysql_error($dbh);
		
		# Mysql disconnects automatically, but I like my disconnects to be explicit.
		$dbc->disconnect();
		exit;
	}

	$cutoff = 20040801000000;
	$prevBugCount = 0;
	$currBugCount = 0;
	$dateCount = 0;
	while($myrow = mysql_fetch_assoc($rs))
	{
		$bug_when = $myrow['bug_when'];
		$bug_when = substr($bug_when, 0, 4) . substr($bug_when, 5, 2) . substr($bug_when, 8, 2) . substr($bug_when, 11, 2) . substr($bug_when, 14, 2) . substr($bug_when, 17, 2);
		if ($bug_when > $cutoff)
		{
			plot($xmax, $ymax, 70, 70, 15, 85, $dateCount, $prevBugCount, $dateCount + 1, $total[$dateCount] - $currBugCount, $cutoff, $total[$dateCount] - $currBugCount, 255, 128, 0, null);
			$prevBugCount = $total[$dateCount] - $currBugCount;
			$dateCount++;
			$cutoff = date("YmdHis", mktime(substr($cutoff, 8, 2), substr($cutoff, 10, 2), substr($cutoff, 12, 2), substr($cutoff, 4, 2), substr($cutoff, 6, 2) + 7, substr($cutoff, 0, 4)));
		}
		if ($myrow['added'] == "REOPENED")
		{
			$currBugCount--;
		}
		else
		{
			$currBugCount++;
		}
	}
	plot($xmax, $ymax, 70, 70, 15, 85, $dateCount, $prevBugCount, $dateCount + 1, $total[$dateCount] - $currBugCount, $cutoff, $total[$dateCount] - $currBugCount, 255, 128, 0, "New/Assigned/Reopened: " . ($total[$dateCount] - $currBugCount));

	$dbc->disconnect();
	$rs = null;
	$dbh = null;
	$dbc = null;
?>
</svg>