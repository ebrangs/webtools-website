<html>

<head>
<title>Top 10: Resolved, Unverified Bugs</title>
</head>

<body>
<table BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%">
  <tr>
    <td WIDTH="100%">
      <table BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%" BGCOLOR="#006699">
        <tr>
          <td BGCOLOR="#000000" width="116">
            <img src="http://bugs.eclipse.org/bugs/images/EclipseBannerPic.jpg" width="115" height="50">
          </td>
          <td WIDTH="637">
            <a href=http://www.eclipse.org><img SRC="http://bugs.eclipse.org/bugs/images/gradient.jpg" BORDER=0 height=50 width=282></a>
          </td>
          <td WIDTH="250">
            <img src="http://bugs.eclipse.org/bugs/images/eproject-simple.gif" width="250" height="48">
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<table BORDER=0 CELLSPACING=0 CELLPADDING=0 WIDTH="100%">
  <tr>
    <td><h2>Top 10: Resolved, Unverified Bugs</h2></td>
  </tr>
</table>

<?php
include 'top10.php';
genTop10("SELECT COMP.name AS reporter, BUG.bug_severity, BUG.delta_ts FROM bugs AS BUG INNER JOIN products AS PROD ON PROD.id = BUG.product_id INNER JOIN components AS COMP ON COMP.id = BUG.component_id WHERE PROD.name='Web Tools' AND BUG.bug_status='RESOLVED' AND BUG.resolution NOT IN ('REMIND','LATER') ORDER BY COMP.name", "https://bugs.eclipse.org/bugs/report.cgi?x_axis_field=bug_severity&y_axis_field=component&z_axis_field=&query_format=report-table&product=Web+Tools&bug_status=RESOLVED&format=table&action=wrap&field0-0-0=days_elapsed&type0-0-0=greaterthan&value0-0-0=14&amp;field0-1-0=resolution&amp;type0-1-0=nowords&amp;value0-1-0=LATER+REMIND", "Component", 14);
?>

</body>
</html>