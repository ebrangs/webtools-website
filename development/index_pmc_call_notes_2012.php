<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon()); # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>

<p>Toll free (in US and Canada): 888-426-6840 <br />
Access code: 5019600# <br />
Caller pay alternative number: 215-861-6239<br />
<a
	href="https://www.teleconference.att.com/servlet/glbAccess?process=1&accessCode=6460142&accessNumber=2158616239">Full
list of global access numbers</a><br />
<br />
<p><a href="http://www.timeanddate.com/worldclock/custom.html?cities=224,207,1440,107&amp;hour=11&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1500 UTC</a></p>

<h2>Meeting agendas and minutes</h2>
<ul compact="true">
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-12-11">December 11, 2012</a></li>
<li>December 4, 2012 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-11-27">November 27, 2012</a></li>
<li>November 20, 2012 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-11-13">November 13, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-11-06">November 6, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-10-30">October 30, 2012</a></li>
<li>October 23, 2012 - Meeting cancelled</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-10-16">October 16, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-10-09">October 9, 2012</a></li>
<li>October 2, 2012 - Call cancelled - use mailing list for discussion</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-09-25">September 25, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-09-18">September 18, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-09-04">September 4, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-08-21">August 21, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-08-07">August 07, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-07-31">July 31, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-07-17">July 17, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-06-26">June 26, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-06-19">June 19, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-06-12">June 12, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-06-05">June 5, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-05-22">May 22, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-05-15">May 15, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-05-08">May 8, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-05-01">May 1, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-04-17">April 17, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-04-10">April 10, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-04-03">April 3, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-03-20">March 20, 2012</a></li>
<li>March 13, 2012 - No call this week - Use mailing list for any topics</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-03-06">March 6, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-02-28">February 28, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-02-21">February 21, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-02-14">February 14, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-02-07">February 7, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-01-31">January 31, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-01-24">January 24, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-01-17">January 17, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-01-10">January 10, 2012</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2012-01-03">January 03, 2012</a></li>

<li><a href="index_pmc_call_notes_2011.php">2011</a></li>
<li><a href="index_pmc_call_notes_2010.php">2010</a></li>
<li><a href="index_pmc_call_notes_2009.php">2009</a></li>
<li><a href="index_pmc_call_notes_2008.php">2008</a></li>
<li><a href="index_pmc_call_notes_2007.php">2007</a></li>
<li><a href="index_pmc_call_notes_2006.php">2006</a></li>
<li><a href="index_pmc_call_notes_2005.php">2005</a></li>
<li><a href="index_pmc_call_notes_2004.php">2004</a></li>
</ul>
<hr />
<!-- <p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
-->
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadHTML($xmlString);
// Load the XSL source
//$xsl = DOMDocument::load($root . '/webtools/wtpphoenix.xsl');
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');
// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

//echo "xml:";
//echo $xml->saveXML();
$maincontent = $proc->transformToXml($xml);
//echo "maincontent:";
//echo $maincontent;
$html = <<<EOHTML
<div id="maincontent">
$wtpTopButtons
$maincontent
</div>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
