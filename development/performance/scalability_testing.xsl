<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

<xsl:template match="/">
<html>
<body>
<table width="100%" cellspacing="5" cellpadding="2" border="0">
	<tr>
		<td width="60%" align="left"><font style="font-size: x-large;; font-family: Verdana, Arial, Helvetica, sans-serif; font-weight: bold">Scalability Testing</font></td><td width="40%"><img width="120" hspace="50" height="86" align="middle" src="../../images/Idea.jpg"/></td>
	</tr>
</table>

<table width="100%" cellspacing="5" cellpadding="2" border="0">
	<tr>
		<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Editors</font></b></td>
	</tr>
</table>

<table width="100%" border="1">
	<tr>
		<th>Editor</th>
		<th>Plug-in</th>
		<th>Opening files of various size</th>
		<th>Opening the editor mutliple times</th>
	</tr>
	<xsl:for-each select="scalabilitytesting/editor">
		<tr>
			<td><xsl:value-of select="@name"/></td>
			<td><xsl:value-of select="@plugin"/></td>
			<td><xsl:call-template name="passfail"><xsl:with-param name="value" select="@filesizetestcase"/></xsl:call-template></td>
			<td><xsl:call-template name="passfail"><xsl:with-param name="value" select="@multiopentestcase"/></xsl:call-template></td>
		</tr>
	</xsl:for-each>
</table>

<p> </p>

<table width="100%" cellspacing="5" cellpadding="2" border="0">
	<tr>
		<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Validators</font></b></td>
	</tr>
</table>

<table width="100%" border="1">
	<tr>
		<th>Validator</th>
		<th>Plug-in</th>
		<th>Validating files of various size</th>
		<th>Running the validator multiple time</th>
	</tr>
	<xsl:for-each select="scalabilitytesting/validator">
		<tr>
			<td><xsl:value-of select="@name"/></td>
			<td><xsl:value-of select="@plugin"/></td>
			<td><xsl:call-template name="passfail"><xsl:with-param name="value" select="@filesizetestcase"/></xsl:call-template></td>
			<td><xsl:call-template name="passfail"><xsl:with-param name="value" select="@multivalidatetestcase"/></xsl:call-template></td>
		</tr>
	</xsl:for-each>
</table>

<p> </p>

<table width="100%" cellspacing="5" cellpadding="2" border="0">
	<tr>
		<td BGCOLOR="#0080C0" COLSPAN="2" VALIGN="TOP" ALIGN="LEFT"><b><font face="Arial,Helvetica" color="#FFFFFF">Builders</font></b></td>
	</tr>
</table>

<table width="100%" border="1">
	<tr>
		<th>Builder</th>
		<th>Plug-in</th>
		<th>Building projects of various size</th>
	</tr>
	<xsl:for-each select="scalabilitytesting/builder">
		<tr>
			<td><xsl:value-of select="@name"/></td>
			<td><xsl:value-of select="@plugin"/></td>
			<td><xsl:call-template name="passfail"><xsl:with-param name="value" select="@projectsizetestcase"/></xsl:call-template></td>
		</tr>
	</xsl:for-each>
</table>

</body>
</html>
</xsl:template>

<xsl:template name="passfail">
	<xsl:param name="value"/>
	<xsl:choose>
		<xsl:when test="$value = 'n' or $value = 'N'">
			<img src="../../images/FAIL.gif"/>
		</xsl:when>
		<xsl:otherwise>
			<img src="../../images/OK.gif"/>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

</xsl:stylesheet>