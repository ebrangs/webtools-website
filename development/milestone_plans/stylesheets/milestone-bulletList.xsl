<?xml version="1.0" encoding="UTF-8"?>
<!--
 
 ChangeLog:
 
 2005-02-24 Phil Avery, pavery@us.ibm.com
 - added 'onmouseover' and 'onmouseout' actions
 to display 'detail' info in a new window
 
-->
<xsl:stylesheet
 version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:xalan="http://xml.apache.org/xslt"
 xmlns="http://www.w3.org/1999/xhtml"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
 <xsl:output
  method="html"
  encoding="UTF-8" />
 <xsl:include href="milestone-common.xsl" />
 <xsl:variable
  name="rel"
  select="'../../../../'" />
 <xsl:template match="/">
  <html>
   <head>
    <xsl:variable
     name="component"
     select="/plan/component[1]" />
    <title>
     <xsl:value-of select="concat($component/@name, ' ', $component/milestone[1]/@name, ' milestone plan')" />
    </title>
    <link
     href="../../../../development/milestone_plans/stylesheets/images/default_style.css"
     type="text/css"
     rel="stylesheet" />
    <script
     type="text/javascript"
     src="../../../../popup.js">
     // script is linked
    </script>
   </head>
   <body>
    <xsl:for-each select="//component">
     <xsl:sort select="@name" />
     <xsl:apply-templates select="." />
    </xsl:for-each>
    <br />
   </body>
  </html>
 </xsl:template>
 <xsl:template match="component">
  <table
   cellSpacing="5"
   cellPadding="2"
   width="100%"
   border="0">
   <tr>
    <td>
     <font class="indextop">
      <xsl:value-of select="concat(@name, ' ', milestone[1]/@name)" />
     </font>
     <br />
     <font class="indexsub">
      <xsl:value-of select="description" />
     </font>
    </td>
    <td>
     <br />
    </td>
    <td width="28%">
     <img
      src="../../../../development/milestone_plans/stylesheets/images/Idea.jpe"
      width="120"
      height="86" />
    </td>
   </tr>
  </table>
  <!--  in this case print the Legend after the first milestone -->
  <!--  and then print the rest of the milestones -->
  <xsl:apply-templates select="//milestone[1]" />
  <xsl:call-template name="printLegend">
   <xsl:with-param
    name="rel"
    select="$rel" />
  </xsl:call-template>
  <xsl:for-each select="//milestone[position() > 1]">
   <xsl:sort select="@name" />
   <xsl:apply-templates select="." />
  </xsl:for-each>
 </xsl:template>
 <xsl:template match="milestone">
  <table
   cellSpacing="5"
   cellPadding="2"
   width="100%"
   border="0">
   <tr>
    <a name="Overall_Planning">
     <td
      valign="top"
      align="left"
      width="100%"
      bgColor="#0080c0"
      colSpan="2"
      rowspan="3">
      <b>
       <font face="Arial,Helvetica">
        <font color="#ffffff">
         <xsl:copy-of select="title/text()" />
        </font>
       </font>
      </b>
     </td>
    </a>
   </tr>
  </table>
  <table
   cellspacing="5"
   width="100%">
   <tr>
    <td>
     <xsl:copy-of select="description" />
    </td>
   </tr>
  </table>
  <xsl:for-each select=".//category">
   <!--  <xsl:sort select="@name" /> -->
   <xsl:apply-templates select="." />
  </xsl:for-each>
 </xsl:template>
 <xsl:template match="category">
  <tr>
   <td>
    <table>
     <tr>
      <td valign="top">
       <img
        src="../../../../development/milestone_plans/stylesheets/images/Adarrow.gif"
        nosave=""
        border="0"
        height="16" />
      </td>
      <td>
       <b>
        <xsl:value-of select="@name" />
       </b>
       <ul>
        <xsl:apply-templates select="*" />
       </ul>
      </td>
     </tr>
    </table>
   </td>
  </tr>
 </xsl:template>
 <xsl:template match="item">
  <li>
   <xsl:call-template name="drawStatus">
    <xsl:with-param
     name="rel"
     select="$rel" />
    <xsl:with-param
     name="value"
     select="@status" />
   </xsl:call-template>
   <xsl:if test="@bug">
    <xsl:call-template name="drawBug">
     <xsl:with-param
      name="bug"
      select="@bug" />
    </xsl:call-template>
   </xsl:if>
   <xsl:copy-of select="description" />
   <xsl:if test="@priority">
    <font color="8080FF">
     <xsl:value-of select="concat(' [',@priority,']')" />
    </font>
   </xsl:if>
   <xsl:if test="detail">
    <xsl:call-template name="drawDetail">
     <xsl:with-param
      name="detail"
      select="detail" />
     <xsl:with-param
      name="rel"
      select="$rel" />
    </xsl:call-template>
   </xsl:if>
   <xsl:if test="@helpWanted">
    <xsl:call-template name="drawHelpWanted">
     <xsl:with-param
      name="rel"
      select="$rel" />
    </xsl:call-template>
   </xsl:if>
   <xsl:if test="/plan/@scheduleInfo">
    <font color="C1C1C1">
     <xsl:choose>
      <xsl:when test="developer/@name">
       (
       <xsl:value-of select="developer/@name" />
       )
      </xsl:when>
      <xsl:otherwise>(unassigned)</xsl:otherwise>
     </xsl:choose>
    </font>
   </xsl:if>
  </li>
  <xsl:if test="step/description">
   <ul>
    <xsl:apply-templates select="step" />
   </ul>
  </xsl:if>
 </xsl:template>
 <xsl:template match="step">
  <li>
   <xsl:call-template name="drawStatus">
    <xsl:with-param
     name="rel"
     select="$rel" />
    <xsl:with-param
     name="value"
     select="@status" />
   </xsl:call-template>
   <xsl:copy-of select="description" />
  </li>
 </xsl:template>
</xsl:stylesheet>
