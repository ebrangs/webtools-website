<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>

<h2>Meeting agendas and minutes</h2>
<ul>
   <li>December 30, 2008 (no meeting, Holiday)</li>
   <li>December 23, 2008 (no meeting, Holiday)</li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-12-16">December 16, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-12-09">December 9, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-12-02">December 2, 2008</a></li>
   <li>November 25, 2008 (no meeting, Holiday week)</li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-11-18">November 18, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-11-11">November 11, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-11-04">November 04, 2008</a></li>
   <li>October 28, 2008 (no meeting, EclipseWorld)</li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-10-21">October 21, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-10-14">October 14, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-10-07">October 07, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-09-30">September 30, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-09-23">September 23, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-09-16">September 16, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-09-09">September 09, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-09-02">September 02, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-08-26">August 26, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-08-19">August 19, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-08-12">August 12, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-08-05">August 05, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-07-29">July 29, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-07-22">July 22, 2008</a></li>
   <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-07-15">July 15, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-07-08">July 08, 2008</a></li>
	<li>July 01, 2008 (no meeting)</li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-06-24">June 24, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-06-17">June 17, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-06-10">June 10, 2008</a></li>
	<li>June 13, 2008 (no meeting)</li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-05-27">May 27, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-05-20">May 20, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-05-13">May 13, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-05-06">May 06, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-04-29">April 29, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-04-22">April 22, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-04-15">April 15, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-04-08">April 08, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-04-01">April 01, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-03-25">March 25, 2008</a></li>
	<li>EclipseCon</li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-03-11">March 11, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-03-04">March 4, 2008</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-02-26">February 26, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-02-19">February 19, 2008</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-02-12">February 12, 2008</a></li>
	<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-02-05">February 05, 2008</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-01-29">January 29, 2008</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-01-22">January 22, 2008</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-01-15">January 15, 2008</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2008-01-08">January 8, 2008</a></li>
</ul>
<hr />
<p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadXML($xmlString);
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$maincontent = $proc->transformToXML($xml);


$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
	$maincontent
	</div>
</div>
EOHTML;
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
	?>
