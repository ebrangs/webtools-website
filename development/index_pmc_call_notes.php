<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon()); # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>

<p>Toll free (in US and Canada): 888-426-6840 <br />
Access code: 32557235# <br />
Caller pay alternative number: 215-861-6239<br />
<a
	href="https://www.teleconference.att.com/servlet/glbAccess?process=1&accessCode=6460142&accessNumber=2158616239">Full
list of global access numbers</a><br />
<br />
<p><a href="http://www.timeanddate.com/worldclock/custom.html?cities=224,207,1440,107&amp;hour=14&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1800 UTC</a></p>

<h2>Meeting agendas and minutes - every two weeks</h2>
<ul compact="true">

<li>Next meeting: August ??, 2017</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2017-06-13">June 13, 2017</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2017-05-30">May 30, 2017</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2017-05-16">May 16, 2017</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2017-04-11">April 11, 2017</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2017-02-28">February 28, 2017</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2017-01-17">January 17, 2017</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2017-01-03">January 3, 2017</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-11-15">November 15, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-10-18">October 18, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-10-04">October 4, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-09-06">September 6, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-08-23">August 23, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-06-14">June 14, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-05-31">May 31, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-05-17">May 17, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-05-03">May 3, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-04-19">April 19, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-03-15">March 15, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-03-01">March 1, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-02-16">February 16, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-02-02">February 2, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-01-19">January 19, 2016</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2016-01-05">January 5, 2016</a></li>


<li><a href="index_pmc_call_notes_2015.php">2015</a></li>
<li><a href="index_pmc_call_notes_2014.php">2014</a></li>
<li><a href="index_pmc_call_notes_2013.php">2013</a></li>
<li><a href="index_pmc_call_notes_2012.php">2012</a></li>
<li><a href="index_pmc_call_notes_2011.php">2011</a></li>
<li><a href="index_pmc_call_notes_2010.php">2010</a></li>
<li><a href="index_pmc_call_notes_2009.php">2009</a></li>
<li><a href="index_pmc_call_notes_2008.php">2008</a></li>
<li><a href="index_pmc_call_notes_2007.php">2007</a></li>
<li><a href="index_pmc_call_notes_2006.php">2006</a></li>
<li><a href="index_pmc_call_notes_2005.php">2005</a></li>
<li><a href="index_pmc_call_notes_2004.php">2004</a></li>
</ul>
<hr />
<!-- <p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
-->
<p>Please send any additions or corrections to <a href="mailto: ccc@us.ibm.com">Carl Anderson.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadHTML($xmlString);
// Load the XSL source
//$xsl = DOMDocument::load($root . '/webtools/wtpphoenix.xsl');
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');
// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

//echo "xml:";
//echo $xml->saveXML();
$maincontent = $proc->transformToXml($xml);
//echo "maincontent:";
//echo $maincontent;
$html = <<<EOHTML
<div id="maincontent">
$wtpTopButtons
$maincontent
</div>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
