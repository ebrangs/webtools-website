<?php                                                                                                                          require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");    require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");     require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");     $App     = new App();    $Nav    = new Nav();    $Menu     = new Menu();        include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords    = "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor        = "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>
<p>Toll free in the US: 877-421-0030</p>
<p>Alternate: 770-615-1247</p>
<p>Access code: 800388# </p>
<p><a href="https://wiki.eclipse.org/images/f/f6/WTP_status_phone_access.pdf">Full list of phone numbers</a></p>
<p><a href="http://www.timeanddate.com/worldclock/custom?cities=224,207,1440,107&amp;hour=11&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1500 UTC</a></p>
<h2>Meeting agendas and minutes</h2>
<ul>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-12-21">December 21, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-12-14">December 14, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-11-30">November 30, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-11-23">November 23, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-11-16">November 16, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-11-09">November 9, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-11-02">November 2, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-10-26">October 26, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-10-19">October 19, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-10-12">October 12, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-10-05">October 5, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-09-27">September 27, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-09-22">September 22, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-09-13">September 13, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-09-06">September 06, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-08-16">August 16, 2004</a></li>
    <li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2004-08-09">August 9, 2004</a></li>
    <li><a href="pmc_call_notes/2004Q4-plan-update.html">Quarterly Planning Updates 2004 Q4</a></li>
</ul>
<hr />
<p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

    $xml = DOMDocument::loadXML($xmlString);
    // Load the XSL source
    $xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

    // Configure the transformer
    $proc = new XSLTProcessor;
    $proc->importStyleSheet($xsl); // attach the xsl rules

    $maincontent = $proc->transformToXML($xml);

$html = <<<EOHTML
<div id="maincontent">
    <div id="midcolumn">
    $maincontent
    </div>
</div>
EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
