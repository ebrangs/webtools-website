<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>

<h2>Meeting agendas and minutes</h2>
<ul>
<li>December 29, 2009 (no meeting, Holiday)</li>
<li>December 22, 2009 (no meeting, Holiday)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-12-15">December 15, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-12-08">December 08, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-12-01">December 01, 2009</a></li>
<li>November 24, 2009 (no meeting - U.S. Holiday week)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-11-17">November 17, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-11-10">November 10, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-11-03">November 3, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-10-27">October 27, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-10-20">October 20, 2009</a></li>
<li>October 13, 2009 (no meeting)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-10-06">October 06, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-09-29">September 29, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-09-22">September 22, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-09-15">September 15, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-09-08">September 08, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-09-01">September 01, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-08-25">August 25, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-08-18">August 18, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-08-11">August 11, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-08-04">August 4, 2009</a></li>
<li>July 27, 2009 (no meeting - Holidays)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-07-21">July 21, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-07-14">July 14, 2009</a></li>
<li>July 07, 2009 (no meeting - Holiday and release break)</li>
<li>June 30, 2009 (no meeting - Holiday and release break)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-06-23">June 23, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-06-16">June 16, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-06-09">June 09, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-06-02">June 02, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-05-26">May 26, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-05-19">May 19, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-05-12">May 12, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-05-05">May 05, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-04-28">April 28, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-04-21">April 21, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-04-14">April 14, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-04-07">April 07, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-03-31">March 31, 2009</a></li>
<li>March 24, 2009 (no meeting - EclipseCon)</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-03-17">March 17, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-03-10">March 10, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-03-03">March 03, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-02-24">February 24, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-02-17">February 17, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-02-10">February 10, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-02-03">February 3, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-01-27">January 27, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-01-20">January 20, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-01-13">January 13, 2009</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2009-01-06">January 06, 2009</a></li>
</ul>
<hr />
<p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadXML($xmlString);
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$maincontent = $proc->transformToXML($xml);


$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
	$maincontent
	</div>
</div>
EOHTML;
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
	?>
