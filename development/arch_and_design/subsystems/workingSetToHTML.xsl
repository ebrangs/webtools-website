<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:stylesheet
	version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/">
		<html>
			<body>
				<xsl:apply-templates select="workingSetManager" />
			</body>
		</html>
	</xsl:template>

	<xsl:template match="workingSetManager">
		<h2>Build Components (by plugins)</h2>
		<p>The listed plugins are preliminary, for review.</p>
		<xsl:apply-templates select="workingSet" />

	</xsl:template>

	<xsl:template match="workingSetManager/workingSet">

		<xsl:variable
			name="toplevelname"
			select="substring(@name,1,3)">
		</xsl:variable>

		<xsl:if test="$toplevelname = 'jst' or $toplevelname = 'wst'">

			<h3>
				<xsl:value-of select="$toplevelname" />
				<xsl:text>Build Components</xsl:text>
			</h3>

			<xsl:call-template name="doItems" />
		</xsl:if>
	</xsl:template>

	<xsl:template
		name="doItems"
		match="item">

		<h4>
			<xsl:value-of select="@name" />
		</h4>
		<ul>

			<xsl:for-each select="item">
				<xsl:if test="@elementID">
					<li>
						<xsl:value-of select="substring(@elementID, 2)" />
					</li>
				</xsl:if>
				<xsl:if test="@path">
					<li>
						<xsl:value-of select="substring(@path, 2)" />
					</li>
				</xsl:if>
			</xsl:for-each>
		</ul>

	</xsl:template>


</xsl:stylesheet>