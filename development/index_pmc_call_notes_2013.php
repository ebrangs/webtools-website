<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon()); # All on the same line to unclutter the user's desktop'
$pageKeywords	= "PMC Meetings Eclipse Web Tools Platform WTP";
$pageAuthor		= "David Williams";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$pageTitle = "Eclipse Web Tools Platform PMC Meetings";

$xmlString = <<<EOXML
<html>
<body>
<h1>$pageTitle</h1>
<h2>Call Info</h2>

<p>Toll free (in US and Canada): 888-426-6840 <br />
Access code: 18184877# <br />
Caller pay alternative number: 215-861-6239<br />
<a
	href="https://www.teleconference.att.com/servlet/glbAccess?process=1&accessCode=6460142&accessNumber=2158616239">Full
list of global access numbers</a><br />
<br />
<p><a href="http://www.timeanddate.com/worldclock/custom.html?cities=224,207,1440,107&amp;hour=11&amp;min=0&amp;sec=0&amp;p1=207">Call
Time: 1500 UTC</a></p>

<h2>Meeting agendas and minutes</h2>
<ul compact="true">

<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-12-10">December 10, 2013</a></li>
<li>December 3, 2013 - No meeting this week</li>
<li>November 26, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-11-19">November 19, 2013</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-11-12">November 12, 2013</a></li>
<li>October 29, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-10-22">October 22, 2013</a></li>
<li>October 15, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-10-08">October 8, 2013</a></li>
<li>October 1, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-09-24">September 24, 2013</a></li>
<li>September 17, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-09-10">September 10, 2013</a></li>
<li>September 3, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-08-27">Aug 27, 2013</a></li>
<li>Aug 20, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-08-13">Aug 13, 2013</a></li>
<li>Aug 6, 2013 - No meeting this week</li>
<li>July 30, 2013 - No meeting this week</li>
<li>July 23, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-07-16">July 16, 2013</a></li>
<li>July 2, 2013 - No meeting this week</li>
<li>June 25, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-06-18">June 18, 2013</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-06-04">June 4, 2013</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-05-28">May 28, 2013</a></li>
<li>May 21, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-05-14">May 14, 2013</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-05-07">May 7, 2013</a></li>
<li>April 30, 2013 - No meeting this week</li>
<li>April 23, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-04-16">April 16, 2013</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-04-09">April 9, 2013</a></li>
<li>April 2, 2013 - No meeting this week</li>
<li>March 27, 2013 - Combined Eclipsecon breakfast - telecon  (Details TBD)</li>
<li>March 19, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-03-12">March 12, 2013</a></li>
<li>March 5, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-02-26">February 26, 2013</a></li>
<li>February 12, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-02-05">February 5, 2013</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-01-29">January 29, 2013</a></li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-01-22">January 22, 2013</a></li>
<li>January 15, 2013 - No meeting this week</li>
<li><a href="pmc_call_notes/pmcMeeting.php?meetingDate=2013-01-08">January 8, 2013</a></li>

<li><a href="index_pmc_call_notes_2012.php">2012</a></li>
<li><a href="index_pmc_call_notes_2011.php">2011</a></li>
<li><a href="index_pmc_call_notes_2010.php">2010</a></li>
<li><a href="index_pmc_call_notes_2009.php">2009</a></li>
<li><a href="index_pmc_call_notes_2008.php">2008</a></li>
<li><a href="index_pmc_call_notes_2007.php">2007</a></li>
<li><a href="index_pmc_call_notes_2006.php">2006</a></li>
<li><a href="index_pmc_call_notes_2005.php">2005</a></li>
<li><a href="index_pmc_call_notes_2004.php">2004</a></li>
</ul>
<hr />
<!-- <p>Back to <a href="/webtools/development/index_pmc_call_notes.php">meeting list</a>.</p>
-->
<p>Please send any additions or corrections to <a href="mailto: david_williams@us.ibm.com">David Williams.</a></p>
</body>
</html>
EOXML;

$xml = DOMDocument::loadHTML($xmlString);
// Load the XSL source
//$xsl = DOMDocument::load($root . '/webtools/wtpphoenix.xsl');
// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/wtpnova.xsl');
// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

//echo "xml:";
//echo $xml->saveXML();
$maincontent = $proc->transformToXml($xml);
//echo "maincontent:";
//echo $maincontent;
$html = <<<EOHTML
<div id="maincontent">
$wtpTopButtons
$maincontent
</div>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
