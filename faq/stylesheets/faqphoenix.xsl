<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xalan="http://xml.apache.org/xslt"
    xmlns:faq="http://www.eclipse.org/webtools/faq" xmlns:date="http://exslt.org/dates-and-times"
    exclude-result-prefixes="date faq xalan xsl">
    
    <xsl:output method="xml" omit-xml-declaration="yes" encoding="UTF-8" />
   
    <xsl:template match="/">
        <xsl:apply-templates mode="faqpage" />
    </xsl:template>
    
    <xsl:template match="faq:faq" mode="faqpage">
        <table>
            <tr>
                <td width="60%">
                    <h1>
                        <xsl:value-of select="@name" />
                    </h1>
                    <div class="wtpsubtitle">
                        <xsl:value-of select="'frequently asked questions and answers'" />
                    </div>
                </td>
                <td>
                    <img src="/webtools/images/wtplogosmall.jpg" align="middle" height="129" hspace="50" width="207" />
                </td>
            </tr>
        </table>
        <xsl:apply-templates select="." />
    </xsl:template>
    
    <xsl:template name="aboutFAQ">
        <xsl:call-template name="heading">
            <xsl:with-param name="title" select="'About the FAQ'" />
            <xsl:with-param name="alink" select="''" />
        </xsl:call-template>
        <!--
        We would like "last updated" info here, but we are dynamically generating the page.
        Can we get the date the xml doc last changed ?
        -->
        <xsl:if test="/faq:faq/faq:about">
            <p>
                <xsl:apply-templates select="/faq:faq/faq:about" mode="body" />
            </p>
        </xsl:if>
        <p>
            For problems with this FAQ please open a
            <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools&amp;component=FAQ">bug report</a>
            .
        </p>
    </xsl:template>
    
    <xsl:template name="endpage">
        <xsl:param name="root" />
        <p></p>
        <p>
            Please see our
            <a href="http://www.eclipse.org/legal/privacy.html">privacy policy</a>
            and website
            <a href="http://www.eclipse.org/legal/termsofuse.html">terms of use</a>
            . For problems with the eclipse.org site, please contact the
            <a href="mailto:webmaster@eclipse.org">webmaster</a>
            or read the
            <a href="{$root}/../webmaster/index.html">webmaster FAQ</a>
            for answers to common questions!
        </p>
    </xsl:template>
    
    <xsl:template name="heading">
        <xsl:param name="alink" />
        <xsl:param name="title" />
        <xsl:param name="toplink" />
        <h2 class="bar">
            <span class="left">
                <a name="{$alink}">
                    <xsl:value-of select="$title" />
                </a>
            </span>
            <xsl:if test="$toplink != ''">
                <span class="right">
                    <a href="{$toplink}">top</a>
                </span>
            </xsl:if>
            <div class="clearall" />
        </h2>
    </xsl:template>
    
    <xsl:template match="faq:faq">
        <xsl:param name="root" />
        <xsl:call-template name="aboutFAQ" />
        <!-- Create the TOC. -->
        <xsl:call-template name="heading">
            <xsl:with-param name="title" select="'Table of Contents'" />
            <xsl:with-param name="alink" select="'toc'" />
        </xsl:call-template>
        <ul class="indent">
            <xsl:for-each select="faq:category">
                <li>
                    <a href="#{@name}">
                        <xsl:value-of select="@name" />
                    </a>
                </li>
                <xsl:variable name="catname" select="@name" />
                <ol>
                    <xsl:for-each select="../faq:entry[@category=$catname]">
                        <xsl:sort select="@id" data-type="text" order="ascending" />
                        <li>
                            <a href="#{@id}">
                                <xsl:value-of select="faq:question" />
                            </a>
                        </li>
                    </xsl:for-each>
                </ol>
                <br />
            </xsl:for-each>
        </ul>
        <!-- Create the answer sections. -->
        <xsl:for-each select="faq:category">
            <xsl:call-template name="heading">
                <xsl:with-param name="title" select="@name" />
                <xsl:with-param name="alink" select="@name" />
<!--                 <xsl:with-param name="toplink" select="'#toc'" /> -->
            </xsl:call-template>
            <xsl:variable name="catname" select="@name" />
            <ol>
                <xsl:for-each select="../faq:entry[@category=$catname]">
                    <xsl:sort select="@id" />
                    <li>
                        <a name="{@id}">
                            <b>
                                <xsl:value-of select="faq:question" />
                            </b>
                        </a>
                        <br />
                        <br />
                        <xsl:apply-templates select="faq:answer" />
                        <hr />
                        <br />
                    </li>
                </xsl:for-each>
            </ol>
        </xsl:for-each>
        <br />
        <!-- 
            <table border="0" cellpadding="2" cellspacing="5" width="100%">
            <tbody>			
            <xsl:apply-templates select="." mode="body"/>
            </tbody>
            </table>
        -->
    </xsl:template>
    
    <xsl:template match="faq:answer">
        <xsl:apply-templates select="*|@*|text()" mode="body" />
    </xsl:template>
    
    <xsl:template match="*|@*|text()" mode="faqpage"></xsl:template>
    
    <xsl:template match="*|@*|text()" mode="body">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()" mode="body" />
        </xsl:copy>
    </xsl:template>
    
    <xsl:template match="*|@*|text()"></xsl:template>
</xsl:stylesheet>
