<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

# Generate the web page
// Load the XML source
$xml = DOMDocument::load('CommitterFAQ.xml');

//Set the page title
$pageTitle = "eclipse.org webtools FAQ";
$xpath = new DOMXPath($xml);
$faqNode = $xpath->query("/faq:faq[@name]")->item(0);
if ($faqNode != null) {
	$pageTitle = $faqNode->getAttribute("name");
}

// Load the XSL source
$xsl = DOMDocument::load($root . '/webtools/faq/stylesheets/faqphoenix.xsl');

// Configure the transformer
$proc = new XSLTProcessor;
$proc->importStyleSheet($xsl); // attach the xsl rules

$html = $proc->transformToXML($xml);
$html .= $wtpTopButtons;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
