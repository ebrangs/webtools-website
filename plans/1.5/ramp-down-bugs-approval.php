<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
		<link rel="stylesheet" href="../..//default_style.css" type="text/css">
		<link rel="stylesheet" href="../..//webtools/wtp.css" type="text/css">
		<title>Web Tools 1.5 Bugs</title>
	</head>

	<body>
		<table width="100%" cellspacing="5" cellpadding="2" border="0">
			<tr>
				<td width="60%" align="left"><font class="indextop">wtp 1.5 bug list</font>
				<br>
				<font class="indexsub">Web Tools 1.5 Bugs</font></td><td width="40%"><img width="207" hspace="50" height="129" align="middle" src="../..//images/wtplogosmall.jpg"></td>
			</tr>
		</table>

		<table width="100%" cellspacing="5" cellpadding="2" border="0">
			<tr>
				<td valign="top" bgcolor="#0080c0" align="left" colspan="2"><b><font face="Arial,Helvetica" color="#ffffff">Introduction</font></b></td>
			</tr>
			<tr>
				<td valign="top" align="right">&nbsp;</td>
				<td valign="top">
					This document lists bugs that requires PMC approval in WTP 1.5.
				</td>
			</tr>
		</table>

		<table cellspacing="4" width="95%" cellpadding="5" border="1">
			<tr>
				<th>Bug</th>
				<th>Status</th>
				<th>Owner</th>
				<th>Yes</th>
				<th>No</th>
			</tr>

<?php
	require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";
	
	# Connect to database
	$sqlquery = "SELECT 
					BUG.bug_id,
					USR.realname,
					BUG.status_whiteboard
				FROM 
					bugs AS BUG 
					INNER JOIN products AS PROD ON PROD.id = BUG.product_id
					INNER JOIN profiles AS USR ON USR.userid = BUG.assigned_to 
				WHERE 
					BUG.status_whiteboard like '%PMC%'
					AND PROD.name='Web Tools' 
				ORDER BY 
					BUG.bug_id";

	$dbc = new DBConnectionBugs();
	$dbh = $dbc->connect();
	$rs = mysql_query($sqlquery, $dbh);

	if(mysql_errno($dbh) > 0)
	{
		echo "There was an error processing this request".
		$dbc->disconnect();
		exit;
	}

	while($myrow = mysql_fetch_assoc($rs))
	{
		echo "<tr>";
		echo "<td><a href=\"https://bugs.eclipse.org/bugs/show_bug.cgi?id=".$myrow['bug_id']."\">".$myrow['bug_id']."</a></td>";
		echo "<td>";
		$yes = "";
		$no = "";
		if (strpos($myrow['status_whiteboard'], 'PMC_approved') !== false)
		{
			echo "<font color=\"green\"><b>Approved</b></font>";
			$yes = $yes."--";
			$no = $no."--";
		}
		else
		{
			$sqlquery2 = "SELECT 
					USR.realname,
					COMMENT.thetext 
				FROM 
					longdescs AS COMMENT 
					INNER JOIN profiles AS USR ON USR.userid = COMMENT.who
				WHERE 
					COMMENT.bug_id=".$myrow['bug_id'];
			$rs2 = mysql_query($sqlquery2, $dbh);
			if(mysql_errno($dbh) <= 0)
			{
				$votes = 0;
				while($myrow2 = mysql_fetch_assoc($rs2))
				{
					if (strpos($myrow2['thetext'], '+1') !== false)
					{
						$yes = $yes.$myrow2['realname']."<br>";
						$votes++;
					}
					if (strpos($myrow2['thetext'], '-1 ') !== false)
					{
						$no = $no.$myrow2['realname']."<br>";
						$votes = -1;
						break;
					}
				}
				if ($votes == -1)
				{
					echo "<font color=\"red\"><b>Rejected</b></font>";
				}
				else if ($votes > 2)
				{
					echo "<font color=\"green\"><b>Approved</b></font>";
				}
				else
				{
					echo "Pending...";
				}
			}
			$rs2 = null;
		}
		echo "</td>";
		echo "<td>".$myrow['realname']."</td>";
		echo "<td>".$yes."&nbsp;</td>";
		echo "<td>".$no."&nbsp;</td>";
		echo "</tr>";
	}

	$dbc->disconnect();
	$rs = null;
	$dbh = null;
	$dbc = null;
?>

		</table>
	</body>
</html>