<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../stylesheets/wtp.xsl"?>
<html>
	<head>
        <meta name="root" content="../../../../../../" />
		<title>WTP 1.5 Milestone 5</title>
	</head>
	<body>
		<h1>HTML Test Plan</h1>
		<h2>Status of this Plan</h2>
		<p>Proposed Plan (2.24.05)</p>
		
		<h2>Overall goals</h2>
		<h3><b>New for M5</b></h3>
		<p>
			Not much has been added in terms of new functionality
			for M5.  Most of the work done was in the areas of
			refactoring, cleaning up dead code, and cleaning up APIs.
			Also more unit tests were added which are run regularly
			with the build.
		</p>
		<h3><b>Co-developer Testing</b></h3>
		<p>
			We will inspect &quot;runtime&quot; version of build to be
			sure extra source is not included, and more important, we'll
			inspect and test importing SDK version to be sure all
			relevant &quot;open source&quot; is included in that SDK
			build and that it correctly imports into a development
			environment.
		</p>
		<h3><b>API Testing</b></h3>
		<p>
			Here in M5 we don't consider we have any official API yet
			(since not spec'd as such) but will reserve this space for
			future plans to details were API Unit tests are, their
			coverage, etc.
		</p>
		<p>
			We do have several hundred unit tests which we expect to be
			running and passing for M5, which test various
			aspects of parsing, model creation, and correct charset
			handling, among other things.
		</p>
		
		<h3><b>End User Testing</b></h3>
		<p>
			The nature of the end-user
			testing is intentionally planned to be "ad hoc" instead of
			specifying step by step "how to" directions and specific
			"expected results" sections often seen in test cases. This
			is done because its felt leads to greater number of "paths"
			being tested, and allows end-users more motivation for
			logging "bugs" if things didn't work as
			<i>they</i> expected, even if it is working as designed.
		</p>

		<p>
			As we progress through milestones, we'll add more and more
			detail for special cases, special files, special projects,
			etc.When we do have special or sample test files and
			projects, we will keep those stored in CVS, as projects
			under a
			<i>testdata</i>
			directory under the
			<i>development</i>
			directory of relevant component so that testers (from
			immediate team, or community) can easily check out into the
			environment being tested.
		</p>
		
		<h3><b>Platform Testing</b></h3>
		<p>
			While we do not have any platform specific code, or
			function, we will have some team members do end-user tests
			on Linux, some on Windows. We will also confirm unit tests
			pass on both platforms.
		</p>
		
		<h3><b>Performance Testing</b></h3>
		<p>
			We have added (some) automated performance tests along the lines
			of the Eclipse base performance unit tests in future
			milestones.  These are currently in the <b>org.eclipse.wst.*.ui.tests.performance</b>
			and <b>org.eclipse.jst.jsp.ui.tests.performance</b> plugins.
			<br/>
			<br/>
			We will continue to add more test cases in upcoming milestones.		
		</p>

		<h2>HTML Tests</h2>
		<ul>
			<li>check Source Editing features from the <a href="../../sse/M5/milestone_test_plan.html#matrix">feature matrix</a></li>
			<li>
				check &quot;embedded languages&quot; (CSS, JavaScript)
				have same features as indicated in the <a href="../../sse/M5/milestone_test_plan.html#matrix">feature matrix</a>
			</li>
			<li>
				A quick sanity check that preferences have effect, and
				do no harm. Only quick check, since many changes planned
				in this are for future milestones.
			</li>
		</ul>
		
		<p>
			<ul>
				<li>
					as you type validation:
					<ul>
						<li>
							job based, make sure squiggles show up when
							they should and are removed when problems
							are fixed
						</li>
						<li>
							test various partition types in the document
						</li>
						
					</ul>
				</li>
				<li>
					html validation:
					<ul>
						<li>batch workbench validation, as-you-type</li>
					</ul>
				</li>

				<li>
					hyperlink open on:
					<ul>
						<li>Java Elements, href, includes, link, style, etc...</li>
					</ul>
				</li>

				<li>
					preferences:
					<ul>
						<li>how we use the preferences in the "All Text Editors"
					         preference page now.
					    </li>
						<li>make sure preferenes work</li>
						<li>after shutdown and restart</li>
					</ul>
				</li>
				
				<li>
					run performance unit tests:
					<ul>
						<li>
							record results for comparison with future
							milestones
						</li>
					</ul>
				</li>

				<li>
					profiling:
					<ul>
						<li>using your favorite profiler (<a href="http://eclipsefaq.org/chris/xray/index.html">XRay</a>, YourKit, etc...)
						    test basic editor functions and look for 
							problem areas (large memory consumption, intense CPU usage)
						</li>
					</ul>
				</li>
				
			</ul>
		</p>
		
		<h2>XHTML Tests</h2>
		<ul>
			<li>check Source Editing features from the <a href="../../sse/M5/milestone_test_plan.html#matrix">feature matrix</a></li>
			<li>
				check &quot;embedded languages&quot; (CSS, JavaScript)
				have same features as indicated in the <a href="../../sse/M5/milestone_test_plan.html#matrix">feature matrix</a>.
			</li>
			<li>
				Check that the standard different doctypes give
				different, correct content assist.
			</li>
			<li>
				A quick sanity check that preferences have effect, and
				do no harm. Only quick check, since many changes planned
				in this are for future milestones.
			</li>
		</ul>
		<p>
			Tab Preferences
		</p>
		<ul>
			<li>Web and XML->Content Type Files -> Content Type Source 
			-> Indent using tabs / Indent using spaces
			-> Indentation size</li>
		
			<li>Verify the correct tab character is used</li>
			<li>Verify the correct number of tab characters is used</li>
			<li>Verify Source->Shift Left/Shift Right and the Shift-Tab/Tab key follow the preferences</li>
			<li>With nothing selected</li>
			<li>With multiple lines selected</li>
			<li>Verify Format follow the preferences</li>
			<li>Verify when using tab characters, the displayed tab width preference is followed (General->Editors->Text Editors -> Displayed tab width)</li>
		</ul>
		<h2>New for M5</h2>

		New File Wizard:
		<ul>
			<li>make sure the template creates a valid file</li>
			<li>Verify not entering an extension will generate a new 
				file with the default file extension you specified in the preference.
			</li>
			<li>Verify entering a file name that already exists without the extension 
				will still give you an error saying the file already exists 
				(for example, if index.jsp already exists, typing "index" will tell you that it already exists)
			</li>
			<li>Verify entering a file name with valid/invalid extension still works</li>
		</ul>

		File Preference Page:
		<ul>
			<li>A new preference was added,  "Add this suffix (if not specified)"</li>
			<li>Verify for that you can only enter extensions that are currently 
			associated with the content type (via Content Type preference page)
			</li>
		</ul>

		<p>Content Settings</p>
		<br />General Content Settings
		<ul>
			<li>Verify project properties are inherited when file properties are not set</li>
			<li>Verify setting properties actually saves the properties settings when you 
			reopen properties dialog and when you restart workbench</li>
			<li>Verify properties still valid/saved when you move the associated file</li>
			<li>Verify nothing funny happens with build or if you have a readonly project
			or something that nothing goes wrong (since properties are stored inside project)</li>
		</ul>

		<p>JSP Fragment</p>
		<ul>
			<li>Verify JSP Fragment shows up in Properties dialog for jsp fragment files and web 
			projects (and only then)
			</li>
			<li>Verify Language/Content Type/Validation properties are used when not 
			specified in JSP Fragments
			</li>
		</ul>

		<p>Web Content Settings</p>
		<ul>
			<li>Verify Web Content Settings shows up in Properties dialog for html, 
			jsp, css files and web projects (and only then)</li>
			<li>For CSS files, there should only be a CSS Profile option.  Verify it is used
			For HTML/JSP files there should also be a Document type option.</li>
			<li>Verify it is used (for example, specify frameset doctype and 
			verify content assist proposes frameset tag)
			</li>
		</ul>
				
		<h2><b>Regression Tests</b></h2>
		<p>
			<a href="../../../../1.0/wst/sse/M8/viewerconfig-test.html">Structured Text Viewer Configuration tests</a>
			<br/><a href="../../../../1.0/wst/sse/M8/action-test.html">Structured Text Editor Action tests</a>
			<br/><a href="../../../../../wst/components/sse/M4/formatting-test.html">Formatting tests</a>
			<br/>
		</p>
		 
		<h2>Source Editing Test Plans</h2>
		<p>
			<a href="../../sse/M5/milestone_test_plan.html">org.eclipse.wst.sse</a>
			<br/><a href="../../xml/M5/milestone_test_plan.html">org.eclipse.wst.xml</a>
			<br/><a href="../../html/M5/milestone_test_plan.html">org.eclipse.wst.html</a>
			<br/><a href="../../css/M5/milestone_test_plan.html">org.eclipse.wst.css</a>
			<br/><a href="../../dtd/M5/milestone_test_plan.html">org.eclipse.wst.dtd</a>
			<br/><a href="../../javascript/M5/milestone_test_plan.html">org.eclipse.wst.javascript</a>
			
			<br/><a href="../../../jst/jsp/M5/milestone_test_plan.html">org.eclipse.jst.jsp</a>	
		</p>
	</body>
</html>
