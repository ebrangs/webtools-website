<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../stylesheets/wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../www/" />
		<title>dtd wtp 1.5 final test plan</title>
	</head>
	<body>
		<h1>dtd wtp 1.5 final test plan</h1>
		<h2>Status of this Plan</h2>
		<p>Proposed Plan (6.21.06)</p>

		<h2>Overall goals</h2>
		<h3>
			<b>Co-developer Testing</b>
		</h3>
		<p>
			We will inspect &quot;runtime&quot; version of build to be
			sure extra source is not included, and more important, we'll
			inspect and test importing SDK version to be sure all
			relevant &quot;open source&quot; is included in that SDK
			build and that it correctly imports into a development
			environment.
		</p>
		<h3>
			<b>API Testing</b>
		</h3>
		<p>
			DTD API unit tests are in the org.eclipse.wst.dtd.core.tests
			and org.eclipse.wst.dtd.ui.tests plugins, located in the
			<i>tests</i>
			folder in the wst.dtd component.
		</p>
		<p>
			We do have several hundred unit tests which we expect to be
			running and passing for 1.5, which test various aspects of
			parsing, model creation, and correct charset handling, among
			other things.
		</p>

		<h3>
			<b>End User Testing</b>
		</h3>
		<p>
			The nature of the end-user testing is intentionally planned
			to be "ad hoc" instead of specifying step by step "how to"
			directions and specific "expected results" sections often
			seen in test cases. This is done because its felt leads to
			greater number of "paths" being tested, and allows end-users
			more motivation for logging "bugs" if things didn't work as
			<i>they</i>
			expected, even if it is working as designed.
		</p>

		<p>
			As we progress through milestones, we'll add more and more
			detail for special cases, special files, special projects,
			etc.When we do have special or sample test files and
			projects, we will keep those stored in CVS, as projects
			under a
			<i>testdata</i>
			directory under the
			<i>development</i>
			directory of relevant component so that testers (from
			immediate team, or community) can easily check out into the
			environment being tested.
		</p>

		<h3>
			<b>Platform Testing</b>
		</h3>
		<p>
			While we do not have any platform specific code, or
			function, we will have some team members do end-user tests
			on Linux, some on Windows. We will also confirm unit tests
			pass on both platforms.
		</p>

		<h3>
			<b>Performance Testing</b>
		</h3>
		<p>
			We have added (some) automated performance tests along the
			lines of the Eclipse base performance unit tests in future
			milestones. These are currently in the
			<b>org.eclipse.wst.*.ui.tests.performance</b>
			and
			<b>org.eclipse.jst.jsp.ui.tests.performance</b>
			plugins
			<br />
			<br />
			We will continue to add more test cases in upcoming
			milestones.
		</p>

		<h2>DTD Tests</h2>
		<ul>
			<li>
				check Source Editing features from feature
				<a
					href="../../sse/RC5/milestone_test_plan.html#matrix">
					feature matrix
				</a>
			</li>
			<li>DTD Validation(batch only)</li>
			<li>
				test Source page, Outline view, Properties view
				synchronization
			</li>
			<li>
				preferences:
				<ul>
					<li>
						Test our editors follow preferences in the "All
						Text Editors" preference page
					</li>
					<li>
						Check that editors follow content type specific
						preferences under Web and XML preference pages
					</li>
					<li>
						Make sure editors (already open and closed and
						reopened) are updated when preferences change
					</li>
					<li>
						Check preferences are saved when shutdown and
						restart workbench
					</li>
				</ul>
			</li>

			<li>
				tab preferences:
				<ul>
					<li>
						Web and XML->Content Type Files -> Content Type
						Source -> Indent using tabs / Indent using
						spaces -> Indentation size
					</li>

					<li>Verify the correct tab character is used</li>
					<li>
						Verify the correct number of tab characters is
						used
					</li>
					<li>
						Verify Source->Shift Left/Shift Right and the
						Shift-Tab/Tab key follow the preferences
					</li>
					<li>With nothing selected</li>
					<li>With multiple lines selected</li>
					<li>Verify Format follow the preferences</li>
					<li>
						Verify when using tab characters, the displayed
						tab width preference is followed
						(General->Editors->Text Editors -> Displayed tab
						width)
					</li>
				</ul>
			</li>
			<li>
				profiling:
				<ul>
					<li>
						using your favorite profiler (
						<a
							href="http://eclipsefaq.org/chris/xray/index.html">
							XRay
						</a>
						, YourKit, etc...) test basic editor functions
						and look for problem areas (large memory
						consumption, intense CPU usage)
					</li>
				</ul>
			</li>

		</ul>
		<h2>New for 1.5</h2>


		<p>New File Wizard:</p>
		<ul>
			<li>make sure the template creates a valid file</li>
			<li>
				Verify not entering an extension will generate a new
				file with the default file extension you specified in
				the preference.
			</li>
			<li>
				Verify entering a file name that already exists without
				the extension will still give you an error saying the
				file already exists (for example, if index.jsp already
				exists, typing "index" will tell you that it already
				exists)
			</li>
			<li>
				Verify entering a file name with valid/invalid extension
				still works
			</li>
		</ul>

		<p>File Preference Page:</p>
		<ul>
			<li>
				A new preference was added, "Add this suffix (if not
				specified)"
			</li>
			<li>
				Verify for that you can only enter extensions that are
				currently associated with the content type (via Content
				Type preference page)
			</li>
		</ul>

		<p>Tabbed Property Sheets:</p>
		<ul>
			<li>
				Verify property sheets for various dtd items such as
				notations, entities, elements, attributes, comments
			</li>
			<li>
				Verify property sheet follows selection in outline view
				and dtd editor
			</li>
			<li>
				Verify when something is changed in editor/outline view,
				property sheet synchronizes with changes
			</li>
			<li>
				Verify when something is changed in property sheet,
				editor/outline view synchronizes with changes
			</li>
		</ul>
		<h2>
			<b>Regression Tests</b>
		</h2>
		<p>
			<a
				href="../../../../../wst/components/sse/tests/viewerconfig-test.html">
				Structured Text Viewer Configuration tests
			</a>
			<br />
			<a
				href="../../../../../wst/components/sse/tests/action-test.html">
				Structured Text Editor Action tests
			</a>
			<br />
			<a
				href="../../../../../wst/components/sse/tests/formatting-test.html">
				Formatting tests
			</a>
			<br />
		</p>

		<h2>Source Editing Test Plans</h2>
		<p>
			<a href="../../sse/RC5/milestone_test_plan.html">
				org.eclipse.wst.sse
			</a>
			<br />
			<a href="../../xml/RC5/milestone_test_plan.html">
				org.eclipse.wst.xml
			</a>
			<br />
			<a href="../../html/RC5/milestone_test_plan.html">
				org.eclipse.wst.html
			</a>
			<br />
			<a href="../../css/RC5/milestone_test_plan.html">
				org.eclipse.wst.css
			</a>
			<br />
			<a href="../../dtd/RC5/milestone_test_plan.html">
				org.eclipse.wst.dtd
			</a>
			<br />
			<a href="../../javascript/RC5/milestone_test_plan.html">
				org.eclipse.wst.javascript
			</a>

			<br />
			<a href="../../../jst/jsp/RC5/milestone_test_plan.html">
				org.eclipse.jst.jsp
			</a>
		</p>
	</body>
</html>
