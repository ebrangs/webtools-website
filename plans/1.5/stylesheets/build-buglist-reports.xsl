<?xml version="1.0" encoding="UTF-8"?>

<!--
	
	This stylesheet generates the Ant build file that builds the buglist reports.
	The input is the release plan which lists all the bugzilla components and target milestones.
	The output is an Ant build file that generates the buglist for each milestone of each component.
	
	2005-10-12: Arthur Ryman <ryman@ca.ibm.com>
	- Relocated to root directory.
	
	2005-10-02: Arthur Ryman <ryman@ca.ibm.com>
	- Created
	
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:output method="xml" encoding="UTF-8" indent="yes" />

	<xsl:template match="/">
		<xsl:apply-templates />
	</xsl:template>

	<xsl:template match="release">
		<project name="build-buglist-reports" default="main"
			basedir=".">

			<target name="main" description="Generates all buglist reports.">

				<xsl:apply-templates select="bz_component" />

				<xslt style="stylesheets/wtp.xsl"
					includes="**/buglist-report.xml" destdir="." force="yes" />

				<delete>
					<fileset dir="." includes="**/buglist-report.xml" />
				</delete>

			</target>

		</project>
	</xsl:template>

	<xsl:template match="bz_component">

		<xsl:variable name="bz_component" select="@name" />
		<xsl:variable name="component_dir" select="@dir" />

		<xsl:for-each select="//bz_target_milestone">

			<xsl:variable name="bz_target_milestone" select="@name" />
			<xsl:variable name="milestone_dir" select="@dir" />

			<xslt style="stylesheets/buglist-report.xsl"
				in="buglist.xml"
				out="{$component_dir}/{$milestone_dir}/buglist-report.xml"
				force="yes">
				<param name="bz_component" expression="{$bz_component}" />
				<param name="bz_target_milestone"
					expression="{$bz_target_milestone}" />
			</xslt>

		</xsl:for-each>
	</xsl:template>

</xsl:stylesheet>
