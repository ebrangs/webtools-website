<?xml version="1.0" encoding="utf-8"?>

<!-- 
	This XSLT sytlesheet transforms the Bugzilla query results to an XHTML table of unresolved bugs.
	
	Change Log:
	
	2005-12-22: Arthur Ryman <ryman@ca.ibm.com>
	- Created.
-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	version="1.0" xmlns:bz="http://www.bugzilla.org/rdf#"
	exclude-result-prefixes="bz">

	<xsl:param name="timestamp" select="'1900-01-01 00:00:00 UTC'" />

	<xsl:output method="xml" indent="yes" />

	<xsl:template match="/">
		<table cellspacing="4">
			<caption>
				Unresolved Adopter Hot Bugs as of
				<xsl:value-of select="$timestamp" />
			</caption>
			<thead>
				<tr>
					<th>ID</th>
					<th>Target Milestone</th>
					<th>Summary</th>
				</tr>
			</thead>
			<tbody>
				<xsl:for-each select="//bz:bug[bz:bug_status='NEW' or bz:bug_status='ASSIGNED' or bz:bug_status='REOPENED']">
					<xsl:sort select="bz:target_milestone"/>
					<xsl:sort select="bz:id"/>
					<tr>
						<td>
							<a
								href="https://bugs.eclipse.org/bugs/show_bug.cgi?id={bz:id}">
								<xsl:value-of select="bz:id" />
							</a>
						</td>
						<td>
							<xsl:value-of select="bz:target_milestone" />
						</td>
						<td>
							<xsl:value-of select="bz:short_short_desc" />
						</td>
					</tr>
				</xsl:for-each>
			</tbody>
		</table>
	</xsl:template>

</xsl:stylesheet>