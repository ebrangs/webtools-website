<?xml version="1.0" encoding="UTF-8"?>
<!--
 
 This stylesheet generates an index of all the buglist reports from the release.xml file.
 
 ChangeLog:
 
 2005-10-12: Arthur Ryman <ryman@ca.ibm.com>
 - Relocate to root directory.
 
 2005-10-11: Arthur Ryman <ryman@ca.ibm.com>
 - Relocate to buglist directory.
 
 2005-10-07: Arthur Ryman <ryman@ca.ibm.com>
 - Created
 
-->
<xsl:stylesheet
 version="1.0"
 xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
 xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
 xmlns:bz="http://www.bugzilla.org/rdf#"
 xmlns:nc="http://home.netscape.com/NC-rdf#"
 exclude-result-prefixes="rdf bz nc">
 <xsl:output
  method="xml"
  encoding="UTF-8"
  indent="yes" />
 <xsl:variable
  name="rel"
  select="'../'" />
 <!-- 
  This stylesheet generates:
  /1.0/buglist-report-index.xml
  
  The root directory for CSS and images is:
  /1.0/www/
  
  So relative to the buglist.xml document, root is:
  www
 -->
 <xsl:template match="/">
  <xsl:processing-instruction name="xml-stylesheet">type="text/xsl" href="stylesheets/wtp.xsl"</xsl:processing-instruction>
  <xsl:apply-templates />
 </xsl:template>
 <xsl:template match="release">
  <html>
   <head>
    <title>
     <xsl:value-of select="@name" />
     bug list report index
    </title>
    <meta
     name="root"
     content="www" />
   </head>
   <body>
    <h1>bug list reports</h1>
    <h2>Introduction</h2>
    <p>
     This page lists all the bug list reports for
     <xsl:value-of select="@name" />
     .
    </p>
    <table cellspacing="4">
     <tbody>
      <xsl:for-each select="bz_component">
       <xsl:sort select="@name" />
       <xsl:variable
        name="component-dir"
        select="@dir" />
       <tr>
        <th align="right">
         <xsl:value-of select="@name" />
        </th>
        <xsl:for-each select="../bz_target_milestone">
         <td>
          <a href="{$component-dir}/{@dir}/buglist-report.html">
           <xsl:value-of select="@milestone" />
          </a>
         </td>
        </xsl:for-each>
       </tr>
      </xsl:for-each>
     </tbody>
    </table>
   </body>
  </html>
 </xsl:template>
</xsl:stylesheet>
