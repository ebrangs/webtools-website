<?xml version="1.0" encoding="UTF-8"?>

<!--
	
	Stylesheet to generate a report for a component and milestone that combines the information from the milestone plan and bugzilla.
	
	For each bug in the given component and milestone, its information is listed along with all the work items from the milestone plans.
	
	The information in each work item is compared with the information in bugzilla, and error messages are generated in the report.
	
	The intention is that the milestone plans should include information that augments bugzilla.
	
	ChangeLog:
	
	2005-10-12: Arthur Ryman <ryman@ca.ibm.com>
	- Moved release.xml to root directory.
	
	2005-10-02: Arthur Ryman <ryman@ca.ibm.com>
	- Modified to be parameterized by a bugzilla component and target milestone.
	
	2005-09-30: Arthur Ryman <ryman@ca.ibm.com>
	- Created
	
-->

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
	xmlns:bz="http://www.bugzilla.org/rdf#"
	xmlns:nc="http://home.netscape.com/NC-rdf#"
	exclude-result-prefixes="rdf bz nc">

	<xsl:output method="xml" encoding="UTF-8" indent="yes" />
	<xsl:include href="milestone-common.xsl" />

	<xsl:key name="bugs-by-priority" match="bz:bug" use="bz:priority" />

	<!-- bz_component is the bugzilla component name -->
	<xsl:param name="bz_component" />

	<!-- bz_target_milestone is the bugzilla target milestone -->
	<xsl:param name="bz_target_milestone" />

	<xsl:variable name="rel" select="'../../../'" />

	<!-- plans is the list of all plans that have a location attribute -->
	<xsl:variable name="plans"
		select="document('../reports/report-milestone-overview.xml')//plan[@location]" />

	<!-- bug_items is the list of all items in all plans that have a bug attribute -->
	<xsl:variable name="bug_items"
		select="document($plans/@location)//item[@bug]" />

	<!-- bz_product is the bugzilla product name, e.g. Web Tools -->
	<xsl:variable name="bz_product"
		select="document('../release.xml')/release/bz_product/@name" />

	<!-- release, e.g. 1.0 for WTP 1.0 -->
	<xsl:variable name="release"
		select="document('../release.xml')/release/@number" />

	<!-- subproject, e.g. wst -->
	<xsl:variable name="subproject"
		select="document('../release.xml')//bz_component[@name=$bz_component]/@subproject" />

	<!-- component, e.g. command -->
	<xsl:variable name="component"
		select="document('../release.xml')//bz_component[@name=$bz_component]/@component" />

	<!-- milestone, e.g. M8 -->
	<xsl:variable name="milestone"
		select="document('../release.xml')//bz_target_milestone[@name=$bz_target_milestone]/@milestone" />

	<!-- 
		This stylesheet generates files like:
		/1.0/wst/command/M8/buglist-report.xml
		
		The root directory for CSS and images is:
		/1.0/www/
		
		So relative to the buglist-report.xml document, root is:
		../../../www
	-->

	<xsl:template match="/">
		<xsl:processing-instruction name="xml-stylesheet">
			type="text/xsl" href="../../../stylesheets/wtp.xsl"
		</xsl:processing-instruction>

		<html>
			<head>
				<title>
					<xsl:value-of
						select="concat($bz_component, ' ', $bz_target_milestone, ' bug list report')" />
				</title>
				<meta name="root" content="../../../www" />
			</head>
			<body>
				<h1>
					<xsl:value-of
						select="concat($bz_component, ' ', $bz_target_milestone, ' bug list report')" />
				</h1>
				<h2>Introduction</h2>
				<p>
					This report lists the bugs for the
					<xsl:value-of select="$bz_component" />
					component of WTP that have the target milestone
					<xsl:value-of select="$bz_target_milestone" />
					. The bugs are sorted by priority and each bug
					includes a list of all work items that are
					associated with it. Each associated work item
					information is checked for consistency with its bug
					and any discrepencies are noted.
				</p>
				<h3>
					<xsl:variable name="p"
						select="translate($bz_product,' ','+')" />
					<xsl:variable name="c"
						select="translate($bz_component,' ','+')" />
					<xsl:variable name="m"
						select="translate($bz_target_milestone,' ','+')" />
					<a
						href="https://bugs.eclipse.org/bugs/buglist.cgi?product={$p}&amp;component={$c}&amp;target_milestone={$m}">
						<xsl:value-of
						select="concat($bz_component, ' ', $bz_target_milestone, ' Bug List')" />
					</a>
				</h3>
				<br />
				<xsl:apply-templates />

				<xsl:call-template name="writeLegend">
					<xsl:with-param name="rel" select="$rel" />
				</xsl:call-template>

			</body>
		</html>
	</xsl:template>

	<xsl:template match="rdf:RDF">

		<!-- write the bugs in priority order -->
		<xsl:for-each select="//bz:bug">
			<xsl:sort select="bz:priority" />

			<!-- group the bugs by priority -->
			<xsl:if
				test="generate-id()=generate-id(key('bugs-by-priority',bz:priority)[1])">

				<!-- start a new priority group -->
				<xsl:variable name="bz_priority" select="bz:priority" />
				<xsl:variable name="bugs"
					select="//bz:bug[bz:priority=$bz_priority and bz:component=$bz_component and bz:target_milestone=$bz_target_milestone]" />
				<xsl:if test="$bugs">
					<h2>
						<xsl:text>Priority:&#160;</xsl:text>
						<xsl:value-of select="bz:priority" />
					</h2>
					<br />

					<xsl:for-each select="$bugs">
						<xsl:sort select="bz:id" />
						<xsl:apply-templates select="." />
					</xsl:for-each>

				</xsl:if>
			</xsl:if>
		</xsl:for-each>

	</xsl:template>

	<!-- Write the bug detail -->

	<xsl:template match="bz:bug">
		<h3>
			<xsl:text>[</xsl:text>
			<a href="{@rdf:about}">
				<xsl:value-of select="bz:id" />
			</a>
			<xsl:text>]&#160;</xsl:text>
			<b>
				<xsl:value-of select="bz:short_short_desc" />
			</b>
		</h3>

		<span>
			<xsl:apply-templates
				select="bz:keywords | bz:bug_severity | bz:bug_status | bz:assigned_to | bz:resolution" />
		</span>

		<xsl:variable name="bz_id" select="bz:id" />
		<xsl:if test="$bug_items[@bug=$bz_id]">
			<span>Milestone plan items:</span>
			<ul>
				<xsl:for-each select="$bug_items[@bug=$bz_id]">
					<xsl:apply-templates select="." />
				</xsl:for-each>
			</ul>
		</xsl:if>
	</xsl:template>

	<xsl:template
		match="bz:keywords | bz:bug_severity | bz:bug_status | bz:assigned_to | bz:resolution">
		<xsl:if test="string-length()">
			<xsl:value-of select="." />
			<xsl:text>&#32;</xsl:text>
		</xsl:if>
	</xsl:template>

	<!-- Process items in milestone_plan.xml that refer to this bug -->

	<xsl:template match="item">
		<xsl:variable name="item_subproject"
			select="ancestor::component[1]/@subproject" />
		<xsl:variable name="item_component"
			select="ancestor::component[1]/@name" />
		<xsl:variable name="item_milestone"
			select="ancestor::component[1]/milestone[1]/@name" />
		<xsl:variable name="item_category"
			select="ancestor::category[1]/@name" />
		<xsl:variable name="url"
			select="concat($rel, $item_subproject, '/', $item_component, '/', $item_milestone, '/', 'milestone_plan.html')" />
		<xsl:variable name="fragment"
			select="translate(concat($item_category,'.',description),' ','-')" />
		<li>
			<xsl:call-template name="drawStatus">
				<xsl:with-param name="rel" select="$rel" />
				<xsl:with-param name="value" select="@status" />
				<xsl:with-param name="defaultImage"
					select="'default.gif'" />
			</xsl:call-template>
			<xsl:value-of select="' ['" />
			<a href="{concat($url,'#',$fragment)}">
				<xsl:value-of
					select="concat($item_subproject,'.',$item_component,' ',$release,' ',$item_milestone)" />
			</a>
			<xsl:value-of select="'] '" />

			<xsl:value-of select="description" />
			<xsl:if test="@helpWanted">
				<xsl:value-of select="'  '" />
				<xsl:call-template name="drawHelpWanted">
					<xsl:with-param name="rel" select="$rel" />
				</xsl:call-template>
			</xsl:if>
			<xsl:if test="$item_subproject!=$subproject">
				<span style="color: red">
					[The item belongs to a different subproject!]
				</span>
			</xsl:if>
			<xsl:if test="$item_component!=$component">
				<span style="color: red">
					[The item belongs to a different component!]
				</span>
			</xsl:if>
			<xsl:if test="$item_milestone!=$milestone">
				<span style="color: red">
					[The item is planned for a different milestone!]
				</span>
			</xsl:if>
		</li>
	</xsl:template>

	<xsl:template name="writeLegend">
		<xsl:param name="rel" />
		<xsl:variable name="prefix"
			select="concat($rel, 'stylesheets/images/')" />
		<h2>Legend</h2>
		<table cellspacing="5" cellpadding="2" width="100%"
			border="0">
			<tbody>
				<tr>
					<td valign="top">
						<img height="5" width="14">
							<xsl:attribute name="src">
								<xsl:value-of
									select="concat($prefix, 'progress.gif')" />
							</xsl:attribute>
						</img>
					</td>
					<td width="50%">item is under development.</td>
					<td valign="top">
						<img height="10" width="10">
							<xsl:attribute name="src">
								<xsl:value-of
									select="concat($prefix, 'glass.gif')" />
							</xsl:attribute>
						</img>
					</td>
					<td width="50%">item is under investigation.</td>
				</tr>
				<tr>
					<td valign="top">
						<img height="10" width="10">
							<xsl:attribute name="src">
								<xsl:value-of
									select="concat($prefix, 'ok.gif')" />
							</xsl:attribute>
						</img>
					</td>
					<td width="50%">item is finished.</td>
					<td valign="top">
						<img height="12" width="40">
							<xsl:attribute name="src">
								<xsl:value-of
									select="concat($prefix, 'help.gif')" />
							</xsl:attribute>
						</img>
					</td>
					<td width="50%">help wanted</td>
				</tr>
				<tr>
					<td valign="top">
						<img height="10" width="10">
							<xsl:attribute name="src">
								<xsl:value-of
									select="concat($prefix, 'defer.gif')" />
							</xsl:attribute>
						</img>
					</td>
					<td width="50%">item is deferred.</td>
					<td valign="top">
						<font class="indexsub">
							<img>
								<xsl:attribute name="src">
									<xsl:value-of
										select="concat($prefix, 'new.gif')" />
								</xsl:attribute>
							</img>
						</font>
					</td>
					<td width="50%">new</td>
				</tr>
			</tbody>
		</table>
	</xsl:template>



</xsl:stylesheet>
