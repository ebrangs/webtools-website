<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle		= "WTP Community";
$pageKeywords	= "community";
$pageAuthor		="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$html = <<<EOHTML
 <html>
  <body>
	$wtpTopButtons
	<div id="midcolumn" style="width: 725px ! important; padding:0 !important;">
		<div class="communityDiv">
			<div class="communityTitle"></div>
			<div class="communityBg"></div>
			<a href="/webtools/community/education/"><div class="communityEducation"></div></a>
			<a href="/webtools/community/resources/"><div class="communityResource"></div></a>
		</div>
	</div>
        <div id="rightcolumn" style="margin-right: 12px; position: relative; z-index: 2; padding-right:0 !important; float: right !important;">
        	<div class="sideitem">
            <h6>Useful Links</h6>
            <ul>
                <li><a href="http://live.eclipse.org/">Eclipse Live</a></li>
                <li><a href="http://www.eclipse.org/resources/">Eclipse Resources</a></li>
                <li><a href="http://www.eclipse.org/projects/">Eclipse Projects</a></li>
            </ul>
            </div>
        	<div class="sideitem">
            <h6>Community</h6>
            <ul>
                <li><a href="https://wiki.eclipse.org/Category:Eclipse_Web_Tools_Platform_Project">Wiki</a></li>
                <li><a href="http://local.eclipse.org/newsportal/thread.php?group=eclipse.webtools">Newsgroup</a></li>
                <li><a href="http://dev.eclipse.org/mhonarc/lists/wtp-dev/">Mailing List</a></li>
                <li><a href="http://local.eclipse.org/webtools/people/contributors.php">Contributors</a></li>
            </ul>
            </div>
        </div>
  </body>
 </html>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>