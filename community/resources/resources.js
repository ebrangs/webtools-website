function format_filter(id) {
	document.getElementById("aud_groups").style.display="none";
	document.getElementById("author_groups").style.display="none";
	document.getElementById("cat_groups").style.display="none";
	document.getElementById("format_groups").style.display="block";
	var format_groups = document.getElementById('format_groups');
	if(id=='all') {
		for(i=0;i<format_groups.childNodes.length;i++) {
			format_groups.childNodes[i].style.display="block";
		}
	} else {
		for(i=0;i<format_groups.childNodes.length;i++) {
			format_groups.childNodes[i].style.display="none";
		}
		format_groups = document.getElementById(id);
		format_groups.style.display="block";
	}
}

function aud_filter(id) {
	document.getElementById("format_groups").style.display="none";
	document.getElementById("author_groups").style.display="none";
	document.getElementById("cat_groups").style.display="none";
	document.getElementById("aud_groups").style.display="block";
	var aud_groups = document.getElementById('aud_groups');
	for(i=0;i<aud_groups.childNodes.length;i++) {
		aud_groups.childNodes[i].style.display="none";
	}
	aud_groups = document.getElementById(id);
	aud_groups.style.display="block";
}

function author_filter(id) {
	document.getElementById("format_groups").style.display="none";
	document.getElementById("aud_groups").style.display="none";
	document.getElementById("cat_groups").style.display="none";
	document.getElementById("author_groups").style.display="block";
	var author_groups = document.getElementById('author_groups');
	for(i=0;i<author_groups.childNodes.length;i++) {
		author_groups.childNodes[i].style.display="none";
	}
	author_groups = document.getElementById(id);
	author_groups.style.display="block";
}

function cat_filter(id) {
	document.getElementById("format_groups").style.display="none";
	document.getElementById("aud_groups").style.display="none";
	document.getElementById("author_groups").style.display="none";
	document.getElementById("cat_groups").style.display="block";
	var cat_groups = document.getElementById('cat_groups');
	for(i=0;i<cat_groups.childNodes.length;i++) {
		cat_groups.childNodes[i].style.display="none";
	}
	cat_groups = document.getElementById(id);
	cat_groups.style.display="block";
}

function showhide(id){
	if (document.getElementById){
			obj = document.getElementById(id);
		if (obj.style.display == "none"){
			obj.style.display = "block";
		} else {
			obj.style.display = "none";
		}
	}
}