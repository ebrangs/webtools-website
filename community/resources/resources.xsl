<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
xmlns:wtp="http://www.eclipse.org/webtools/education/schemas/resources.xsd"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="wtp:resources">

<style>
p, table, td {padding:0px !important; margin: 0px !important;}
</style>
  <div id="maincontent" style="width:880px; margin:0 auto;">
	<div class="header_top">
		<table width="890" height="110" border="0" cellpadding="0" cellspacing="0">
		  <tr>
		    <td height="60"></td>
		  </tr>
		  <tr>
		    <td height="50">
		    <div style="padding-left: 565px;">
			<a href="#" onClick="format_filter('all')"><img src="./img/all.png"/></a>
			<a href="#" onclick="format_filter('article')"><img src="./img/article.png"/></a>
			<a href="#" onclick="format_filter('book')"><img src="./img/book.png"/></a>
			<a href="#" onclick="format_filter('presentation')"><img src="./img/presentation.png"/></a>
			<a href="#" onclick="format_filter('tutorial')"><img src="./img/tutorial.png"/></a>
			</div>
			</td>
		  </tr>
		</table>
	</div>

	<div class="header_right" align="center">
		<table width="210" border="0" cellpadding="0" cellspacing="0" style="background: url('img/header_bg_right.png') repeat-x;">
		<tr>
			<td colspan="2" align="center"><strong>Target Audience</strong></td>
		</tr>
		<tr>
			<td width="105" align="center"><a href="#" onclick="aud_filter('users')"><img src="./img/users.png"/></a></td>
			<td width="105" align="center"><a href="#" onclick="aud_filter('adopters')"><img src="./img/adopters.png"/></a></td>
		</tr>
		</table>
		<table width="210" border="0" cellpadding="0" cellspacing="0" style="background: url('img/header_bg_right.png') repeat-x;">
		<tr>
			<td width="210" align="center"><strong>Categories</strong></td>
		</tr>
		<tr>
			<td width="210" align="center"><a href="#" onclick="cat_filter('general')">General</a> ,<a href="#" onclick="cat_filter('j2ee')">J2EE</a> ,<a href="#" onclick="cat_filter('ws')">Web Services</a> ,<a href="#" onclick="cat_filter('sse')">SSE</a></td>
		</tr>
		<tr>
			<td width="210" align="center"><a href="#" onclick="cat_filter('rdb')">Data Tools</a> ,<a href="#" onclick="cat_filter('server')">Server/Internet</a> ,<a href="#" onclick="cat_filter('xml')">XML</a></td>
		</tr>
		<tr>
			<td width="210" align="center"><a href="#" onclick="cat_filter('server')">Validation</a></td>
		</tr>
		</table>
		<table width="210" border="0" cellpadding="0" cellspacing="0" style="background: url('img/header_bg_right.png') repeat-x;">
		<tr>
			<td colspan="3" align="center"><strong>Authors</strong></td>
		</tr>
		<tr>
		   <td width="210" align="center">
		   <table  width="210" border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td align="center">
             
             <xsl:for-each select="./wtp:author">
					<xsl:sort order="ascending" select="./@name"/>
					<xsl:variable name="auth_id"><xsl:value-of select="./@id"/></xsl:variable>
					<a href="#" onclick="author_filter('{$auth_id}')"><xsl:value-of select="./@name"/></a>
					<xsl:text>, </xsl:text>
			 </xsl:for-each>
             
			</td>
		   </tr>
		   </table>
		   </td>
		</tr>
		</table>

	</div>

	<div id="format_groups">
	    <ul id="All">
				<xsl:for-each select="/wtp:resources/wtp:resource">
						<xsl:sort order="descending" select="./@lastUpdated" data-type="text"/>
					<xsl:call-template name="resource-template">
						<xsl:with-param name="rsrc" select="." />
						<xsl:with-param name="filterid" select="All"/>
					</xsl:call-template>
				</xsl:for-each>
		</ul>
			
		<xsl:for-each select="/wtp:resources/wtp:format[@id != 'All']">
		<xsl:variable name="format_id"><xsl:value-of select="@id"/></xsl:variable>
			<ul>
				<xsl:attribute name="id">
					<xsl:value-of select="$format_id"/>
				</xsl:attribute>
				<xsl:for-each select="/wtp:resources/wtp:resource[@format=$format_id]">
						<xsl:sort order="descending" select="./@lastUpdated" data-type="text"/>
					<xsl:call-template name="resource-template">
						<xsl:with-param name="rsrc" select="." />
						<xsl:with-param name="filterid" select="$format_id"/>
					</xsl:call-template>
				</xsl:for-each>
			</ul>
		</xsl:for-each>
	</div>
	
	<div id="aud_groups">
		<xsl:for-each select="/wtp:resources/wtp:audience">
		<xsl:variable name="aud_id"><xsl:value-of select="@id"/></xsl:variable>
			<ul>
				<xsl:attribute name="id">
					<xsl:value-of select="$aud_id"/>
				</xsl:attribute>				
				<xsl:for-each select="/wtp:resources/wtp:resource[@audience=$aud_id]">
						<xsl:sort order="descending" select="./@lastUpdated" data-type="text"/>
					<xsl:call-template name="resource-template">
						<xsl:with-param name="rsrc" select="." />
						<xsl:with-param name="filterid" select="$aud_id"/>
					</xsl:call-template>
				</xsl:for-each>
			</ul>
		</xsl:for-each>
	</div>
	
	<div id="author_groups">
		<xsl:for-each select="/wtp:resources/wtp:author">
		<xsl:variable name="author_id"><xsl:value-of select="@id"/></xsl:variable>
			<ul>
				<xsl:attribute name="id">
					<xsl:value-of select="$author_id"/>
				</xsl:attribute>				
				<xsl:for-each select="/wtp:resources/wtp:resource[./wtp:author/@id=$author_id]">
						<xsl:sort order="descending" select="./@lastUpdated" data-type="text"/>
					<xsl:call-template name="resource-template">
						<xsl:with-param name="rsrc" select="." />
						<xsl:with-param name="filterid" select="$author_id"/>
					</xsl:call-template>
				</xsl:for-each>
			</ul>
		</xsl:for-each>
	</div>
	
	<div id="cat_groups">
		<xsl:for-each select="/wtp:resources/wtp:category">
		<xsl:variable name="cat_id"><xsl:value-of select="@id"/></xsl:variable>
			<ul>
				<xsl:attribute name="id">
					<xsl:value-of select="$cat_id"/>
				</xsl:attribute>				
				<xsl:for-each select="/wtp:resources/wtp:resource[./wtp:category/@id=$cat_id]">
						<xsl:sort order="descending" select="./@lastUpdated" data-type="text"/>
					<xsl:call-template name="resource-template">
						<xsl:with-param name="rsrc" select="." />
						<xsl:with-param name="filterid" select="$cat_id"/>
					</xsl:call-template>
				</xsl:for-each>
			</ul>
		</xsl:for-each>
	</div>
  </div>

</xsl:template>

<xsl:template name="resource-template" >
    <xsl:param name="rsrc"/>
	<xsl:param name="filterid"/>
	
	<xsl:variable name="resid" select="position()" />

	<div class="lefttable">
	
	<div class="title">
		<div class="titleclick">
			<table border="0" cellpadding="0" cellspacing="0" height="32">
			<tr class="titleclickfont">
				<td><a href="javascript:showhide('{$filterid}_{$resid}');">
				<xsl:value-of select="$rsrc/@title" /></a></td>
			</tr>
			<tr class="auth">
				<td>By:<xsl:value-of select="$rsrc/wtp:author[1]/@name" />
				        <xsl:text>   </xsl:text> 
						<xsl:value-of select="$rsrc/wtp:author[2]/@name" />
				        <xsl:text>   </xsl:text> 
				        <xsl:value-of select="$rsrc/wtp:author[3]/@name" />
					    <xsl:text>   </xsl:text> 
						<xsl:value-of select="$rsrc/wtp:author[4]/@name" />
				</td>
			</tr>
			</table>
		</div>
		
        <div style="display: none" id="{$filterid}_{$resid}" class="invis">
			<xsl:value-of select="$rsrc/wtp:abstract" />
				<ul>
					<li><span class="article_seperator"/></li>
					<li class="link"><font size="1" color="#333366"><strong>Link: </strong><a>
							<xsl:attribute name="href">
							<xsl:value-of select="./@link" />
							</xsl:attribute>
						<xsl:value-of select="$rsrc/@link"/></a></font></li>
				</ul>
		</div>

		<div class="alt">
			<table class="alt2" width="595px" height="50" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td width="45px">
						<xsl:choose>
							<xsl:when test="@format='article'">
							<img src="img/article_small.png" alt="Article" align="middle"/></xsl:when>
							<xsl:when test="@format='book'">
							<img src="img/book_small.png" alt="Book" align="middle"/></xsl:when>
							<xsl:when test="@format='presentation'">
							<img src="img/presentation_small.png" alt="Presentation" align="middle"/></xsl:when>
							<xsl:when test="@format='tutorial'">
							<img src="img/tutorial_small.png" alt="Tutorial" align="middle"/></xsl:when>
						</xsl:choose>
					</td>
					<td width="175px">
						<ul>
							<li><font STYLE="font-size:8pt; color:#336"><strong>Format: </strong></font><xsl:value-of select="$rsrc/@format" /></li>
							<li><font STYLE="font-size:8pt; color:#336"><strong>Categories: </strong></font><xsl:value-of select="$rsrc/wtp:category[1]/@id" /><xsl:text> </xsl:text><xsl:value-of select="$rsrc/wtp:category[2]/@id" /><xsl:text> </xsl:text><xsl:value-of select="$rsrc/wtp:category[3]/@id" /></li>
						</ul>
					</td>
					<td width="175px">
						<ul>
							<li><font STYLE="font-size:8pt; color:#336"><strong>Audience: </strong></font><xsl:value-of select="$rsrc/@audience" /></li>
							<li><font STYLE="font-size:8pt; color:#336"><strong>Last Updated: </strong></font><xsl:value-of select="$rsrc/@lastUpdated" /></li>
						</ul>
					</td>
				</tr>
				<tr>
					<td colspan="3" width="395px">Published at: 
						<font size="1"><a><xsl:attribute name="href">
						<xsl:value-of select="$rsrc/wtp:publication/@link" />
						</xsl:attribute>
						<xsl:value-of select="$rsrc/wtp:publication/@name" /></a></font>
					</td>
				</tr>
			</table>
		</div>
	</div>
	</div>

</xsl:template>

</xsl:stylesheet>