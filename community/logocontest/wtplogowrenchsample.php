<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

	#*****************************************************************************
	#
	# template.php
	#
	# Author: 		Denis Roy
	# Date:			2005-06-16
	#
	# Description: Type your page comments here - these are not sent to the browser
	#
	#
	#****************************************************************************
	
	#
	# Begin: page-specific settings.  Change these. 
	$pageTitle 		= "Web Tools Platform (WTP) Project";
	$pageKeywords	= "Type, page, keywords, here";
	$pageAuthor		= "Type your name here";
	
	$root = $_SERVER['DOCUMENT_ROOT'];
	// Import the functions that render news.
	// require_once ($root . '/webtools/news.php');
	// Import common WTP functions.
	require_once ($root . '/webtools/common.php');
	require_once ($root . '/community/news/scripts/news.php');
	
	
	function getWTPNews($rssfile, $count) 
	{
		$rss = get_news($rssfile);
//		if($rss->channel == "")
//		{
//		  return "empty";
//		}
//		return $rss;
		foreach ($rss->channel as $channel) 
		{
			$html .= "<h6>";
		
			// Add the RSS image on the right
			$html .= "<a href=\"/home/$rssfile\"><img src=\"/images/rss.gif\" align=\"right\" alt=\"More...\" /></a>";
		
			// Add the title of the channel 
			$html .= "WTP News";	
			// If we're displaying short format, provide a link to
			// show news in long format.
			$html .= "&nbsp;<a href=\"/webtools/news.html\"><img src=\"https://eclipse.org/images/more.gif\" title=\"More...\" alt=\"More...\" /></a>";

			$html .= "</h6>";
		
//		$description = $channel->description;
//		if (strlen($description) > 0) {
//			$html .= $description;
//			}
			$html .= "<ul class=\"midlist\">";

			foreach ($channel->item as $item) 
			{
				if ($count == 0) break;
				$html .= "<li>";
				$date = date("d&#8209;m&#8209;Y", $item->pubDate);
				$html .= "<a href=\"$item->link\" target=\"_blank\">$item->title</a> posted&nbsp;$date<br/>";
				$html .= $item->description;
				$html .= "</li>";
				$count--;
				if(count != 0)
				{
					$html .= "<br/>";
				}
			}

			$html .= "</ul>";
		}
		return $html;
	}
	
	// Generate the HTML content for the news based on the rss files
	// that appear in the root directory.
	$wtpnews = getWTPNews($root . "/webtools/wtpnews.rss", 5);//xml_to_html("news", "news-rss20", "long", "5");
	// Get the project descriptions
	$wtpoverview = read_file('../../project-info/description.html');
	$wstoverview = read_file('../../project-info/wst-description.html');
	$jstoverview = read_file('../../project-info/jst-description.html');
	$jsfoverview = read_file('../../project-info/jsf-description.html');
	# Add page-specific Nav bars here
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# $Nav->addNavSeparator("My Page Links", 	"downloads.php");
	# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
	# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

	# End: page-specific settings
	#
		
	# Paste your HTML content between the EOHTML markers!	
	$html = <<<EOHTML

<div id="maincontent">
	<div id="midcolumn">
		<img src="https://bugs.eclipse.org/bugs/attachment.cgi?id=35360" align="right"/>
		<h1>$pageTitle</h1>
		<p>
			$wtpoverview
			<br /> <a href="about.html">more about wtp &raquo;</a><br/><br/><br/>
 		</p>
		
		<div class="homeitem3col">
			<h3>Subprojects</h3>
			<ul>
				<li>
					<b>Web Standard Tools (WST)</b>&nbsp;<a href="wst/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$wstoverview
					<br /> <a href="wst/">more about wst &raquo;</a>
    			</li>
    			<li>
    				<b>J2EE Standard Tools (JST)</b>&nbsp;<a href="jst/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$jstoverview
					<br /> <a href="jst/">more about jst &raquo;</a>
    			</li>
    			<li>
					<b>JavaServer Faces Tools (JSF) - incubating</b>&nbsp;<a href="jsf/"><img src="https://eclipse.org/images/more.gif" title="More..." alt="More..." /></a>
					<br/>
					$jsfoverview
					<br /> <a href="jsf/">more about jsf &raquo;</a>
    			</li>
    		</ul>
		</div>
		<p></p>
		<p></p>
	</div>
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Discover WTP</h6>
			<ul>
				<li><a href="http://download.eclipse.org/webtools/downloads/">Downloads</a></li>
				<li><a href="community/communityresources.php">Resources</a></li>
				<li><a href="https://wiki.eclipse.org/WTP_FAQ">FAQ</a></li>
				<li><a href="/newsgroups/main.html">Newsgroups</a></li>
			</ul>
		</div>
		<div class="sideitem">
			$wtpnews
		</div>
		<div align="center">
		<a href="http://www.eclipsecon.org/" target="blank"><img src="http://www.eclipsecon.org/2006/images/banner132x38.gif" alt="EclipseCON" /></a>
		</div>
	</div>
</div>


EOHTML;


	# Generate the web page
	$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
