<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:xalan="http://xml.apache.org/xalan"
	xmlns:events="http://www.eclipse.org/webtools/events"
	xmlns:date="http://exslt.org/dates-and-times"
    exclude-result-prefixes="date events xalan">

	<xsl:include href="wtp-common.xsl" />
	<xsl:include href="events.xsl"/>

	<xsl:template match="/">
		<xsl:param name="title" select="'wtp community'"/>
		<xsl:param name="subtitle" select="''"/>
		<xsl:param name="root" select="'../..'"/>
  		<xsl:call-template name="page">
    		<xsl:with-param name="element" select="events:events"/>
    		<xsl:with-param name="title" select="$title"/>
    		<xsl:with-param name="subtitle" select="$subtitle"/>
    		<xsl:with-param name="root" select="$root"/>
  		</xsl:call-template>
	</xsl:template>

	<xsl:template match="events:events" mode="communityresourcespage">
		<xsl:param name="title" select="'wtp community resources'"/>
		<xsl:param name="subtitle" select="''"/>
		<xsl:param name="root" select="'../..'"/>
  		<xsl:call-template name="page">
    		<xsl:with-param name="element" select="."/>
    		<xsl:with-param name="title" select="$title"/>
    		<xsl:with-param name="subtitle" select="$subtitle"/>
    		<xsl:with-param name="root" select="$root"/>
  		</xsl:call-template>
	</xsl:template>

	<xsl:template name="aboutPage">
		<xsl:param name="resources"/>
		<xsl:call-template name="heading">
			<xsl:with-param name="title" select="'Activities and resources from around the WTP community...'"/>
			<xsl:with-param name="alink" select="'top'"/>
		</xsl:call-template>
		<table cellspacing="5" cellpadding="2" border="0">
			<tr>
				<td with="16">&#160;&#160;&#160;&#160;</td>
				<td>
					<p>
			The page is about finding more information about WTP from around the WTP community. 
        	The community has provided many <xsl:apply-templates select="xalan:nodeset($resources)" mode="body"/> 
			that are dedicated to WTP and can help you to make the most of the WTP platform and tools.
					</p>
        			<p>
			We'd like these pages to be useful to you - so please open a 
			<a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools&amp;component=website">bug report</a> 
			for any corrections, additions, pointers, or comments!
       			 	</p>
					<p>
        	For software licensing, website terms of use, and legal FAQs, please see our 
			<a href="http://eclipse.org/legal/main.html" target="_top">legal stuff</a> page.
					</p>
				</td>
				<td width="16">&#160;</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="endpage">
		<xsl:param name="root"/>
		<p></p>
        <p>Please see our <a href="http://www.eclipse.org/legal/privacy.html">privacy 
        policy</a> and website <a href="http://www.eclipse.org/legal/termsofuse.html">terms 
        of use</a>. For problems with the eclipse.org site, please contact the 
        <a href="mailto:webmaster@eclipse.org">webmaster</a> or read the 
        <a href="{$root}/../webmaster/index.html">webmaster FAQ</a> for answers to common questions!</p>
	</xsl:template>
	
	<xsl:template name="heading">
		<xsl:param name="alink"/>
		<xsl:param name="title"/>
		<xsl:param name="toplink"/>
		<table bgcolor="#0080c0" border="0" cellpadding="2" cellspacing="0" width="100%">
			<tbody>
				<tr>
					<td width="90%">
						<b>
							<font color="#ffffff" face="Arial,Helvetica">
								<a name="{$alink}"><xsl:value-of select="$title"/></a>
							</font>
						</b>
					</td>
					<td width="10%" align="right">
						<xsl:if test="$toplink != ''">
							<a href="{$toplink}">top</a>
						</xsl:if>
					</td>
				</tr>
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="events:events">
		<xsl:param name="root"/>
		
		<xsl:variable name="resources"><xsl:for-each select="document('../communityresources.xml')/resources/resource"><xsl:sort select="@shortTitle"/><xsl:choose><xsl:when test="position() = last()">and <a href="communityresources.html#{@shortTitle}"><xsl:value-of select="@shortTitle"/></a></xsl:when><xsl:otherwise><a href="communityresources.html#{@shortTitle}"><xsl:value-of select="@shortTitle"/></a>, </xsl:otherwise></xsl:choose></xsl:for-each></xsl:variable>
		<xsl:call-template name="aboutPage">
			<xsl:with-param name="resources" select="$resources"/>
		</xsl:call-template>

		<!-- Create page sections. -->
		<xsl:call-template name="heading">
			<xsl:with-param name="title" select="'This Month at a Glance'"/>
			<xsl:with-param name="alink" select="'thismonth'"/>
			<xsl:with-param name="toplink" select="'#top'"/>
		</xsl:call-template>
		<br/>
		&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;An overview of this month's events related to WTP.<br/>
		<xsl:call-template name="createCalendar"/>
		<br/>
		<br/>
		<xsl:call-template name="heading">
			<xsl:with-param name="title" select="'Upcoming WTP Events'"/>
			<xsl:with-param name="alink" select="'upcomingevents'"/>
			<xsl:with-param name="toplink" select="'#top'"/>
		</xsl:call-template>
		<p>
			&#160;&#160;&#160;&#160;&#160;&#160;&#160;&#160;WTP past event information can be found on the <a href="pastevents.html">past events page</a>.
		</p><!-- date:format-date(date:date(),'yyyy-MM-dd') -->
		<xsl:variable name="date" select="date:date()"/>
		<xsl:for-each select="events:event[date:year(@enddate) > date:year($date) or (date:year(@enddate) = date:year($date) and date:day-in-year(@enddate) >= date:day-in-year($date))]">
			<xsl:sort select="@startdate"/>
			<xsl:apply-templates select="."/>
		</xsl:for-each>
		<xsl:call-template name="endpage">
			<xsl:with-param name="root" select="$root"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template name="createCalendar"><!-- date:format-date(date:date(), 'MMMMMMMM') -->
		<xsl:variable name="month" select="date:monthName()"/>
		<br/>
		<center>
			<b><font size="+1"><xsl:value-of select="$month"/></font></b>
			<table border="1" width="98%">
				<thead>
					<tr>
						<td width="14%" align="center"><b>Sunday</b></td>
						<td width="14%" align="center"><b>Monday</b></td>
						<td width="14%" align="center"><b>Tuesday</b></td>
						<td width="14%" align="center"><b>Wednesday</b></td>
						<td width="14%" align="center"><b>Thursday</b></td>
						<td width="14%" align="center"><b>Friday</b></td>
						<td width="14%" align="center"><b>Saturday</b></td>
					</tr>
				</thead><!-- date:format-date(date:date(), 'M') -->
				<xsl:variable name="month" select="date:monthInYear() + 1"/><!-- date:format-date(date:date(), 'yyyy-MM-01') -->
				<xsl:variable name="firstofmonth"><xsl:value-of select="date:year()"/>-<xsl:value-of select="date:monthInYear()+1"/>-01</xsl:variable>
				<xsl:variable name="firstweekday" select="date:day-in-week($firstofmonth)"/>
				<xsl:variable name="daysinmonth">
					<xsl:choose>
						<xsl:when test="$month = 1 or $month = 3 or $month = 5 or $month = 7 or $month = 8 or $month = 10 or $month = 12">
							31
						</xsl:when>
						<xsl:when test="$month = 9 or $month = 4 or $month = 6 or $month = 11">
							30
						</xsl:when>
						<xsl:when test="$month = 2 and date:leapyear()">
							29
						</xsl:when>
						<xsl:when test="$month = 2">
							28
						</xsl:when>
					</xsl:choose>
				</xsl:variable>

				<xsl:call-template name="weekloop">
					<xsl:with-param name="startweekday" select="$firstweekday"/>
					<xsl:with-param name="startday" select="'1'"/>
					<xsl:with-param name="daysinmonth" select="number($daysinmonth)"/>
				</xsl:call-template>
			</table>
		</center>
	</xsl:template>

	<xsl:template name="weekloop">
		<xsl:param name="daysinmonth"/>
		<xsl:param name="startweekday"/>
		<xsl:param name="startday"/>

		<xsl:call-template name="createCalendarWeek">
			<xsl:with-param name="startweekday" select="$startweekday"/>
			<xsl:with-param name="startday" select="$startday"/>
			<xsl:with-param name="daysinmonth" select="$daysinmonth"/>
		</xsl:call-template>

		<xsl:if test="$daysinmonth > 8 - $startweekday + $startday">
			<xsl:call-template name="weekloop">
				<xsl:with-param name="startweekday" select="'1'"/>
				<xsl:with-param name="startday" select="8 - $startweekday + $startday"/>
				<xsl:with-param name="daysinmonth" select="$daysinmonth"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:template>

	<xsl:template name="createCalendarWeek">
		<xsl:param name="startweekday"/>
		<xsl:param name="startday"/>
		<xsl:param name="daysinmonth"/>
		<tr>
			<xsl:call-template name="weekdayloop">
  				<xsl:with-param name="weekday" select="1"/>
				<xsl:with-param name="startweekday" select="$startweekday"/>
				<xsl:with-param name="day" select="$startday"/>
				<xsl:with-param name="daysinmonth" select="$daysinmonth"/>
 			</xsl:call-template>
		</tr>
	</xsl:template>

	<xsl:template name="weekdayloop">
		<xsl:param name="weekday"/>
		<xsl:param name="startweekday"/>
		<xsl:param name="day"/>
		<xsl:param name="daysinmonth"/>
		<td width="14%" valign="top">
 			<xsl:if test="$weekday >= $startweekday and $daysinmonth >= $day">
				<xsl:call-template name="createCalendarDay">
					<xsl:with-param name="date" select="$day"/>
				</xsl:call-template>
			</xsl:if>
		</td>
		<xsl:choose>
			<xsl:when test="$weekday != 7 and $weekday >= $startweekday">
				<xsl:call-template name="weekdayloop">
					<xsl:with-param name="weekday" select="$weekday + 1"/>
					<xsl:with-param name="startweekday" select="$startweekday"/>
					<xsl:with-param name="day" select="$day + 1"/>
					<xsl:with-param name="daysinmonth" select="$daysinmonth"/>
				</xsl:call-template>
			</xsl:when>
			<xsl:when test="$weekday != 7">
				<xsl:call-template name="weekdayloop">
					<xsl:with-param name="weekday" select="$weekday + 1"/>
					<xsl:with-param name="startweekday" select="$startweekday"/>
					<xsl:with-param name="day" select="$day"/>
					<xsl:with-param name="daysinmonth" select="$daysinmonth"/>
				</xsl:call-template>
			</xsl:when>
		</xsl:choose>
	</xsl:template>	

	<xsl:template name="createCalendarDay">
		<xsl:param name="date"/><!-- date:format-date(date:date(), 'MM') -->
		<xsl:variable name="month" select="date:monthInYear()+1"/><!-- date:format-date(date:date(), 'yyyy') -->
		<xsl:variable name="year" select="date:year()"/>
		<xsl:variable name="thisdate"><xsl:value-of select="$year"/>-<xsl:value-of select="$month"/>-<xsl:value-of select="$date"/></xsl:variable>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr>
				<td align="right"><xsl:value-of select="$date"/></td>
			</tr>
			<tr>
				<td>
					<xsl:choose>
						<xsl:when test="/events:events/events:event[date:year(@enddate) >= date:year($thisdate) and date:year(@startdate) &lt;= date:year($thisdate) and date:month-in-year(@enddate) >= date:month-in-year($thisdate) and date:month-in-year(@startdate) &lt;= date:month-in-year($thisdate) and date:day-in-month(@enddate) >= date:day-in-month($thisdate) and date:day-in-month(@startdate) &lt;= date:day-in-month($thisdate)]">
							<xsl:for-each select="/events:events/events:event[date:year(@enddate) >= date:year($thisdate) and date:year(@startdate) &lt;= date:year($thisdate) and date:month-in-year(@enddate) >= date:month-in-year($thisdate) and date:month-in-year(@startdate) &lt;= date:month-in-year($thisdate) and date:day-in-month(@enddate) >= date:day-in-month($thisdate) and date:day-in-month(@startdate) &lt;= date:day-in-month($thisdate)]">
								<li><font size="-2"><a href="#{@link}"><xsl:value-of select="@organization"/></a></font></li>
							</xsl:for-each>
						</xsl:when>
						<xsl:otherwise>
							<br/>
							<br/>
							<br/>
						</xsl:otherwise>
					</xsl:choose>
				</td>
			</tr>
		</table>
	</xsl:template>
</xsl:stylesheet>
