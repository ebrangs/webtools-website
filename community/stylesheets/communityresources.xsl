<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    version="1.0"
    xmlns:xalan="http://xml.apache.org/xalan"
    xmlns:books="http://www.eclipse.org/webtools/books"
    xmlns:articles="http://www.eclipse.org/webtools/articles"
    xmlns:tutorials="http://www.eclipse.org/webtools/tutorials"
	xmlns:whitepapers="http://www.eclipse.org/webtools/whitepapers"
	xmlns:bios="http://www.eclipse.org/webtools/biopages"
	xmlns:date="http://exslt.org/dates-and-times"
    exclude-result-prefixes="xalan date books articles tutorials whitepapers bios">

    <xsl:include href="wtp-common.xsl" />
	<xsl:template match="/">
		<xsl:apply-templates mode="communityresourcespage"/>
	</xsl:template>

	<xsl:template match="resources" mode="communityresourcespage">
		<xsl:param name="title" select="@title"/>
		<xsl:param name="subtitle" select="''"/>
		<xsl:param name="root" select="'../..'"/>
  		<xsl:call-template name="page">
    		<xsl:with-param name="element" select="."/>
    		<xsl:with-param name="title" select="$title"/>
    		<xsl:with-param name="subtitle" select="$subtitle"/>
    		<xsl:with-param name="root" select="$root"/>
  		</xsl:call-template>
	</xsl:template>

	<xsl:template name="aboutPage">
		<xsl:param name="resources"/>
		<xsl:call-template name="heading">
			<xsl:with-param name="title" select="'Resources from around the WTP community...'"/>
			<xsl:with-param name="alink" select="'top'"/>
		</xsl:call-template>
		<table cellspacing="5" cellpadding="2" border="0">
			<tr>
				<td with="16">&#160;&#160;&#160;&#160;</td>
				<td>
					<xsl:apply-templates select="overview" mode="body"/>
		<p>
			The page is about finding more information about WTP from around the WTP community. 
        	The community has provided many <xsl:apply-templates select="xalan:nodeset($resources)" mode="body"/> 
			that are dedicated to WTP and can help you to make the most of the WTP platform and tools.
		</p>
		<p>
			WTP event information can be found on the <a href="community.html">community page</a>.
		</p>
		<p>
			WTP presentation material can be found on the <a href="presentations.html">WTP presentations archive</a>.
		</p>
        <p>
			We'd like these pages to be useful to you - so please open a 
			<a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools&amp;component=website">bug report</a> 
			for any corrections, additions, pointers, or comments!
        </p>
		<p>
        	For software licensing, website terms of use, and legal FAQs, please see our 
			<a href="http://eclipse.org/legal/main.html" target="_top">legal stuff</a> page.
		</p>
				</td>
			</tr>
		</table>
	</xsl:template>

	<xsl:template name="endpage">
		<xsl:param name="root"/>
		<p></p>
        <p>Please see our <a href="http://www.eclipse.org/legal/privacy.html">privacy 
        policy</a> and website <a href="http://www.eclipse.org/legal/termsofuse.html">terms 
        of use</a>. For problems with the eclipse.org site, please contact the 
        <a href="mailto:webmaster@eclipse.org">webmaster</a> or read the 
        <a href="{$root}/../webmaster/index.html">webmaster FAQ</a> for answers to common questions!</p>
	</xsl:template>
	
	<xsl:template name="heading">
		<xsl:param name="alink"/>
		<xsl:param name="title"/>
		<xsl:param name="toplink"/>
		<table bgcolor="#0080c0" border="0" cellpadding="2" cellspacing="0" width="100%">
			<tbody>
				<tr>
					<td width="90%">
						<b>
							<font color="#ffffff" face="Arial,Helvetica">
								<a name="{$alink}"><xsl:value-of select="$title"/></a>
							</font>
						</b>
					</td>
					<td width="10%" align="right">
						<xsl:if test="$toplink != ''">
							<a href="{$toplink}">top</a>
						</xsl:if>
					</td>
				</tr>
			</tbody>
		</table>
	</xsl:template>

	<xsl:template match="resources">
		<xsl:param name="root"/>
		<xsl:variable name="resources"><xsl:for-each select="resource"><xsl:sort select="@shortTitle"/><xsl:choose><xsl:when test="position() = last()">and <a href="#{@shortTitle}"><xsl:value-of select="@shortTitle"/></a></xsl:when><xsl:otherwise><a href="#{@shortTitle}"><xsl:value-of select="@shortTitle"/></a>, </xsl:otherwise></xsl:choose></xsl:for-each></xsl:variable>
		<xsl:call-template name="aboutPage">
			<xsl:with-param name="resources" select="$resources"/>
		</xsl:call-template>

		<!-- Create page sections. -->
		<xsl:for-each select="resource">
			<xsl:call-template name="heading">
				<xsl:with-param name="title" select="@title"/>
				<xsl:with-param name="alink" select="@shortTitle"/>
				<xsl:with-param name="toplink" select="'#top'"/>
			</xsl:call-template>
			<xsl:apply-templates select="overview" mode="body"/>
			<xsl:apply-templates select="document(@src)"/>
		</xsl:for-each>
		<xsl:call-template name="endpage">
			<xsl:with-param name="root" select="$root"/>
		</xsl:call-template>
	</xsl:template>

	<xsl:template match="*|@*|text()" mode="communityresourcespage">
	</xsl:template>

	<xsl:template match="*|@*|text()">
	</xsl:template>

	<xsl:template match="books:books" mode="communityresourcespage">
		<table cellspacing="5" cellpadding="2" border="0">
			<tr>
				<td with="16">&#160;&#160;</td>
				<td>
		<p>
		<xsl:for-each select="books:book">
			<table border="0" cellspacing="5" cellpadding="2">
				<tr>
					<td width="25%" valign="top">
						<xsl:if test="@image">
							<xsl:choose>
								<xsl:when test="@link">
									<a href="{@link}" target="_top"><img src="{@image}" border="0"/></a>
								</xsl:when>
								<xsl:otherwise>
									<img src="{@image}"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:if>
					</td>
					<td width="75%" valign="top">
						<p>
							<xsl:choose>
								<xsl:when test="@link">
									<a href="{@link}" target="_top"><xsl:value-of select="@title"/></a>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="@title"/>
								</xsl:otherwise>
							</xsl:choose>
						</p>
						<p>
							by:
							<xsl:for-each select="books:author">
								<xsl:if test="position() = last()">
									and
								</xsl:if>
								<xsl:variable name="authorname" select="@name"/>
								<xsl:choose>
									<!-- 
									<xsl:when test="@bioPage">
										<a href="{@bioPage}"><xsl:value-of select="@name"/></a>
									</xsl:when>
									-->
									<xsl:when test="document('../biopages.xml')/bios:bios/bios:bio/@name = $authorname">
										<a href="{document('../biopages.xml')/bios:bios/bios:bio[@name = $authorname]/@link}"><xsl:value-of select="@name"/></a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@name"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if test="position() != last()">
									,
								</xsl:if>
							</xsl:for-each>
						</p>
						<p>
							Publisher:
							<xsl:for-each select="books:publisher">
								<xsl:choose>
									<xsl:when test="@link">
										<a href="{@link}" target="_top"><xsl:value-of select="@name"/></a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@name"/>
									</xsl:otherwise>
								</xsl:choose>
							</xsl:for-each>
						</p>
					</td>	
				</tr>
			</table>	
		</xsl:for-each>
		</p>
				</td>
			</tr>
		</table>
	</xsl:template>

	<!-- Template for articles. This template will create articles that appear as follows:
		> MONTH YEAR  - ARTICLE NAME (PUBLICATION) by AUTHOR NAME
		where the article name will link to the article, the publication will link to the 
		publication if a link has been provided, and the author name will link to a WTP
		bio page for the author if one as been provided.  
	 -->
	<xsl:template match="articles:articles" mode="communityresourcespage">
		<p>
			<table border="0" cellspacing="5" cellpadding="2">
				<xsl:for-each select="articles:article">
				<xsl:sort order="descending" select="@publicationDate"/>
					<tr>
						<td valign="top" align="right"><img width="16" height="16" border="0" src="../../images/Adarrow.gif"/></td>
						<xsl:variable name="month" select="date:month-name(@publicationDate)"/>
						<xsl:variable name="year" select="date:year(@publicationDate)"/>
						<td><xsl:value-of select="$month"/>&#160;<xsl:value-of select="$year"/>
						</td>
						<td>-</td>
						<td><a href="{@link}" target="_top"><xsl:value-of select="@title"/></a>
							<xsl:choose>
								<xsl:when test="articles:publication/@link">
									(<a href="{articles:publication/@link}" target="_top"><xsl:value-of select="articles:publication/@name"/></a>)
								</xsl:when>
								<xsl:otherwise>
									(<xsl:value-of select="articles:publication/@name"/>)
								</xsl:otherwise>
							</xsl:choose>
							by 
							<xsl:for-each select="articles:author">
								<xsl:variable name="authorname" select="@name"/>
								<xsl:choose>
									<xsl:when test="document('../biopages.xml')/bios:bios/bios:bio/@name = $authorname">
										<a href="{document('../biopages.xml')/bios:bios/bios:bio[@name = $authorname]/@link}"><xsl:value-of select="@name"/></a>
									</xsl:when>
									<xsl:otherwise>
										<xsl:value-of select="@name"/>
									</xsl:otherwise>
								</xsl:choose>
								<xsl:if test="position() != last()">
									,&#160;
								</xsl:if>
							</xsl:for-each>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</p>
	</xsl:template>

	<!-- Template for tutorials. This template will create tutorials that appear as follows:
		CATEGORY
		TUTORIAL TITLE
		ABSTRACT
		where the tutorial title is a link to the tutorial and a 'new' or 'updated' image may
		appear next to the title if added or updated within a two week period.  
	 -->
	<xsl:template match="tutorials:tutorials" mode="communityresourcespage">
		<xsl:variable name="displayChangedDays" select="'30'"/>
		<xsl:variable name="dateToday" select="date:date()"/>
		<p></p>
		<xsl:for-each select="tutorials:category">
			<a href="#{@id}"><xsl:value-of select="@name"/></a>
			<xsl:if test="position() != last()">
				&#160;|&#160;
			</xsl:if>
		</xsl:for-each>
		<xsl:for-each select="tutorials:category">
			<p>
				<a name="{@id}"/>
				<table border="0" cellspacing="5" cellpadding="2">
					<tr>
						<td colspan="2">
							<b><xsl:value-of select="@name"/> Tutorials</b>
						</td>
					</tr>
					<xsl:variable name="categoryId" select="@id"/>
					<xsl:for-each select="/tutorials:tutorials/tutorials:tutorial[contains(@categories,$categoryId)]">
						<xsl:sort select="@title"/>
						<tr>
							<td valign="top" align="right"><img width="16" height="16" border="0" src="../../images/Adarrow.gif"/></td>
							<td>
								<b><a href="{@link}"><xsl:value-of select="@title"/></a></b>
								<!-- Add 'new' or 'updated' images. -->
								<!-- Get days of updated date. -->
								<xsl:variable name="numDaysSincePosted">
									<xsl:call-template name="dateDifference">
										<xsl:with-param name="date1" select="@datePosted"/>
										<xsl:with-param name="date2" select="$dateToday"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:variable name="numDaysSinceUpdated">
									<xsl:call-template name="dateDifference">
										<xsl:with-param name="date1" select="@lastUpdated"/>
										<xsl:with-param name="date2" select="$dateToday"/>
									</xsl:call-template>
								</xsl:variable>
								<xsl:choose>
									<xsl:when test="$numDaysSincePosted &lt; $displayChangedDays">
										<img src="images/new.gif"/>
									</xsl:when>
									<xsl:when test="$numDaysSinceUpdated &lt; $displayChangedDays">
										<img src="images/updated.gif"/>
									</xsl:when>
								</xsl:choose>
							</td>
						</tr>
						<tr>
							<td></td>
							<td>
								<xsl:apply-templates select="tutorials:abstract"/>
							</td>
						</tr>
					</xsl:for-each>
				</table>
			</p>
			<hr/>
		</xsl:for-each>
	</xsl:template>

	<xsl:template match="tutorials:abstract">
		<xsl:apply-templates select="*|@*|text()" mode="body"/>
	</xsl:template>

	<!-- Given two dates, determine the difference between them in days. -->
	<xsl:template name="dateDifference">
		<xsl:param name="date1"/>
		<xsl:param name="date2"/>
		
		<xsl:variable name="numDaysDate1">
			<xsl:call-template name="numDays">
				<xsl:with-param name="date" select="$date1"/>
			</xsl:call-template>
		</xsl:variable>
		<xsl:variable name="numDaysDate2">
			<xsl:call-template name="numDays">
				<xsl:with-param name="date" select="$date2"/>
			</xsl:call-template>
		</xsl:variable>

		<xsl:value-of select="$numDaysDate2 - $numDaysDate1"/>
	</xsl:template>

	<!-- Given a date, determine the number of days it's been since Jan. 1, 2004 -->
	<xsl:template name="numDays">
		<xsl:param name="date"/>
		<xsl:variable name="day" select="date:day-in-month($date)"/>
		<xsl:variable name="month" select="date:month-in-year($date)"/>
		<xsl:variable name="year" select="date:year($date)"/>
		<xsl:variable name="dayAdd" select="$day"/>
		<xsl:variable name="monthAdd">
			<xsl:choose>
				<xsl:when test="$month = 0">0</xsl:when>
				<xsl:when test="$month = 1">31</xsl:when>
				<xsl:when test="$month = 2">59</xsl:when>
				<xsl:when test="$month = 3">90</xsl:when>
				<xsl:when test="$month = 4">120</xsl:when>
				<xsl:when test="$month = 5">151</xsl:when>
				<xsl:when test="$month = 6">181</xsl:when>
				<xsl:when test="$month = 7">212</xsl:when>
				<xsl:when test="$month = 8">243</xsl:when>
				<xsl:when test="$month = 9">273</xsl:when>
				<xsl:when test="$month = 10">304</xsl:when>
				<xsl:when test="$month = 11">334</xsl:when>
			</xsl:choose>
		</xsl:variable>
		<xsl:variable name="yearAdd" select="($year - 2004) * 365"/>
		<xsl:value-of select="$dayAdd + $monthAdd + $yearAdd"/>
	</xsl:template>

	<xsl:template match="whitepapers:whitepapers" mode="communityresourcespage">
		<p>
			<table border="0" cellspacing="5" cellpadding="2">
				<xsl:for-each select="whitepapers:paper">
				<xsl:sort order="descending" select="@publicationDate"/>
					<tr>
						<td valign="top" align="right"><img width="16" height="16" border="0" src="../../images/Adarrow.gif"/></td>
						<xsl:variable name="month" select="date:month-name(@publicationDate)"/>
						<xsl:variable name="year" select="date:year(@publicationDate)"/>
						<td><xsl:value-of select="$month"/>&#160;<xsl:value-of select="$year"/>
						</td>
						<td>-</td>
						<td><a href="{@link}" target="_top"><xsl:value-of select="@title"/></a>
						</td>
					</tr>
				</xsl:for-each>
			</table>
		</p>
	</xsl:template>
</xsl:stylesheet>
