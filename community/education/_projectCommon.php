<?php

	# Set the theme for your project's web pages.
	# See the Committer Tools "How Do I" for list of themes
	# https://dev.eclipse.org/committers/
	# Optional: defaults to system theme 
	# miasma
	$theme = "Nova";
	header("Content-type: text/html; charset=utf-8");
	$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/wtpphoenix.css\" />\n");
	
	# Define your project-wide Nav bars here.
	# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
	# these are optional
	$Nav->setLinkList(null);
	$Nav->addCustomNav("WTP Home ...", 	"/webtools/", "_self", 1);
	$Nav->addCustomNav("Documentation ...", 		"/webtools/documentation/", 	"_self", 1);
	$Nav->addCustomNav("Downloads", 		"http://download.eclipse.org/webtools/downloads/", 	"_self", 1);
	$Nav->addNavSeparator("Community", 	"/webtools/community/");
	$Nav->addCustomNav("Resources", 	"/webtools/community/resources/", 		"_self", 2);
	$Nav->addCustomNav("Education", 	"/webtools/community/education/", 		"_self", 2);
	$Nav->addCustomNav("Adopters", 		"/webtools/adopters/", 		"_self", 1);
	$Nav->addCustomNav("Project Wiki", 		"https://wiki.eclipse.org/Category:Eclipse_Web_Tools_Platform_Project", 		"_self", 1);
	$Nav->addCustomNav("Development ...", 		"/webtools/development/", 		"_self", 1);
	$Nav->addCustomNav("FAQ", 				"/webtools/faq/", 			"_self", 1);
	$Nav->addCustomNav("Newsgroup", 	"/newsgroups/main.html", 		"_self", 1);

?>
