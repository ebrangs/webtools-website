<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");
$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();
include($App->getProjectCommon());

$pageKeywords	= "";
$pageAuthor		= "Naci Dai";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

$html = <<<EOHTML

<link href="/webtools/community/education/style.css" rel="stylesheet" type="text/css">
$wtpTopButtons
<div id="edu_maincontent">
	<div class="edu_banner"><img src="/webtools/community/education/education_img/education.png" /></div>
	<div class="edu_desc">
		<p>The goal of WTP Education is to enable and educate WTP end-users by providing them quality resources for self-learning, increase WTP awareness and usage by obtaining, creating, and coordinating, materials for tutorials, articles, webinars, and other digital mediums.</p>
		<p>In these pages you will find a set of courses designed to teach anything from XML, HTML and standards based Web development to advanced topics such as Java Server Faces and Java Persistence.</p>
		<p>You can contribute to WTP Education in many ways. Please provide your feedback, comments, corrections using <a href="https://bugs.eclipse.org/bugs/enter_bug.cgi?product=Web%20Tools" target="_blank">Bugzilla</a> and select Education as the component, they are important to us. Finally, if you would like to add your own courses, please contact the <a href="https://wiki.eclipse.org/Category:WTP_Roles" target="_blank">WTP Education Lead</a>.</p>
	</div>

	<div class="edu_menu">
		<div class="menu_left"><img src="/webtools/community/education/education_img/menu_bg-left.png"/></div>
		<div class="menu_center"><p class="edu_menulink">
			<a href="/webtools/community/education/web/">Web</a>
			<span class="edu_span">|</span>
			<a href="/webtools/community/education/jee/">Java EE</a>
			<span class="edu_span">|</span>
			<a href="/webtools/community/education/jsf/">JSF</a>
			<span class="edu_span">|</span>
			<a href="/webtools/community/education/jpa/">JPA</a>
		</p></div>
		<div class="menu_right"><img src="/webtools/community/education/education_img/menu_bg-right.png"/></div>
	</div>

	<div class="edu_main">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" class="edu_table1">
			<tr class="td_bg-top">
				<td width="75" class="edu_table1td">Name</td> <td width="10">:</td>
				<td><b>WTP-101 Developing Web Applications with Standards</b></td>
			</tr>
			<tr width="75" class="td_bg-bottom" valign="top">
				<td class="edu_table1td">Description</td> <td width="10">:</td>
				<td class="edu_table2td">Developing Web Applications with Standards using W3C org standard technologies such as, HTML, CSS, XML, XSD and XSL.</td>
			</tr>
			<tr width="75" valign="top">
				<td class="edu_table1td">Comments</td> <td width="10">:</td>
				<td><a href="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=WTP-101&product=Web+Tools&component=Education&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&status_whiteboard_type=allwordssubstr&status_whiteboard=&keywords_type=allwords&keywords=&emailtype1=substring&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Importance&field0-0-0=noop&type0-0-0=noop&value0-0-0=" target="_blank">Search</a></td>
			</tr>
		</table>
		<br/>
		<table border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td><img src="/webtools/community/education/education_img/spacer.png" width="20" height="30"/></td>
				<td><a href="/webtools/community/education/web/wtp101/WTP-101-01-WebDevelopment-slides.pdf"><img src="/webtools/community/education/education_img/presentation.png" width="120" height="30" /></a></td>
				<td><img src="/webtools/community/education/education_img/spacer.png" width="20" height="30"/></td>
				<td><a href="/webtools/community/education/web/wtp101/WTP-101-01-WebDevelopment-handouts.pdf"><img src="/webtools/community/education/education_img/book.png" width="80" height="30"/></a></td>
				<td><img src="/webtools/community/education/education_img/spacer.png" width="15" height="30"/></td>
				<td><a href="/webtools/community/education/web/wtp101/WTP-101-01-WebDevelopment-Handson.pdf"><img src="/webtools/community/education/education_img/lab.png" width="63" height="30"/></a></td>
				<td><img src="/webtools/community/education/education_img/spacer.png" width="20" height="30"/></td>
				<td><a href="/webtools/community/education/web/wtp101/handson.zip"><img src="/webtools/community/education/education_img/solution.png" width="91" height="30"/></a></td>
				<td><img src="/webtools/community/education/education_img/spacer.png" width="20" height="30"/></td>
				<td><a href="#"><img src="/webtools/community/education/education_img/live.png" width="57" height="30"/></a></td>
				<td><img src="/webtools/community/education/education_img/spacer.png" width="20" height="30"/></td>
				<td><a href="/webtools/community/education/web/wtp101/WTP-101-01-WebDevelopment.zip"><img src="/webtools/community/education/education_img/source.png" width="104" height="30"/></a></td>
			</tr>
		</table>
		<br/>
	</div>
	<div class="edu_main">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" class="edu_table1">
			<tr class="td_bg-top">
				<td width="75" class="edu_table1td">Name</td> <td width="10">:</td>
				<td><b>T320 Ebusiness Technologies: Foundations and Practice</b></td>
			</tr>
			<tr width="75" class="td_bg-bottom" valign="top">
				<td class="edu_table1td">Description</td> <td width="10">:</td>
				<td class="edu_table2td">This series of tutorials are adapted from those created as a part of a third level (year) course at the Open University. The course, "E-business Technologies: Foundations and Practice", encompasses a wide range of E-business aspects and related technology.</td>
			</tr>
			<tr width="75" valign="top">
				<td class="edu_table1td">Comments</td> <td width="10">:</td>
				<td><a href="t320.php" target="_self">T320 Ebusiness Technologies: Foundations and Practice</a></td>
			</tr>
		</table>
		<br/>
	</div>

</div>

EOHTML;
		$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>