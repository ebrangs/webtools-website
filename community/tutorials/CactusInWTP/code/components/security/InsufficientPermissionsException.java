package components.security;

public class InsufficientPermissionsException extends Exception {

	private static final long serialVersionUID = 1L;

	public InsufficientPermissionsException(String message)
	{
		super(message);
	}
	
}
