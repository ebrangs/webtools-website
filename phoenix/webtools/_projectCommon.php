<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "Phoenix";
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/wtpphoenix.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
#
# We have folding menus.  Second level (anything with a ... below) has a _projectCommon.php that overrides this one.
# It is up to you to keep all these files in sync.
$Nav->addNavSeparator("WTP Home", 	"/webtools/phoenix/webtools/");
$Nav->addCustomNav("Subprojects", "", "_self", 1);
$Nav->addCustomNav("ATF", 	"/webtools/atf/main.php", 		"_self", 2);
$Nav->addCustomNav("Common", 	"/webtools/phoenix/webtools/common/", 		"_self", 2);
$Nav->addCustomNav("Dali", 	"/webtools/dali/main.php", 		"_self", 2);
$Nav->addCustomNav("EJB", 	"/webtools/phoenix/webtools/ejb/", 		"_self", 2);
$Nav->addCustomNav("Java EE", 	"/webtools/phoenix/webtools/jee/", 		"_self", 2);
$Nav->addCustomNav("JSF", 	"/webtools/jsf/main.php", 		"_self", 2);
$Nav->addCustomNav("Server", 	"/webtools/phoenix/webtools/server/", 		"_self", 2);
$Nav->addCustomNav("Source Editors", 	"/webtools/phoenix/webtools/sse/", 		"_self", 2);
$Nav->addCustomNav("Web Services", 	"/webtools/phoenix/webtools/ws/", 		"_self", 2);
$Nav->addCustomNav("WTP Incubator", "/webtools/phoenix/webtools/incubator/", 		"_self", 2);
$Nav->addCustomNav("Documentation ...", 		"/webtools/documentation/", 	"_self", 1);
$Nav->addCustomNav("Downloads", 		"http://download.eclipse.org/webtools/downloads/", 	"_self", 1);
$Nav->addCustomNav("Community ...", 	"/webtools/community/communityresources.php", 		"_self", 1);
#$Nav->addCustomNav("Installation", 		"install.php", 		"_self", 1);
$Nav->addCustomNav("Adopters", 		"/webtools/adopters/", 		"_self", 1);
$Nav->addCustomNav("Project Wiki", 		"https://wiki.eclipse.org/Category:Eclipse_Web_Tools_Platform_Project", 		"_self", 1);
$Nav->addCustomNav("Development ...", 		"/webtools/development/", 		"_self", 1);
$Nav->addCustomNav("FAQ", 				"/webtools/faq/", 			"_self", 1);
$Nav->addCustomNav("Newsgroup", 	"/newsgroups/main.html", 		"_self", 1);

?>
