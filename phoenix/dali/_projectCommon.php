<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "Phoenix";
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/wtpphoenix.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
#
# We have folding menus.  Second level (anything with a ... below) has a _projectCommon.php that overrides this one.
# It is up to you to keep all these files in sync.
$Nav->addNavSeparator("WTP Home", 	"/webtools/main.php");
$Nav->addCustomNav("Dali",          "/webtools/phoenix/dali/main.php", "_self", 1);
$Nav->addCustomNav("Downloads",     "/webtools/phoenix/dali/downloads.php", "_self", 2);
$Nav->addCustomNav("Getting Started (0.5)", "/webtools/phoenix/dali/gettingstarted-0.5.php", 		"_self", 2);

?>
