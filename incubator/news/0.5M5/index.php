<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageKeywords	= "";
$pageAuthor		= "Nitin Dahyabhai";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

# Generate the web page
// Load the XML source
$xml = DOMDocument::load('index.xml');

// Create and configure the transformer
$proc = new XSLTProcessor;
$xsl = DOMDocument::load($root . '/webtools/development/news/new_and_noteworthy.xsl');
$proc->importStyleSheet($xsl); // attach the N&N xsl rules
$maincontent = $proc->transformToXML($xml);

//Set the page title
$xpath = new DOMXPath($xml);
$titleNode = $xpath->query("/release/name");
$pageTitle = ($titleNode != null) ? 'Web Tools Platform ' . $titleNode->nodeValue . ' News' : "eclipse.org webtools page";


$html = <<<EOHTML
<div id="maincontent">
	<div id="midcolumn">
		<p>
		$maincontent
		</p>
	</div>
</div>



EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
