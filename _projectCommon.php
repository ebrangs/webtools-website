<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "solstice";
header("Content-type: text/html; charset=utf-8");
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/nova/wtpnova.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
/*
$Nav->setLinkList(null);
$Nav->addCustomNav( "About This Project", "https://eclipse.org/projects/project_summary.php?projectid=webtools", "_self", 1  );
$Nav->addNavSeparator("WTP", "/webtools/");
$Nav->addCustomNav("Common", "/webtools/common/", "_self", 2);
$Nav->addCustomNav("Dali", "/webtools/dali/main.php", "_self", 2);
$Nav->addCustomNav("EJB Tools", "/webtools/ejb/", "_self", 2);
$Nav->addCustomNav("Java EE Tools", 	"/webtools/jee/", 		"_self", 2);
$Nav->addCustomNav("JSF", 	"/webtools/jsf/", 		"_self", 2);
$Nav->addCustomNav("Releng", 	"/webtools/releng/", 		"_self", 2);
$Nav->addCustomNav("Server Tools", 	"/webtools/server/", 		"_self", 2);
$Nav->addCustomNav("Source Editing", 	"/webtools/sse/", 		"_self", 2);
$Nav->addCustomNav("Web Services", 	"/webtools/ws/", 		"_self", 2);
$Nav->addCustomNav("WTP Incubator", "/webtools/incubator/", 		"_self", 2);
$Nav->addNavSeparator("Downloads", 		"http://download.eclipse.org/webtools/downloads/");
$Nav->addNavSeparator("Documentation", 		"/webtools/documentation/");
$Nav->addCustomNav("Help Docs", "/webtools/documentation/", "_self", 2);
$Nav->addCustomNav("FAQ", 				"https://wiki.eclipse.org/WTP_FAQ", 			"_self", 1);
$Nav->addCustomNav("New and Noteworthy", "/webtools/development/news/", "_self", 1);
$Nav->addCustomNav("Plan", "http://www.eclipse.org/projects/project-plan.php?projectid=webtools", "_self", 2);
$Nav->addNavSeparator("Community", 	"/webtools/community/");
$Nav->addCustomNav("Wiki", 		"https://wiki.eclipse.org/Category:Eclipse_Web_Tools_Platform_Project", 		"_self", 2);
$Nav->addCustomNav("Newsgroup", "http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools", "_self", 2);
$Nav->addCustomNav("Mailing List", 	"http://dev.eclipse.org/mhonarc/lists/wtp-dev/", "_self", 2);
$Nav->addCustomNav("Resources", "/webtools/community/resources/", "_self", 2);
$Nav->addCustomNav("Education", "/webtools/community/education/", "_self", 2);
$Nav->addCustomNav("Contributors", "/webtools/people/contributors.php", "_self", 2);
$Nav->addCustomNav("Source", "http://git.eclipse.org/c/?q=webtools", "_self", 2);
$Nav->addNavSeparator("Development", "/webtools/development/");*/
?>
