<?php
 function &read_file($file_name) {
	$fp = fopen($file_name, "r");
	$file_contents = fread($fp, filesize($file_name));
	fclose($fp);
	return $file_contents;
}

# * * * * *   $wtpTopButtons   * * * * * #
$wtpTopButtons = <<<EOHTML
<link rel="stylesheet" type="text/css" href="/webtools/customMain.css" />
<link rel="shortcut icon" href="/webtools/images/favicon.ico">

<div class="logoBanner"><a href="/webtools/"><img src="/webtools/images/wtp_logo_2010.png" alt="WTP Logo Banner"/></a></div>
<div id="bigbuttons">
	<table border="0" cellspacing="0" cellpadding="0">
		<tr>
			<td>
				<div class="bigbuttonsAbout">
					<a href="/projects/project_summary.php?projectid=webtools"><div class="bigbuttonsTitleLink"></div></a>
					<span><a href="https://git.eclipse.org/c/?q=webtools">Source Repositories</a>, PMC,<br/>Committers and more...</span>
				</div>
			</td>
			<td>
				<div class="bigbuttonsDocumentation">
					<a href="/webtools/documentation/"><div class="bigbuttonsTitleLink"></div></a>
					<span>
						<a href="/webtools/documentation/">Help Docs</a>,
						<a href="https://wiki.eclipse.org/WTP_FAQ"> FAQ</a>,
						<a href="https://eclipse.org/projects/project-plan.php?projectid=webtools"> Plans,</a><br/>
						<a href="/webtools/development/news/main.php">New and Noteworthy</a>
					</span>
				</div>
			</td>
		</tr>
		<tr>
			<td>
				<div class="bigbuttonsCommunity">
					<a href="/webtools/community/"><div class="bigbuttonsTitleLink"></div></a>
					<span>
						<a href="https://wiki.eclipse.org/Category:Eclipse_Web_Tools_Platform_Project">Wiki</a>,
						<a href="https://eclipse.org/forums/eclipse.webtools">Forum</a>,<br/>
						<a href="https://dev.eclipse.org/mailman/listinfo/wtp-dev">Mailing List</a>,
						<a href="/webtools/community/resources/">Resources</a>,<br/>
						<a href="/webtools/community/education/">Education</a>
					</span>
				</div>
			</td>
			<td>
				<div class="bigbuttonsDownloads">
					<a href="https://www.eclipse.org/downloads/packages/release/2024-03/r/eclipse-ide-enterprise-java-and-web-developers"><div class="bigbuttonsTitleLink"></div></a>
					<span>
						<a href="https://www.eclipse.org/downloads/packages/release/2024-03/r/eclipse-ide-enterprise-java-and-web-developers">Enterprise Java IDE</a> package,
						<a href="https://download.eclipse.org/webtools/downloads/">WTP Builds</a>,
						<a href="https://download.eclipse.org/releases/latest/">Latest Update Site</a>,
						<a href="https://marketplace.eclipse.org/content/eclipse-java-ee-developer-tools-0">Install from Marketplace</a>
					</span>
				</div>
			</td>
		</tr>
	</table>
</div>
EOHTML;
# * * * * *   $wtpTopButtons END   * * * * * #
$aboutLinks = <<<EOHTML
<style>#leftcol{display:block !important;}#midcolumn{width:725px !important;}</style>
EOHTML;
?>
