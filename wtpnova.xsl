<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output omit-xml-declaration="yes" />
    <xsl:template match="html">
        <div id="midcolumn">
            <xsl:apply-templates select="body/h1" mode="banner" />
            <xsl:apply-templates select="body/*" />
        </div>
    </xsl:template>
    <xsl:template match="h1" mode="banner">
        <div class="wtpbanner">
            <table>
                <tr>
                    <td width="100%">
                        <h1>
                            <xsl:value-of select="." />
                        </h1>
                        <div class="wtpsubtitle">
                            <xsl:value-of select="/html/head/title" />
                        </div>
                    </td>
                    <td valign="top">
                      	<a href="javascript:history.back()">Back</a>
                    </td>
                    <!--<td>
                      <img src="/webtools/images/wtp_logo_2010.png" alt="WTP Logo" align="right" height="174"
                            width="175" usemap="#wtp_logo_2009" />
                     <map name="wtp_logo_2009">
                            <area shape="circle" 
                                   href="/webtools/" alt="WTP Logo"
                                   coords="85,85,85,85" />
                     </map>
                    </td>-->
                </tr>
            </table>
        </div>
    </xsl:template>
    <xsl:template match="h1" />
    <xsl:template match="*|@*|text()">
        <xsl:copy>
            <xsl:apply-templates select="*|@*|text()" />
        </xsl:copy>
    </xsl:template>
</xsl:stylesheet>
