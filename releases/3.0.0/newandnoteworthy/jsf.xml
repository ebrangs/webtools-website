<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../development/news/new_and_noteworthy.xsl"?>
<release name="3.0" root="../../..">
	<!--
		<component name="component name, such as JavaServer Faces Tools">
		<item title="item title can go here">
			<title>item title can also go here</title>
		<description>description of this new feature, preferably with cropped .pngs (800px wide or less) of the UI</description>
		</item>
		</component>
	-->
	<component name="JavaServer Faces Tools">
		<item
			title="Completed enhancements to the Tag Registry and Component Tree Views">
			<description>
            <a href="http://wiki.eclipse.org/Design_Time_View_Handlers">See here for information on Alternative View Handler support</a>
				<div>
					<h4>Tag Registry View</h4>
					<div>
						<p>The Tag Registry View allows a JSF user to browse the JSF
							tags declared in each project including information about what
							components, converters and validators they create at runtime.
						</p>
					</div>
					<div
						style="padding-top: 2em;padding-bottom: 2em;border-width: 0px; border-style: inset;margin: 1em;">
						<img width="766" height="410" src="jsf/TagRegistry.png"
							alt="Picture of resolver extension in plugin.xml editor" />
					</div>
				</div>
				<div>
					<h4>Component Tree View</h4>
					<p>The Component Tree View allows a user to browse a design-time
						approximation of what a view (i.e. a JSP page or Facelet XHTML)
						will look like at runtime. This component tree will also be used
						by an increasing number of component-based features in the future.
					</p>
					<div
						style="padding-top: 2em;padding-bottom: 2em;border-width: 0px; border-style: inset;margin: 1em;">
						<img width="823" height="410" src="jsf/ComponentTree.png"
							alt="Picture of resolver extension in plugin.xml editor" />
					</div>
				</div>
			</description>
		</item>
		<item title="Hyperlink support in WPE's Source tab">
			<description>
				The Source tab of the Web Page Editor (WPE) supports Hyperlink on a
				Managed Bean variable, Managed Bean property and Managed Bean method
				referenced in the Expression Language(EL) of a tag-attribute. Users
				can
				<b>(Ctrl+click)</b>
				on the hyperlink to navigate to the source of the Managed Bean.
				<br />
				<br />
				<img src="jsf/wpe-hyperlink.png" alt="Hyperlink" />
				<br />
				<br />
			</description>
		</item>
		<item title="Hover Help support in WPE's Source tab">
			<description>
				The Source tab of the Web Page Editor (WPE) provides Hover Help on
				all symbols defined in the Expression Language(EL) of a
				tag-attribute including references to Managed Beans and Resource
				Bundles.
				<br />
				<br />
				<img src="jsf/wpe-hover-help.png" alt="Hover Help" />
				<br />
				<br />
			</description>
		</item>
		<item title="Enhancements to Web Page Editor (WPE) Property Pages">
			<description>
				The tabbed property pages in the Web Page Editor (WPE) have been
				enhanced. This change is important for adopters of the project. See
				<a href="http://wiki.eclipse.org/Enhancements_to_WPE_Property_Pages">Enhancements to WPE Property Pages</a>
				for details.
			</description>
		</item>

		<item title="Support for Apache MyFaces Trinidad">
			<description>
				Apache MyFaces Trinidad tags are now supported in the Web Page Editor, in source editors, and in the Properties view.
				<br/>
				<br/>
				<img src="jsf/trinidad_in_wpe.png" alt="Trinidad tags in the Web Page Editor"/>
				<br/>
				<br/>
				<img src="jsf/trinidad_codeassist.png" alt="Code-assist for Trinidad tags in source editors"/>
				<br/>
				<br/>
				<img src="jsf/trinidad_properties.png" alt="Advanced property sections for Trinidad tags"/>
			</description>
		</item>
        <item title="Improvements to EL content assist and hover help">
            <description>
                <div>Thanks to <b>Matthias Fuessel</b> for providing improvements to our EL support:</div>
                
                <div style="margin: 1em;">
                    <img width="752" height="203" src="jsf/ImprovedContentAssist.png" alt="Improved icons and additional info in content assist"/>
                </div>
                <div style="padding-top: 1em;padding-bottom: 1em;border-width: 0px; border-style: inset;margin: 1em;">
                    <img width="741" height="194" src="jsf/ImprovedHoverHelp.png" alt="Improved info in content assist"/>
                </div>
            </description>
        </item>
        <item title="New EL framework enhancements">
	        <description>
	           <div><p>Thanks to <b>Xiaonan Jiang</b> at IBM for driving new framework requirements and providing prototypes.</p>
               <h4>Decorative Variable and Property Resolvers</h4>
               <p>To better support the decorative nature of JSF variable and property resolvers, we have added the ability
                    to register decorative design time resolvers to the default application manager.  Decorative resolvers
                    are called only if the runtime resolver that they are associated with is found in the configuration
                    of the current web project.  For more information see: <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=206514#c22">here</a></p></div>
               <div style="padding-top: 2em;padding-bottom: 2em;border-width: 0px; border-style: inset;margin: 1em;">
                    <img width="407" height="194" src="jsf/DecorativeResolver.png" alt="Picture of resolver extension in plugin.xml editor"/>
                </div>
                <div>
                    <h4>Enhancements to symbol creation API</h4>
                    <p>We have refactored several internal symbol creation methods and combined them with contributions
                    by <b>Matthias Fuessel</b> and <b>Xiaonan Jiang</b> (IBM), to create a new set of extension points
                    to simplify creation of common JSF symbols.  See <a href="https://bugs.eclipse.org/bugs/show_bug.cgi?id=211321#c11">here</a>
                    for more details.</p>
                    <p>
                        We have also made the symbol creation methods available on a new factory called <i>JSFSymbolFactory</i>.
                    </p>
                </div>
	        </description>
        </item>
                <item title="New EL validation preferences">
            <description>
               <div>
               <p>New validation preferences for EL validation problems allow you
               to change the severity of type assignment problems between Error, Warning and Ignore.</p></div>
               <div style="padding-top: 2em;padding-bottom: 2em;margin: 1em;">
                    <img width="666" height="585" src="jsf/NewELValidationPreferences.png" alt="Shows new preference options in the JSF->Validation properties page"/>
                </div>
            </description>
        </item>
	</component>
</release>