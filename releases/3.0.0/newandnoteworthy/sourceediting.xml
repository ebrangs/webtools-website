<?xml version="1.0" encoding="utf-8"?>
<?xml-stylesheet type="text/xsl" href="../../../development/news/new_and_noteworthy.xsl"?>
<release
	name="3.0"
	root="../../.."
>
	<component
		name="General"
	>
		<item
			title="Filterable Task marker types"
		>
			<description>
				Many of WTP's supported languages now offer their own Task
				Types, allowing them to be better filtered within the Tasks
				view.
				<br />
				<br />
				<img
					src="sourceediting/task-types.png"
					alt="A portion of the Tasks view's Configure Contents dialog" />
			</description>
		</item>
		<item
			title="Go to Matching Bracket"
		>
			<description>
			The source editors now support jumping directly to the highlighted matching bracket from the <b>Navigate > Go To</b> menu.
			<br/>
			<br/>
			<img src="sourceediting/gotomatchingbracket.png" alt="Go To Matching Bracket in menu"/>
			</description>
		</item>
	</component>
	<component name="XML">
		<item title="Import/export xml catalog using Import/Export wizard">
			<description>
				Users are now able to use the Import/Export wizard to import/export xml catalog entries.
				Previously, users had to navigate to the XML Catalog preference page and press the Advanced..
				button to import/export.
				<br />
				<br /> 
				<img src="sourceediting/xmlcatalogimport.png" alt="import xml catalog"/>
				<br />
				<br /> 
				<img src="sourceediting/xmlcatalogexport.png" alt="export xml catalog"/>
			</description>
		</item>
		<item title="Adding filters for XML validation">
			<description>
				You can add filters for XML validation to customize which files the
				XML validator should validate.
				<br />
				<br />
				<img src="sourceediting/validationprefs.png" alt="Validation preferences" />
				<br />
				<br />
				In the Validation preferences, click on the Settings button.
				<br />
				<br />
				<img src="sourceediting/xmlvalidationprefs.png" alt="XML validation filter preferences" />
				<br />
				<br />
				You are presented with a dialog where you can specify which files
				and folders you want to validate and which files and folders you do
				not want to validate.
			</description>
		</item>
		<item title="Sibling and Matching Tag navigation">
			<description>
			The XML, HTML, and JSP editors now allow for jumping back and forth between matching start and end tag pairs, as well as quickly moving between sibling DOM Nodes when editing source.  These actions are available from the <b>Navigate > Go To</b> menu.
			<br/>
			<br/>
			<img src="sourceediting/navigategotosubmenu.png" alt="Navigate...Go To sub-menu"/>
			<br/>
			<br/>
			<img src="sourceediting/siblingnavigation.png" alt="Sibling navigation example"/>
			<br/>
			<br/>
			<img src="sourceediting/matchingtagnavigation.png" alt="Go To Matching Tag example"/>
			</description>
		</item>
		<item title="Validation preferences">
			<description>
			The XML editor now supports validation of documents with XInclude
			directives. In addition, any changes to an XML's grammar (in its DTD or
			XML Schema files) will trigger a revalidation on the XML file. The
			severity of the reported problem for a lack of grammar constraints is
			now also controllable. Both of these features are controllable from the
			<b>XML Files</b>
			preference page.
			<br/>
			<br/>
			<img src="sourceediting/xmlvalidation.png" alt="XML Validation preference"/>
			</description>
		</item>
		<item title="Smart Insert preferences">
			<description>
			The XML editor's Smart Insert features are now controllable from the XML Editor's <b>Typing</b> preference page.
			<br/>
			<br/>
			<img src="sourceediting/xmltyping.png" alt="XML Typing preference page"/>
			</description>
		</item>
	</component>
	<component name="JSP">
		<item title="Validation severity preferences">
		<description>
			The severity of problems reported by JSP Validation is now controllable from the JSP <b>Validation</b> preference page.
			<br/>
			<br/>
			<img src="sourceediting/jspvalidation3.png" alt="JSP Validation preference page"/>
		</description>
		</item>
		<item title="Custom tag validation">
		<description>
			The JSP Syntax Validator will now report validation messages generated
			by a custom tag's TagExtraInfo class, as well as problems 
			found using that tag's classes.
			<br/>
			<br/>
			<img src="sourceediting/jspvalidation.png" alt="A validation message from the TEI class indicating an invalid attribute value"/>
			<br/>
			<br/>
			<img src="sourceediting/jspvalidation2.png" alt="Notifications that the tag handler and TEI classes can not be found"/>
		</description>
		</item>
		<item title="Smart Insert preferences">
			<description>
			The JSP editor's Smart Insert features are now controllable.
			<br/>
			<br/>
			<img src="sourceediting/jsptyping.png" alt="JSP Typing preference page"/>
			</description>
		</item>
		<item title="New Tag Wizard">
			<description>
				A wizard for creating JSP 2.0 Tag files has been added, as has a new
				JSP Template context for use in the wizard, or when editing empty .tag
				files.
				<br/>
				<br/>
				<!-- To be retaken when we get finalized icons -->
				<img src="sourceediting/newtag1.png" alt="The new Tag wizard in the wizard dialog"/>
				<br/>
				<br/>
				<img src="sourceediting/newtag2.png" alt="The new Tag wizard showing New Tag templates"/>
			</description>
		</item>
	</component>
	<component name="XML Schema">
		<item title="Check Full XML Schema Conformance">
			<description>
				The XML Schema Files preference page can now
				control whether XSDs are checked for full
				conformance to the XSD specification. Enabling this
				option will enforce tighter adherence to the XML
				Schema specification at the cost of some validation
				speed.
				<br />
				<br />
				<img src="sourceediting/FullSchemaValidation.png"
					alt="Check Full XML Schema Conformance" />
			</description>
		</item>
		<item title="Able to open XML Schema files not located in current workspace">
			<description>
				Users are now able to use the XML Schema editor to open XSD
				files not located in the current workspace. This includes remote XSD files
				not located in the current file system.
			</description>
		</item>
		<item title="Adding filters for XML Schema validation">
			<description>
				You can add filters for XML Schema validation to customize which files the
				XML Schema validator should validate.
				<br />
				<br />
				<img src="sourceediting/validationprefs.png" alt="Validation preferences" />
				<br />
				<br />
				In the Validation preferences, click on the Settings button.
				<br />
				<br />
				<img src="sourceediting/xsdvalidationprefs.png" alt="XML Schema validation filter preferences" />
				<br />
				<br />
				You are presented with a dialog where you can specify which files
				and folders you want to validate and which files and folders you do
				not want to validate.
			</description>
		</item>
	</component>
	<component name="Javascript">
		<item title="JSDT">
			<description>
 The original JavaScript support and multi-page editor have been replaced by the
 more comprehensive JavaScript Development Tools. It provides
 powerful object-oriented JavaScript features and a greatly improved
 editing experience. Although its integration into WTP isn't fully
 completed in M3, many of its features are already available for use.
				<br />
				<br /><i>
<img src="sourceediting/jsdt-ca.png" alt="Better Content Assist proposals"/><br/>Content Assist proposals are now smarter with the inclusion of objects.
				<br />
				<br />
				<br />
<img src="sourceediting/jsdt-formatting1.png" alt="Formatting with profiles"/><br/>A JavaScript formatter is now provided with the ability to configure multiple profiles.
				<br />
				<br />
				<br />
<img src="sourceediting/jsdt-formatting2.png" alt="Numerous formatting settings"/><br/>The Javascript formatter has a wide array of options to suit almost any taste.
				<br />
				<br />
				<br />
<img src="sourceediting/jsdt-outline1.png" alt="More flexible Outline view"/><br/>The improved Outline view supports displaying object types, filtering of member variables and functions, and much more.
				<br />
				<br />
				<br />
<img src="sourceediting/jsdt-validator.png" alt="Syntax Validation"/><br/>Configurable JavaScript Validation is now provided alongside WTP's existing validators.
				<br />
				<br />
				<br />
<img src="sourceediting/jsdt-quickfix1.png" alt="Quick Fix"/><br/>Many JavaScript validation problems also provide QuickFix opportunities.</i>
				<br />
				<br />
				<br />
				<img
					src="sourceediting/jsdt-paths.png"
					alt="Setting the JavaScript Include Path"
				/><br/>
				To use the new JavaScript editor for web-page
				oriented scripts much like the old editor, select
				Window() in the ECMA 3 Browser Support Library as
				the project's Global SuperType.
				<br />
				<br /> 
 For more information on these and other features, visit
 the
 <a href="http://wiki.eclipse.org/JSDT">JSDT</a> wiki page or see it in action on <a href="http://live.eclipse.org/node/355">Eclipse Live</a>.
			</description>
		</item>
	</component>
	<component name="HTML">
		<item title="Validation severity preferences">
		<description>
			The severity of problems reported by HTML Validation is now controllable from the HTML <b>Validation</b> preference page.
			<br/>
			<br/>
			<img src="sourceediting/htmlvalidation.png" alt="HTML Validation preference page"/>
		</description>
		</item>
		<item title="Smart Insert preferences">
			<description>
			The HTML editor's Smart Insert features are now controllable.
			<br/>
			<br/>
			<img src="sourceediting/htmltyping.png" alt="HTML Typing preference page"/>
			</description>
		</item>
	</component>
</release>