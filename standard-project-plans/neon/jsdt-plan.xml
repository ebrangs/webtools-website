<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
<?xml-stylesheet type="text/xsl" href="http://www.eclipse.org/projects/project-plan.xsl"?>
    <!--
        Format is detailed at
        https://wiki.eclipse.org/Development_Resources/Project_Plan
        
        Use this to test local rendering -->
<!-- <?xml-stylesheet type="text/xsl" href="http://www.eclipse.org/projects/project-plan.xsl"?> -->
<p:plan plan-format="1.0" xmlns:p="http://www.eclipse.org/project/plan"
	xmlns="http://www.w3.org/1999/xhtml" name="JavaScript Development Tools">
	<p:release projectid="webtools.jsdt" version="3.8 (Neon)" />
	<p:introduction>
		<p>The JavaScript Development Tools (JSDT) project is a
			part of the Eclipse WTP top-level Project. It provides a JavaScript
			IDE for Eclipse and JavaScript language support for WTP's Source Editing project's web components.
			This document describes	the features and the API set in the JSDT project
			for the	WTP 3.8 release.</p>
	</p:introduction>
	<p:release_deliverables>
		<p>Source code for deliverables tagged in Git as version tagged
			<em>R3_8_0</em>, viewable in the JSDT
			<a
				href="http://git.eclipse.org/c/jsdt/">Git repositories</a>
			.
		</p>
	</p:release_deliverables>
	<p:target_environments>
		<p>
			JSDT will support the platforms certified by the Eclipse Platform
			project. For a list of platforms supported , see
			<a
				href="http://www.eclipse.org/projects/project-plan.php?projectid=eclipse#target_environments">Eclipse Target Operating Environments</a>.
		</p>
		<p:internationalization>
			<p>
				Internationalization and Localization will be supported.
			</p>
		</p:internationalization>
	</p:target_environments>
	<p:themes_and_priorities>
		<p:preamble>
				<p>Themes and their priorities communicate the main objectives of
					the project and their importance. These will be prioritized based
					on the community feedback. New themes could be synthesized from the
					requirements submitted by the community.
				</p>
				<p>
					The sections to follow defines the plan items in the JSDT project. The plan items are grouped under the respective
					themes where applicable. Each plan item corresponds to a new
					feature, API or some aspects of the project that needs to be
					improved. A plan item has an entry in the Eclipse Bugzilla system
					that has a detailed description of the needed improvement. Not all plan
					items
					represent the same amount of work; some may be quite large,
					others,
					quite small. Although some plan items are for work that is
					more
					pressing than others, the plan items appear in no particular
					order.
					See the corresponding bugzilla items for up-to-date status
					information on ongoing work and planned delivery milestones.</p>
				<p>For Mars, JSDT will integrate tools like bower, nodejs, npm, basing on Stéphane Bégaudeau's 
				    project (https://github.com/sbegaudeau/webtools.jsdt). As result, this tooling would include 
				    a launch configuration for node.js applications, a basic java implementation of bower built on top 
				    of JGit and a Java implementation of node.js semantic versioning engine, actions dedicated to bower, 
				    npm, grunt and gulp, etc. </p>
				<p>Support for ECMA5 Script as well is the first priority task for JSDT.</p> 
				<p>Yet another task for JSDT is a support for injecting parser implementation
				    that should allow a user to use some external parser (like Tern.java, esprima or 
				    any other implementations for example https://github.com/bkiers/ecmascript-parser) 
				    for different tasks like validation, content assist etc.</p>			
				<p>The focus on maintenance issues, fixing bugs, and upgrading dependencies will continue to be the primary task for JSDT as well.</p>
		</p:preamble>
		<p:theme name="Everything">
			<p:description>
				<p>
					The following are plan items including those not yet
					categorized into a theme.
				</p>
			</p:description>
			<!-- keyword "plan" -->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;keywords_type=allwords&amp;short_desc=&amp;product=JSDT&amp;target_milestone=3.8+M1&amp;target_milestone=3.8+M2&amp;target_milestone=3.8+M3&amp;target_milestone=3.8+M4&amp;target_milestone=3.8+M5&amp;target_milestone=3.8+M6&amp;target_milestone=3.8+M7&amp;target_milestone=3.8+RC1&amp;target_milestone=3.8+RC2&amp;target_milestone=3.8+RC3&amp;target_milestone=3.8+RC4&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit" />
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;keywords_type=allwords&amp;short_desc=&amp;product=JSDT&amp;target_milestone=3.8&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:proposed>
			<p:deferred
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;keywords_type=allwords&amp;short_desc=&amp;product=JSDT&amp;target_milestone=Future&amp;target_milestone=---&amp;target_milestone=4.0&amp;keywords=plan&amp;order=target_milestone%2Cpriority%2Cbug_id&amp;cmdtype=doit"></p:deferred>
		</p:theme>
	</p:themes_and_priorities>
	<p:appendix name="References">
		<p>
			The general WTP plan can be found
			<a
				href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools">
				here
			</a>
			.
		</p>
	</p:appendix>
</p:plan>