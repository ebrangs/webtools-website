<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>
	<!--  Use this to test local rendering in firefox -->
	<!--
		<?xml-stylesheet type="text/xsl" href="project-plan-render.xsl"?>
	-->
	<!-- Format is detailed at http://wiki.eclipse.org/Development_Resources/Project_Plan -->
<?xml-stylesheet type="text/xsl" href="http://www.eclipse.org/projects/project-plan.xsl"?>
<p:plan plan-format="1.0" xmlns:p="http://www.eclipse.org/project/plan"
	xmlns="http://www.w3.org/1999/xhtml" name="WTP Source Editing">
	<p:release projectid="webtools.sourceediting" version="3.1" />
	<p:introduction><div>The Source Editing project is a sub-project in the
		Eclipse WTP top level Project. Its main goal is to provide source
		editing support for typical "web" languages and the mixing of them,
		with a focus on making the editors extensible and easily reusable.
		This document describes the features and the API set in the Source
		Editing subproject for the Galileo release.</div></p:introduction>
	<p:release_deliverables>
		<div>
			<ul>
				<li>Source
					code for deliverables tagged in CVS as version tagged &quot;R3_1_0&quot;, viewable in the WebTools_Project
					<a href="http://dev.eclipse.org/viewcvs/">CVS repository</a>.
    			</li>
			</ul>
		</div>
	</p:release_deliverables>
	<p:release_milestones>
	<p:preamble>
		<div>WTP Milestones follow the Eclipse release milestones by an offset of "+2" as set by the <a href="http://wiki.eclipse.org/Galileo#Milestones_and_Release_Candidates">Galileo release schedule</a>.</div>
	</p:preamble>
		<p:milestone date="8/22/2008" milestone="M1"/>
		<p:milestone date="9/26/2008" milestone="M2">
			<div>(lengthened to avoid conflicting with 3.0.2)</div>
		</p:milestone>
		<p:milestone date="11/7/2008" milestone="M3"/>
		<p:milestone date="12/19/2008" milestone="M4"/>
		<p:milestone date="2/6/2009" milestone="M5"/>
		<p:milestone date="3/18/2009" milestone="M6">
			<div>Feature complete, API and UI Freeze.  (EclipseCon is March 23-27)</div>
		</p:milestone>
		<p:milestone date="5/8/2009" milestone="M7">
			<div>Fixes and focused performance tuning</div>
		</p:milestone>
		<p:milestone date="5/22/2009" milestone="RC1">
			<div>PMC approval required, 1 vote required</div>
		</p:milestone>
		<p:milestone date="5/29/2009" milestone="RC2">
			<div>PMC approval required, Normal and higher bugs only.  2 votes required</div>
		</p:milestone>
		<p:milestone date="6/5/2009" milestone="RC3">
			<div>Document Freeze. Major and higher bugs only.  PMC approval required, 3 votes required</div>
		</p:milestone>
		<p:milestone date="6/12/2009" milestone="RC4">
			<div>Only stop-ship defects, PMC approval required, 3 votes required</div>
		</p:milestone>
		<p:milestone date="6/19/2009" milestone="RC5">
			<div>Only <b>very</b> compelling stop-ship defects, PMC approval required, 3 votes required</div>
		</p:milestone>
		<p:milestone date="6/24/2009" milestone="GA"/>
		<p:postamble />
	</p:release_milestones>
	<p:target_environments>
		<div>WTP will support the platforms certified by the Eclipse Platform project. For a list of platforms supported in WTP 3.1, see <a
			href="http://www.eclipse.org/eclipse/development/eclipse_project_plan_3_4.html#TargetOperatingEnvironments%20Eclipse%20Target%20Operating%20Environments">Eclipse Target Operating Environments</a>.</div>
		<p:internationalization><div>Internationalization and Localization will be supported.</div></p:internationalization>
	</p:target_environments>
	<p:compatibility_with_previous_releases />
	<p:themes_and_priorities>
		<p:preamble>
			<div><p>Themes and their priorities communicate the main objectives of
				the project and their importance. The following themes are derived
				from those defined by the Eclipse Requirement council for the
				Eclipse Galileo release and from the WTP 3.0 release themes. These
				will be prioritized based on the community feedback. New themes
				could be synthesized from the requirements submitted by the
				community.
				</p><p>
				The sections to follow defines the plan items in the Source
				Editing project. The plan items are grouped under the respective
				themes where applicable. Each plan item corresponds to a new
				feature, API or some apsects of the project that needs to be
				improved. A plan item has an entry in the Eclipse Bugzilla system
				that has a detailed description of the plan item. Not all plan items
				represent the same amount of work; some may be quite large, others,
				quite small. Although some plan items are for work that is more
				pressing than others, the plan items appear in no particular order.
				See the corresponding bugzilla items for up-to-date status
				information on ongoing work and planned delivery milestones.</p></div>
		</p:preamble>
		<p:theme name="Ease of Use">
			<p:description>
				<div><p>
					Features provided by WTP should be simple to use for users with
					widely-varying backgrounds and skill sets.
				</p>
				<ul>
					<li>WTP User Interface should be consistent and should follow the
						Eclipse User Experience Guidelines. Experienced Eclipse users should
						find few, if any, unpleasant surprises or omissions in the UI.</li>
					<li>Usability and Accessibility reviews should be done for the
						most common task flows. Cheat Sheets should be provided to assist
						users in performing tasks.</li>
					<li>WTP should provide enhanced user documentation, tutorials, white
						papers, demonstrations.</li>
				</ul>
				<p>
					<i>Ease of Use</i> plan items are designated with "EaseOfUse" in the Status Whiteboard and the "plan" keyword.
				</p>
				</div>
			</p:description>
			<!-- keyword "plan" and status white-board includes the string "EaseOfUse" -->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;status_whiteboard_type=substring&amp;status_whiteboard=EaseOfUse&amp;product=WTP+Source+Editing&amp;target_milestone=3.1+M1&amp;target_milestone=3.1+M2&amp;target_milestone=3.1+M3&amp;target_milestone=3.1+M4&amp;target_milestone=3.1+M5&amp;target_milestone=3.1+M6&amp;target_milestone=3.1+M7&amp;target_milestone=3.1+RC1&amp;target_milestone=3.1+RC2&amp;target_milestone=3.1+RC3&amp;target_milestone=3.1+RC4&amp;keywords=plan&amp;cmdtype=doit"></p:committed>
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;status_whiteboard_type=substring&amp;status_whiteboard=EaseOfUse&amp;product=WTP+Source+Editing&amp;target_milestone=---&amp;keywords=plan&amp;cmdtype=doit"></p:proposed>
			<p:deferred
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;status_whiteboard_type=substring&amp;status_whiteboard=EaseOfUse&amp;product=WTP+Source+Editing&amp;target_milestone=Future&amp;target_milestone=3.2&amp;keywords=plan&amp;cmdtype=doit"></p:deferred>
		</p:theme>
		<p:theme name="Scaling Up">
			<p:description>
			<div>
				<p>
					Source Editing must be able to deal with development and deployment on an
					increasingly larger and more complex scale. WTP should spend focused
					effort on performance testing and improvement when dealing with
					extremely large projects and workspaces, particularly where
					extensibility is offered.
				</p>
				<p>
					<i>Scaling Up</i> plan items are designated with the "plan" and "performance" keywords.
				</p>
				</div>
			</p:description>
			<!-- keywords "plan" and "performance" -->
			<p:committed bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;product=WTP+Source+Editing&amp;target_milestone=3.1&amp;target_milestone=3.1+M1&amp;target_milestone=3.1+M2&amp;target_milestone=3.1+M3&amp;target_milestone=3.1+M4&amp;target_milestone=3.1+M5&amp;target_milestone=3.1+M6&amp;target_milestone=3.1+M7&amp;target_milestone=3.1+RC1&amp;target_milestone=3.1+RC2&amp;target_milestone=3.1+RC3&amp;target_milestone=3.1+RC4&amp;keywords_type=allwords&amp;keywords=plan+performance&amp;cmdtype=doit"/>
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;product=WTP+Source+Editing&amp;target_milestone=---&amp;keywords_type=allwords&amp;keywords=plan+performance&amp;cmdtype=doit"></p:proposed>
			<p:deferred
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;product=WTP+Source+Editing&amp;target_milestone=Future&amp;target_milestone=3.2&amp;keywords_type=allwords&amp;keywords=plan+performance&amp;cmdtype=doit"></p:deferred>
		</p:theme>		
		<p:theme name="Design for Extensibility">
			<p:description>
			<div>
			<p>
				The 'P' in WTP stands for Platform, meaning Source Editing can be used by adopters to
				extend its functionality. This theme is about continuing to ensure the
				success of its adopters by promoting new API's and Extension points.
				These should be backed with robust Junit tests and good documentation.
			</p>
			<p>
				<i>Design for Extensibility</i> plan items are designated with the "plan" and "api" keywords.
			</p>
			</div>
			</p:description>
			<!-- keywords "plan" and "api" -->
			<p:committed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;product=WTP+Source+Editing&amp;target_milestone=3.1&amp;target_milestone=3.1+M1&amp;target_milestone=3.1+M2&amp;target_milestone=3.1+M3&amp;target_milestone=3.1+M4&amp;target_milestone=3.1+M5&amp;target_milestone=3.1+M6&amp;target_milestone=3.1+M7&amp;target_milestone=3.1+RC1&amp;target_milestone=3.1+RC2&amp;target_milestone=3.1+RC3&amp;target_milestone=3.1+RC4&amp;keywords_type=allwords&amp;keywords=plan+api&amp;cmdtype=doit"></p:committed>
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;product=WTP+Source+Editing&amp;target_milestone=---&amp;keywords_type=allwords&amp;keywords=plan+api&amp;cmdtype=doit"></p:proposed>
			<p:deferred
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;product=WTP+Source+Editing&amp;target_milestone=Future&amp;target_milestone=3.2&amp;keywords_type=allwords&amp;keywords=plan+api&amp;cmdtype=doit"></p:deferred>
		</p:theme>		
		<p:theme name="Everything">
			<p:description>
				<div>Following are plan items including those not yet categorized into a theme.</div>
			</p:description>
			<!-- keyword "plan" -->
			<p:committed bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=WTP+Source+Editing&amp;target_milestone=3.1&amp;target_milestone=3.1+M1&amp;target_milestone=3.1+M2&amp;target_milestone=3.1+M3&amp;target_milestone=3.1+M4&amp;target_milestone=3.1+M5&amp;target_milestone=3.1+M6&amp;target_milestone=3.1+M7&amp;target_milestone=3.1+RC1&amp;target_milestone=3.1+RC2&amp;target_milestone=3.1+RC3&amp;target_milestone=3.1+RC4&amp;keywords=plan&amp;cmdtype=doit" />
			<p:proposed
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=WTP+Source+Editing&amp;target_milestone=---&amp;keywords=plan&amp;cmdtype=doit"></p:proposed>
			<p:deferred
				bugzilla="https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=WTP+Source+Editing&amp;target_milestone=Future&amp;target_milestone=3.2&amp;keywords=plan&amp;cmdtype=doit"></p:deferred>
		</p:theme>
	</p:themes_and_priorities>
	<p:appendix name="References">
		<ul>
			<li>The general WTP plan can be found <a href="http://www.eclipse.org/projects/project-plan.php?projectid=webtools">here</a>.</li>
		</ul>
	</p:appendix>
</p:plan>