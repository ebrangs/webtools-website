
/*
	ChangeLog:
	
	2005-02-24 Phil Avery, pavery@us.ibm.com
	- initial version
	
*/

	var isIE = document.all?true:false;
	if (!isIE) document.captureEvents(Event.MOUSEMOVE);
	document.onmousemove = getMousePosition;
	
	var mouse_x;
	var mouse_y;
	var popup_height = 200;
	var popup_width = 300;
	
	function showPopup(text) {
		
		var params = 'status=no, menubar=0,menubars=0,personalbar=0,statusbars=0, width='+ popup_width +',height='+ popup_height+',left='+mouse_x+',top='+mouse_y;
		popup = open("","hoverwindow", params);

		popup.document.open();
		
		//popup.document.write('mouse_x is:' + mouse_x + ' mouse_y is:' + mouse_y );
		popup.document.write("<body bgcolor='#eeeeee' style='font-family:arial;font-size:12px;'>"							 
							 + text  
							 + "</body>");	
		popup.document.close();
	}
	
	function hidePopup() {
		popup.close();
	}
	
	function getMousePosition(e) {

	  if (!isIE) {
	    mouse_x = e.pageX + window.screenX + 25;
	    mouse_y = e.pageY + window.screenY - (popup_height/2) - 25;
	  }
	  if (isIE) {
	    mouse_x = event.clientX + document.body.scrollLeft + window.screenLeft + 25;
	    mouse_y = event.clientY + document.body.scrollTop + window.screenTop - popup_height - 50;
	  }
	  return true;
	}
