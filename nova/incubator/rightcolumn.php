<?php
$rightColumn = <<<EOHTML
	<div id="rightcolumn">
		<div class="sideitem">
			<h6>Incubation</h6>
   			<div class="wtpaligncenter">
   				<a href="/projects/what-is-incubation.php">
   					<img src="/images/egg-incubation.png" border="0" alt="Incubation" />
   				</a>
   			</div>
		</div>
		
		<div class="sideitem">
			<h6>Project Links</h6>
			<ul>
				<li><a href="proposals/">Proposals</a></li>
				<li><a href="downloads/">Downloads</a></li>
			</ul>
		</div>
	</div>
EOHTML;
?>