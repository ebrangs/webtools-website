<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle              = "Server Tools";
$pageKeywords   = "Eclipse WTP webtools XML HTML JSP CSS DTD IDE";
$pageAuthor             = "Angel Vera";

$html = <<<EOHTML

<div id="midcolumn">
<table>
    <tr>
        <td width="60%">
        <h1>$pageTitle</h1>
        </td>
        <td><img
            src="/webtools/images/wtplogosmall.jpg"
            alt="WTP Logo"
            align="middle"
            height="129"
            hspace="50"
            width="207"
            usemap="logomap" /> <map
            id="logomap"
            name="logomap">
            <area
                coords="0,0,207,129"
                href="/webtools/"
                alt="WTP Home" />
        </map></td>
    </tr>
</table>
<h2>Project overview</h2>
<p>
	The Server Tools project is a sub-project in the Eclipse
	WTP Top-Level Project. Its main goal is to provide a extensible
	server framework to provide support but not exclusisvely for J2EE,
	JEE, and HTTP Servers. 
</p>
<p>
	This project overview page is continually evolving, so
	please check back regularly for the most up-to-date
	information on the Server Tools project.  In the meantime,
	consult the older <a href="../../webtools/server/index.php">Server overview page</a> 
	page, as well as those our archived out of date information, if it of any use to you
	<a href="../../wst/components/server/">Server archives docs</a>.
</p>
</div>

EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
