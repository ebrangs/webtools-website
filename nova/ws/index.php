<?php  																														require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php"); 	require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php"); 	$App 	= new App();	$Nav	= new Nav();	$Menu 	= new Menu();		include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle              = "Web Services";
$pageKeywords   = "Eclipse WTP Web Services Tools";
$pageAuthor             = "Keith Chong";

$html = <<<EOHTML

<div id="midcolumn">
<table>
    <tr>
        <td width="60%">
        <h1>$pageTitle</h1>
        </td>
        <td><img
            src="/webtools/images/wtplogosmall.jpg"
            alt="WTP Logo"
            align="middle"
            height="129"
            hspace="50"
            width="207"
            usemap="logomap" /> <map
            id="logomap"
            name="logomap">
            <area
                coords="0,0,207,129"
                href="/webtools/"
                alt="WTP Home" />
        </map></td>
    </tr>
</table>
<h2>Project overview</h2>
<p>
	The Web Services project is a sub-project in the Eclipse
	WTP Top-Level Project. There are two components:
	
    <ul>
      <li>
      The JST Web services component contains tools for developing and interacting with Java Web services. It consists of:
      <ul>
        <li>extensible Web services wizards for creating Web service and Web services client wizards for consumming Web service,</li> 
        <li>Web services Ant tasks for creating and consumming Web services,</li> 
        <li>wizard extensions for the Apache Axis v1.4 and Apache Axis2 Web service runtimes.</li>
      </ul>
      </li>
      <li> 
      The WST Web services component contains tools for Web services development which is not Java specific. It consists of:
      <ul>
         <li>Web services preferences pages,</li> 
         <li>Web services frameworks such as the creation framework and finder framework,</li> 
         <li>Web Services Explorer, a Web application that let you discover and publish to UDDI, and invoke a WSDL/WSIL via native XML.</li> 
         <li>WSDL model</li> 
         <li>WSDL Editor </li>
         <li>WSDL and WS-I validator</li>
      </ul> 
      </li>
    </ul>
</p>
<p>
	This project overview page is continually evolving, so
	please check back regularly for the most up-to-date
	information on the Web Services Tools project.  In the meantime,
	consult the older <a href="../../ws/index.php">Web Services overview page</a> 
	page.
</p>
</div>

EOHTML;
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>
