<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author:               Kaloyan Raev
# Date:                 2000-02-13
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle              = "WTP EJB Tools";
$pageKeywords   = "Eclipse WTP EJB Java EE IDE";
$pageAuthor             = "Kaloyan Raev";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links", "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Include this php file to get the right column
# as assigned by the $rightColumn variable in HTML content
#include( 'rightcolumn.php' );


# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="midcolumn">
<table>
    <tr>
        <td width="60%">
        <h1>$pageTitle</h1>
        </td>
        <td><img
            src="/webtools/images/wtplogosmall.jpg"
            alt="WTP Logo"
            align="middle"
            height="129"
            hspace="50"
            width="207"
            usemap="logomap" /> <map
            id="logomap"
            name="logomap">
            <area
                coords="0,0,207,129"
                href="/webtools/"
                alt="WTP Home" />
        </map></td>
    </tr>
</table>
<h1>Project overview</h1>
<p> The EJB Tools Project provides frameworks and tools focused on the
	development of Enterprise JavaBeans artifacts. Currently, the project
	supports EJB 1.1, 2.0, 2.1 and 3.0 specification levels.
</p>
</div>
EOHTML;


        # Generate the web page
        $App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
        ?>

