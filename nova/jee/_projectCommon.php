<?php

# Set the theme for your project's web pages.
# See the Committer Tools "How Do I" for list of themes
# https://dev.eclipse.org/committers/
# Optional: defaults to system theme
# miasma
$theme = "Nova";
header("Content-type: text/html; charset=utf-8");
$App->AddExtraHtmlHeader("<link rel=\"stylesheet\" type=\"text/css\" href=\"/webtools/nova/wtpnova.css\" />\n");

# Define your project-wide Nav bars here.
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# these are optional
$Nav->setLinkList(null);
$Nav->addCustomNav( "About This Project", "/projects/project_summary.php?projectid=webtools.jeetools", "_self", 1  );
$Nav->addNavSeparator("WTP Home", "/webtools/");
$Nav->addNavSeparator("WTP Java EE Tools", "/webtools/jee");
$Nav->addNavSeparator("Downloads", "http://download.eclipse.org/webtools/downloads/");
$Nav->addNavSeparator("Documentation", "/webtools/documentation/");
$Nav->addCustomNav("Project Plan", "http://www.eclipse.org/projects/project-plan.php?projectid=webtools.jeetools", "_self", 2);
$Nav->addCustomNav("JEE Status Meetings", "https://wiki.eclipse.org/JEE_Status_Meetings", "_self", 2);
$Nav->addNavSeparator("Community", 	"/webtools/community/");
$Nav->addCustomNav("Newsgroup", "http://www.eclipse.org/newsportal/thread.php?group=eclipse.webtools", "_self", 2);
$Nav->addCustomNav("Mailing List", "http://dev.eclipse.org/mhonarc/lists/wtp-dev/", "_self", 2);
$Nav->addCustomNav("Open Bugs", "https://bugs.eclipse.org/bugs/buglist.cgi?query_format=advanced&short_desc_type=allwordssubstr&short_desc=&classification=WebTools&product=WTP+Java+EE+Tools&long_desc_type=allwordssubstr&long_desc=&bug_file_loc_type=allwordssubstr&bug_file_loc=&status_whiteboard_type=allwordssubstr&status_whiteboard=&keywords_type=allwords&keywords=&bug_status=UNCONFIRMED&bug_status=NEW&bug_status=ASSIGNED&bug_status=REOPENED&emailtype1=substring&email1=&emailtype2=substring&email2=&bugidtype=include&bug_id=&votes=&chfieldfrom=&chfieldto=Now&chfieldvalue=&cmdtype=doit&order=Reuse+same+sort+as+last+time&known_name=Java+EE+All+Open&query_based_on=Java+EE+All+Open&field0-0-0=noop&type0-0-0=noop&value0-0-0=", "_self", 2);
$Nav->addCustomNav("Submit A Bug", "https://bugs.eclipse.org/bugs/enter_bug.cgi?product=WTP%20Java%20EE%20Tools", "_self", 2);
?>
