<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'

#*****************************************************************************
#
# template.php
#
# Author:               Denis Roy
# Date:                 2005-06-16
#
# Description: Type your page comments here - these are not sent to the browser
#
#
#****************************************************************************

#
# Begin: page-specific settings.  Change these.
$pageTitle              = "Common Tools";
$pageKeywords   = "Eclipse WTP webtools";
$pageAuthor             ="Ugur Yildirim @ Eteration A.S.";

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links",        "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

# End: page-specific settings
#

# Paste your HTML content between the EOHTML markers!
$html = <<<EOHTML

<div id="midcolumn">
	<table><tbody><tr><td width="60%"><h1>Common tools project</h1><div class="wtpsubtitle">Common tools</div></td><td><img src="/webtools/images/wtplogosmall.jpg" alt="WTP Logo" usemap="logomap" align="middle" height="129" hspace="50" width="207"><map id="logomap" name="logomap"><area coords="0,0,207,129" href="/webtools/" alt="WTP Home"></map></td></tr></tbody></table><h2 class="bar">Project overview</h2><p>
			The common component contains common utilities and
			infrastructure which are required by the Web Tools Platform
			project, but which are not specific to web tooling. The most
			notable examples are Facet APIs, Validation, the Snippets View,
			and an Extensible URI Resolver. These components may make
			use of the prerequisites for WTP itself, such as
			EMF/SDO/XSD, GEF, and DTP, but require no WTP plug-ins from
			outside of the Common project.
		</p><h2 class="bar">Components</h2><ul class="indent">
			<li>Facets</li>
			<li>
				<a href="/webtools/wst/components/common/overview/snippets/overview.html">Snippets View</a>
			</li>
			<li>
				<a href="/webtools/wst/components/validation/ValidationOverview.html">Validation</a>
			</li>
			<li>Extensible URI Resolver</li>
		</ul><p>
			This project overview page is still evolving, so
			please check back regularly for the most up-to-date
			information on the Common Tools project.
		</p>
</div>
EOHTML;


# Generate the web page
$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>

