<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../../../../" />
		<title>Top Down Web Service Supporting Flexible Projects</title>
	</head>
	<body>
		<h1>Top Down Web Service Supporting Flexible Projects</h1>
		<p>
			<b>By Kathy Chan</b>
			<br />
			August 11, 2005
			<br />
			<br />
		</p>

		<h2>Introduction</h2>
		<br></br>
		<p>
			This document is one of a series of tutorials to
			demonstrate the use of the Web Services tools in the Web
			Tools Platform Project using the <b>WTP 0.7 drivers</b>.
		</p>
		<p>
			This tutorial shows how to create a simple top-down Web
			service from a WSDL file. The WSDL file in this scenario
			calculates the area of an rectangle.

		</p>
		<br></br>


		<h2>Set Up</h2>
		<br></br>
		<p>
			Before creating the Web service, there are three
			prerequisites:
			<ol>
				<li>
					<a href="../InstallTomcat/InstallTomcat.html">
						Install Apache Tomcat
					</a>
				</li>
				<li>
				Set the <b>J2EE Project Module Preference</b> to
				allow multiple modules per project.  Open 
				<b>Window -&gt; Preferences... -&gt; J2EE Project Modules Preference
				</b> and check the <b>Allow Multiple modules per project</b> option.
				</li>
				<li>Create a Web project</li>
				<ol>
					<li>
						Open
						<b>
							File -&gt; New -&gt; Other... -&gt; Web -&gt; Dynamic Web
							Project
						</b>
						to create a new flexible project and Web module.
					</li>
					<li>Click <b>Next</b>.</li>
					<li>Add a new project by clicking <b>New...</b> beside the project name field.</li>
					<li>
						Enter
						<tt>AreaProject</tt>
						into the
						<b>Name</b>
						field.
					</li>
					
					<li>
						Ensure that the 
						<b>Target server</b>
						is set to 
						<b>Apache Tomcat v5.0</b>
						.  If you have not previously configured
						a server runtime use the <b>New...</b>
						button beside the <b>Target server</b> field to
						do so. Result:
					</li>
					
					<br></br>
					<img border="0"
						SRC="images/AreaProject.JPG" />
					<br></br>
					
					<li>Click <b>Finish</b> to go back to the <b>Dynamic Web Project</b> wizard.</li>
					<li>					
						Enter
						<tt>area</tt>
						into the
						<b>Module Name</b>
						field.  Result:
					</li>
					<br></br>
					<img border="0"
						SRC="images/area.JPG" />
					<br></br>
					<li>
						Click
						<b>Finish</b>.
					</li>
					<li>
					Say <b>Yes</b> to open the J2EE perspective and you will
					see the <tt>AreaProject</tt> you created listed
					under <b>Dynamic Web Projects</b> in the <b>Project Explorer</b>
					view. Expand the <tt>AreaProject</tt> project to
					see its contents.
					</li>
				</ol>

			</ol>
		</p>
		

		<h2>Create a top down Java bean Web service</h2>

		<ol>
			<li>
				Import the file
				<a href="srcfiles/AreaService.wsdl">AreaService.wsdl</a>
				into AreaProject/area/WebContent.
			</li>

			<li>Select the <b>AreaService.wsdl</b> file.</li>

			<li>
				Open File -&gt; New -&gt; Other... -&gt; Web Services -&gt; Web
				Service.
			</li>
			<li>Click <b>Next</b></li>
			<li>
				In the Web service type combo, select
				<b>Top down Java bean Web Service</b>
				.
			</li>

			<li>
				Select
				<b>Overwrite files without warning</b>
				. Result:
			</li>
			
			<br/>
			<img border="0"
				SRC="images/td1.JPG" />
		    <br/>
		    
			<li>
				Click
				<b>Next</b>
				. Result:
			</li>

			<br></br>
			<img border="0"
				src="images/td2.JPG" />
			<br></br>

			<li>
				Click
				<b>Next</b>
				to get to the
				<b>Service Deployment Configuration Page</b>
				. Result:
			</li>
	
			<br></br>
			<img border="0"
				src="images/td3.JPG" />
			<br></br>
	
			<li>
				If you want to choose a server different 
				from the one defaulted by the wizard, click the
				<b>Edit</b>
				button to:
				<a href="../SelectServer/SelectServer.html">
					select a server
				</a>
			</li>
			<li>
				Click
				<b>Finish</b>.
			</li>
			<li>
				It will take about one minute for the wizard to assemble
				the Web service projects, start Apache Tomcat and deploy
				the projects to Tomcat. Once finished, the Java
				implementation file
				<b>AreaServiceSOAPImpl.java</b>
				will appear in the browser view. Fill in the expression
				<b>parameters.getHeight()*parameters.getWidth()</b>
				as the return value for the <tt>calculateRectArea</tt> method and save the file.
			</li>

			<br />
			<img border="0"
				SRC="images/td4.JPG" />
		</ol>
		<p>
			Follow the instructions at
			<a href="../WebServiceClient/WebServiceClient.html">
				Creating Web Service Client
			</a>
			to create a Web service client that you can then use to test this Web service.
		</p>

	</body>
</html>
