The session-timeout element defines the default
        session timeout interval for all sessions created
        in this web application. The specified timeout
        must be expressed in a whole number of minutes.
        If the timeout is 0 or less, the container ensures
        the default behaviour of sessions is never to time
        out. If this element is not specified, the container
        must set its default timeout period.
