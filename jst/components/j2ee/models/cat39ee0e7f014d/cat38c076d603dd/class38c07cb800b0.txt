The user-data-constraint element is used to indicate how data communicated between the client and container should be protected
