The webservices element is the root element for the web services deployment descriptor.  It specifies the set of Web service descriptions that are to be deployed into the J2EE Application Server and the dependencies they have on container resources and services.

