EnterpriseJavaBean is a class.  It can have instances, someone could write an 
instance document containing Departments and Employees.  It also has 
attributes, operations, and associations.  These are actually derived/filtered 
from its implementation classes and interfaces.  For mapping and browsing 
purposes, though, you would like the EJB to appear as a class.  

In this light, even Session Beans can have associations and properties 
implemented by their bean.  For example, it would be meaningful to describe 
associations from a Session to the Entities which it uses to perform its work.

