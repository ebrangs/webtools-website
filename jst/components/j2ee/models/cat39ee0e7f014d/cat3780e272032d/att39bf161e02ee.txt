Specifies that, within a particular relationship, the lifetime of one or more entity beans is dependent upon the lifetime of another entity bean. The cascade-delete element can Cascade delete can only be specified for an EJBRelationshipRole contained in an EJBrelation in which the other EJBRelationshipRole element specifies a multiplicity of One.



