The message-selector element is used to specify the JMS message
selector to be used in determining which messages a message-driven
bean is to receive.

Example value: 
JMSType = `car' AND color = `blue' AND weight > 2500

