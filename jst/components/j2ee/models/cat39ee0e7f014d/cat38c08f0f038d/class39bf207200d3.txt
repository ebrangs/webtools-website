The resource-env-refType is used to define
resource-env-type elements.  It contains a declaration of a
Deployment Component's reference to an administered object
associated with a resource in the Deployment Component's
environment.  It consists of an optional description, the
resource environment reference name, and an indication of
the resource environment reference type expected by the
Deployment Component code.

Example:

<resource-env-ref>
    <resource-env-ref-name>jms/StockQueue
    </resource-env-ref-name>
    <resource-env-ref-type>javax.jms.Queue
    </resource-env-ref-type>
</resource-env-ref>
