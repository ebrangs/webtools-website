@since J2EE1.4
The message-destinationType specifies a message
destination. The logical destination described by this
element is mapped to a physical destination by the Deployer.

The message destination element contains:

        - an optional description
        - an optional display-name
        - an optional icon
        - a message destination name which must be unique
          among message destination names within the same
          Deployment File.

Example:

<message-destination>
        <message-destination-name>CorporateStocks
        </message-destination-name>
</message-destination>
