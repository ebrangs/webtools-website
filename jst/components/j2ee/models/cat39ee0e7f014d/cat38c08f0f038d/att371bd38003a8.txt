The env-entry-value designates the value of a
        Deployment Component's environment entry. The value
        must be a String that is valid for the
        constructor of the specified type that takes a
        single String parameter, or for java.lang.Character,
        a single character.

        Example:

        <env-entry-value>100.00</env-entry-value>
