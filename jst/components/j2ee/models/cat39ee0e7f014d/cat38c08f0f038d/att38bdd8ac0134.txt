The role-link element is a reference to a defined
        security role. The role-link element must contain
        the name of one of the security roles defined in the
        security-role elements.

