@since J2EE1.4
The display-name type contains a short name that is intended
to be displayed by tools. It is used by display-name
elements.  The display name need not be unique.

Example:

...
   <display-name xml:lang="en">Employee Self Service</display-name>

The value of the xml:lang attribute is "en" (English) by default.
