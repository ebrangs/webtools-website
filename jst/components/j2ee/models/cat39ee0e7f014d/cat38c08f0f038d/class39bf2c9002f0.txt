The use-caller-identity element specifies that the caller's security identity be used as the security identity for the execution of the enterprise bean's methods.

