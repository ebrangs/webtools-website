The role-name element contains the name of a security role.

The name must conform to the lexical rules for an NMTOKEN.
