The element adminobject-class specifies the fully
        qualified Java class name of an administered object.

        Example:
            <adminobject-class>com.wombat.DestinationImpl
            </adminobject-class>
