Contains the name of a configuration property.

The connector architecture defines a set of well-defined properties all of type java.lang.String. These are as follows:

     ServerName
     PortNumber
     UserName
     Password
     ConnectionURL

A resource adapter provider can extend this property set to include properties specific to the resource adapter and its underlying EIS.

