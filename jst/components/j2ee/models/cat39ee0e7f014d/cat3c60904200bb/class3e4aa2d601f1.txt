@since J2EE1.4
The messageadapterType specifies information about the
messaging capabilities of the resource adapter. This
contains information specific to the implementation of the
resource adapter library as specified through the
messagelistener element.
