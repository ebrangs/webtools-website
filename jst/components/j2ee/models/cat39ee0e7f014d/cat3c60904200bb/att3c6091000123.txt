Contains information about the type of the EIS. For example, the type of an EIS can be product name of EIS independent of any version info.

This helps in identifying EIS instances that can be used with
this resource adapter.

