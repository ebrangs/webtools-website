Returns true if the method is system generated.
This is usually determined by the @generated tag in the comment.
