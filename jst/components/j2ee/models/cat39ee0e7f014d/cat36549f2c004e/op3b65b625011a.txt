 Return an Field with the passed name from this JavaClass or any supertypes.

Return null if a Field named fieldName is not found.
