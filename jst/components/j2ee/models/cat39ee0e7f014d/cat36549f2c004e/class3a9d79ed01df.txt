Describes a Java Array type
   For multi-dimensional arrays, it is unlikely that the component type will be
   specified directly.  This would require instantiating a chain of component types
   such as String[][][][]->String[][][]->String[][]->String[]->String.

  The component type relationship will be computed if the finalComponentType
  and array dimensions is specified.
 
  For this reason, the preferred way to create is through the JavaRefFactory factory method:
       createArrayType(JavaClass finalComponentType, int dimensions)
