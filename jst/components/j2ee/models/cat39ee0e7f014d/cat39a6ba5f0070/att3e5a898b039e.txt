Provides the signature, as in the Java Language
        Specification, of the static Java method that is
        to be used to implement the function.

        Example:

        java.lang.String nickName( java.lang.String, int )
