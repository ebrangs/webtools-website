The taglib tag is the document root, it defines:

description     a simple string describing the "use" of this taglib,
                should be user discernable

display-name    the display-name element contains a
                short name that is intended to be displayed
                by tools

icon            optional icon that can be used by tools

tlib-version    the version of the tag library implementation

short-name      a simple default short name that could be
                used by a JSP authoring tool to create
                names with a mnemonic value; for example,
                the it may be used as the prefered prefix
                value in taglib directives

uri             a uri uniquely identifying this taglib

validator       optional TagLibraryValidator information

listener        optional event listener specification

tag             one or more tags in the tag library

tag-file        one or more tag files in the tag library

function        zero or more EL functions defined in this
                tag library

taglib-extension zero or more extensions that provide extra
		information about this taglib, for tool
		consumption
