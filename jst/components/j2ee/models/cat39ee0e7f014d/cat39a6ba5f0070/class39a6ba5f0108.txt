The tag element defines an action in this tag library. The tag element has one attribute, id.
The tag element may have several subelements defining:
name -- The unique action name
tag-class -- The tag handler class implementing javax.servlet.jsp.tagext.Tag
tei-class -- An optional subclass of javax.servlet.jsp.tagext.TagExtraInfo
body-content -- The body content type
display-name -- A short name that is intended to be displayed by tools
small-icon -- Optional small-icon that can be used by tools
large-icon -- Optional large-icon that can be used by tools
description -- Optional tag-specific information
variable -- Optional scripting variable information
attribute -- All attributes of this action
example -- Optional informal description of an example of a use of this action.

