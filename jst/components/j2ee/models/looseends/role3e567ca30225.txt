@since J2EE1.4
The service-endpoint element contains the
        fully-qualified name of the enterprise bean's web
        service endpoint interface. The service-endpoint
        element may only be specified for a stateless
        session bean. The specified interface must be a
        valid JAX-RPC service endpoint interface.
