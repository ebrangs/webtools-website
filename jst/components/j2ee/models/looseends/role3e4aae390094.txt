The messagelistener-type element content must be
        unique in the messageadapter. Several messagelisteners
        can not use the same messagelistener-type.
