The resource-env-ref-type element specifies the type
        of a resource environment reference.  It is the
        fully qualified name of a Java language class or
        interface.
