Provides the fully-qualified class name of the Java
        class containing the static method that implements
        the function.
