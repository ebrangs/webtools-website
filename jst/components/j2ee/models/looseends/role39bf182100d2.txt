A declaration of the CMRField, if any, by means of which the other side of the relationship is accessed from the perspective of the role source

@migration EJB1.1: Used to be handled via ibmejbext::EjbRole::attributes list
