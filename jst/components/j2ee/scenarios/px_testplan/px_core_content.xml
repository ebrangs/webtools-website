<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../../.." />
		<title>test plan: wtp project explorer.</title>
	</head>
	
	<body>
		<h1>wst common</h1>
		<h2>WTP Project Explorer Core Function</h2>
		<p>The WTP Project Explorer is developed as a generic framework for
		third party vendors to extend. Basic facilities for improving usability
		and coordination among contributions are discussed below.
		</p>		
		<br/>

		<H5>Show Projects / Working Sets</H5>
		<p>By default, when the WTP Project Explorer surfaces, the available
		module groupings will be shown.</p>
		<table cellspacing="10" cellpadding="10">
			<tr>
				<td>
				<p><img src="./images/default_open.jpg"/></p>
				</td>
			</tr>
			<tr>
				<td>
				<p><i>Figure 1: By default, the available module groupings are
				presented to the user.</i></p>
				</td>
			</tr>
		</table>		
		<p>These groupings associate similar projects under a logical
		("abstract") header. Users can choose to show
		only the simple projects by 
		</p>		
		
		<table cellspacing="10" cellpadding="10">
			<tr>
				<td>
				<p><img src="./images/group_projects_button.jpg"/></p>
				</td>
			</tr>
			<tr>
				<td>
				<p><i>Figure 2: Select "Show Projects/ Working Sets" to disable
				or enable the Groups.</i></p>
				</td>
			</tr>
		</table>		
		<p>Select "Projects" from the WTP Project Explorer View Menu. Ensure that the projects appear in the root of the 
		viewer. Select "Working Sets" from the WTP Project Explorer View Menu. Ensure that the projects are grouped.
		</p>
		<table cellspacing="10" cellpadding="10">
			<tr>
				<td>
				<p><img src="./images/grouped_view.jpg"/></p>
				</td>
				<td>
				<p><img src="./images/ungrouped_view.jpg"/></p>
				</td>
			</tr>
			<tr>
				<td colspan="2">
				<p><i>Figure 3: Ensure that when the "Working Sets" 
				is selected, the projects are grouped. Otherwise, the 
				projects should appear on the root of the viewer.</i></p>
				</td>
			</tr>
		</table> 
		
		
		
		<H5>Collapse All</H5>
		<p>The "Collapse All" button pushes the WTP Project Explorer into a 
		fully collapsed state. Expand a few items in the viewer and then 
		toggle the "Collapse All" button. 
		</p>
		<table cellspacing="10" cellpadding="10">
			<tr>
				<td>
				<p><img src="./images/collapse_all_button.jpg"/></p>
				</td> 
			</tr>
			<tr>
				<td >
				<p><i>Figure 4: The "Collapse All" button should force 
				all expanded nodes into a collapsed state.</i></p>
				</td>
			</tr>
		</table>
		
		<H5>Link with Editor</H5>
		<p>The "Link with Editor" button keeps the current selection in the 
		WTP Project Explorer in sync with the open editors (when possible). 
		Unlike the Java Package Explorer or the Resource Navigator, it may not
		always be possible to isolate an exact selection. However, resource 
		selections and Java selections should always be functional. 
		</p>
		<table cellspacing="10" cellpadding="10">
			<tr>
				<td>
				<p><img src="./images/link_with_editor_button.jpg"/></p>
				</td> 
			</tr>
			<tr>
				<td >
				<p><i>Figure 5: When the Link with Editor button is depressed, 
				an appropriate selection should be automatically highlighted 
				based on the active editor. When the selection in the WTP Project 
				Explorer is changed by the user, an appropriate editor should be 
				activated as needed.</i></p>
				</td>
			</tr>
		</table>		
		<p>Before proceeding, ensure that you have created a few Java classes and 
		simple text file. If you skipped the previous section on creating Java and 
		resource content, please return to it now before proceeding. The following 
		steps require at least the creation of a Java project with a Java class and 
		a text file. Together, these two artifacts should be sufficient to make sure that 
		the "Link with Editor" support is working as designed for Java and resource 
		content.</p>	 
		
		<p>Now that you have a few resources to use for this test, toggle the 
		"Link with Editor" button so that it is in a depressed state. Double 
		click TestClass.java under Java1/src/test.pakg/TestClass.java to open
		this file in the Java editor. Then double click TestFile.txt to open 
		this file in a text editor. Now as you change your selection in the WTP
		Project Explorer, the correct editor should automatically activate and 
		come to the forefront. When you select the editor, it should activate the
		correct selection in the WTP Project Explorer.
		</p>
		<br/>
		
		<H5>Filters and Types of Content</H5>
		<P>In order to customize the look and feel of the WTP Project Explorer
		and to allow the ability to show or hide content based on a user's
		input, filters are used. The filters can prevent an uninterested user
		from seeing types of extraneous content or files.</P>
		<P>The filters dialog is accesed by hitting the triangle on the top
		right of the Project Explorer and selecting Filters...</P>
		<TABLE cellspacing="10" cellpadding="10">
			<TR>
				<TD>
				<P><IMG src="images/notesbuddy3532_7041.jpg"/></P>
				</TD>
			</TR>
			<TR>
				<TD>
				<P><I>Figure 6: Select "Filters..." from the viewer menu in the
				Project Explorer.</I></P>
				</TD>
			</TR>
		</TABLE>
		<P>The first tab of this dialog is the Filters tab:</P>
		<TABLE cellspacing="10" cellpadding="10">
			<TR>
				<TD>
				<P><IMG src="images/notesbuddy3532_7720.jpg"/></P>
				</TD>
			</TR>
			<TR>
				<TD>
				<P><I>Figure 7: The Filters dialog has two tabs. The first tab,
				&quot;Filters&quot;, allows you to select content to hide. </I></P>
				</TD>
			</TR>
		</TABLE>
		<P>It is important to note that filters which are checked here are
		enabled to hide content.</P>
		<UL>
			<LI>
			<P>As a simple test, check and uncheck filters here and make sure the
			content is updated accordingly in the WTP Project Explorer.</P>
			</LI>
		</UL>
		<P>The second tab of this dialog is the &quot;Types of content&quot;
		page:</P>

		<TABLE cellspacing="10" cellpadding="10">
			<TR>
				<TD>
				<P><IMG src="images/notesbuddy3532_8588.jpg"/></P>
				</TD>
			</TR>
			<TR>
				<TD>
				<P><I>Figure 8: <I>The second tab, .&quot;Types of content&quot;,
				allows to choose what broad types of content to show. </I></I></P>
				</TD>
			</TR>
		</TABLE>
		<P>It is import to note that unlike the first page, extensions checked
		here will enable to show content, not hide it.</P>
	
		<UL>
			<LI>
			<P>As a simple test, check and uncheck navigator extensions in the
			content list and ensure the WTP Project Explorer is updated
			accordingly.</P>
			</LI>
		</UL>
	
		<H5>Selecting Working Sets to view</H5>
		<p>In the "Working set" mode select "Configure Working Sets...". 
		</p>
		<table cellspacing="10" cellpadding="10">
			<tr>
				<td>
				<p><img src="./images/configure_working_set.jpg"/></p>
				</td> 
			</tr>
			<tr>
				<td >
				<p><i>Figure 9: Configuring working sets.</i></p>
				</td>
			</tr>
		</table>
		<p>Uncheck "Ejb Projects" and "Connector Projects". Ensure that the Project Explorer does not show
		Ejb Projects and Connector Projects in the navigator and the the Ejb Projects and Connector Projects
		are moved in the "Other Projects" if they do not contain any other components.
		</p>
		<table cellspacing="10" cellpadding="10">
			<tr>
				<td>
				<p><img src="./images/configure_working_set_dialog.jpg"/></p>
				</td> 
				<td>
				<p><img src="./images/configured_navigator.jpg"/></p>
				</td> 
			</tr>
			<tr>
				<td >
				<p><i>Figure 10: Configuring working sets dialog.</i></p>
				</td>
				<td >
				<p><i>Figure 11: Navigator not showing Ejb Projects and Connector Projects.</i></p>
				</td>
			</tr>
		</table>
				
				
			<p>Return to <A HREF="px_overview.html">overview</A>.</p>		
		
		
		
						
	</body>
</html>		
