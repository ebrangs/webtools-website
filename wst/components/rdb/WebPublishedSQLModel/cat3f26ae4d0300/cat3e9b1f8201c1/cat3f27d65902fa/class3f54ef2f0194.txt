4.13 Columns, fields, and attributes

The terms column, field, and attribute refer to structural components of tables, row types, and structured types, respectively, in analogous fashion. As the structure of a table consists of one or more columns, so does the structure of a row type consist of one or more fields and that of a structured type one or more attributes. Every structural element, whether a column, a field, or an attribute, is primarily a name paired with a declared type.

The elements of a structure are ordered. Elements in different positions in the same structure can have the same declared type but not the same name. Although the elements of a structure are distinguished from each other by name, in some circumstances the compatibility of two structures (for the purpose at hand) is determined solely by considering the declared types of each pair of elements at the same ordinal position.

An attribute A is described by an attribute descriptor. An attribute descriptor includes:
 - The name of the attribute.
 - The data type descriptor of the declared type of A.
 - The ordinal position of A within the structured type that contains it.
 - The value of the implicit or explicit <attribute default> of A.
 - If the data type of the attribute is a reference type, then an indication of whether reference values shall be checked, and the <reference scope check action> , if specified or implied.
 - The name of the structured type defined by the <user-defined type definition> that defines A.

