4.13 Columns, fields, and attributes

The terms column, field, and attribute refer to structural components of tables, row types, and structured types, respectively, in analogous fashion. As the structure of a table consists of one or more columns, so does the structure of a row type consist of one or more fields and that of a structured type one or more attributes. Every structural element, whether a column, a field, or an attribute, is primarily a name paired with a declared type.

The elements of a structure are ordered. Elements in different positions in the same structure can have the same declared type but not the same name. Although the elements of a structure are distinguished from each other by name, in some circumstances the compatibility of two structures (for the purpose at hand) is determined solely by considering the declared types of each pair of elements at the same ordinal position.

A table (see Subclause 4.14, "Tables") is defined on one or more columns and consists of zero or more rows. A column has a name and a declared type. Each row in a table has exactly one value for each column. Each value in a row is a value in the declared type of the column.
NOTE 21: The declared type includes the null value and values in proper subtypes of the declared type. Every column has a nullability characteristic that indicates whether the value from that column can be the null value. The possible values of nullability characteristic are known not nullable and possibly nullable.

A column C is described by a column descriptor. A column descriptor includes:
 - The name of the column.
 - Whether the name of the column is an implementation-dependent name.
 - If the column is based on a domain, then the name of that domain; otherwise, the data type descriptor of the declared type of C.
 - The value of <default option> , if any, of C.
 - The nullability characteristic of C.
 - The ordinal position of C within the table that contains it.
 - An indication of whether C is a self-referencing column of a base table or not.
 - If the declared type of C is a reference type, then an indication of whether references are to be checked, and the <reference scope check action> , if specified or implied.
 - An indication of whether C is an identity column or not.
 - If C is an identity column, then an indication of whether values are always generated or generated by default.
 - If C is an identity column, then the start value of C.
 - If C is an identity column, then the descriptor of the internal sequence generator for C.
NOTE 22 - Identity columns and the meaning of "start value" are described in Subclause 4.14.4, "Identity columns".

 - If C is a generated column, then the generation expression of C.
NOTE 23 - Generated columns and the meaning of "generation expression" are described in Subclause 4.14.5, "Base columns and generated columns".

An attribute A is described by an attribute descriptor. An attribute descriptor includes:
 - The name of the attribute.
 - The data type descriptor of the declared type of A.
 - The ordinal position of A within the structured type that contains it.
 - The value of the implicit or explicit <attribute default> of A.
 - If the data type of the attribute is a reference type, then an indication of whether reference values shall be checked, and the <reference scope check action> , if specified or implied.
 - The name of the structured type defined by the <user-defined type definition> that defines A.

A field F is described by a field descriptor. A field descriptor includes:
 - The name of the field.
 - The data type descriptor of the declared type of F.
 - The ordinal position of F within the row type that simply contains it.
 - If the data type of the field is a reference type, then an indication of whether reference values shall be checked, and the <reference scope check action> , if specified or implied.

