4.14 Tables

A table is a collection of rows having one or more columns. [...]

A table is either a base table, a derived table, or a transient table.

A base table is either a persistent base table, a global temporary table, a created local temporary table, or a declared local temporary table.

