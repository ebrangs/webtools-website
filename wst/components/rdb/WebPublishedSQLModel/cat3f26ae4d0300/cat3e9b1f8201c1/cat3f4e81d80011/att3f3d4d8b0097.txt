From the below, typical values would be: "DEFINER", "INVOKER", "IMPLEMENTATION DEFINED"
Since there are several implementation defined security designations, it is best to go with the string.

From 5WD-02-Foundation-2002-12
11.50 <SQL-invoked routine>

<external security clause> ::=
  EXTERNAL SECURITY DEFINER
  | EXTERNAL SECURITY INVOKER
  | EXTERNAL SECURITY IMPLEMENTATION DEFINED

4.27 SQL-invoked routines
If the SQL-invoked routine is an SQL routine, then the identifiers are determined according to the SQL security
characteristic of the SQL-invoked routine:
- If the SQL security characteristic is DEFINER , then:
  * If the routine authorization identifier is a user identifier, the user identifier is set to the routine authorization
    identifier and the role name is set to null.
  * Otherwise, the role name is set to the routine authorization identifier and the user identifier is set to
    null.
- If the SQL security characteristic is INVOKER , then the identifiers remain unchanged.      
If the SQL-invoked routine is an external routine, then the identifiers are determined according to the external
security characteristic of the SQL-invoked routine:
- If the external security characteristic is DEFINER , then:
  * If the routine authorization identifier is a user identifier, then the user identifier is set to the routine
    authorization identifier and the role name is set to the null value.
  * Otherwise, the role name is set to the routine authorization identifier and the user identifier is set to the
    null value.
- If the external security characteristic is INVOKER , then the identifiers remain unchanged.
- If the external security characteristic is IMPLEMENTATION DEFINED , then the identifiers are set to
implementation-defined values.
