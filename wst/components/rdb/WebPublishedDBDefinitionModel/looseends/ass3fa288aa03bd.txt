UserDefinedTypes are contained by reference, ie the column does not own the UserDefinedType instance.  A Column must have one of, but not both, a containedType or a referencedType. 

Setting the containedType on a column will remove any referencedType, and vice versa.
