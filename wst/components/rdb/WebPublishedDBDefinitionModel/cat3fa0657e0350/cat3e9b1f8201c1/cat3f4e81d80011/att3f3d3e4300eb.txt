Maximun number of result sets the procedure can return.  If the stored procedure returns more result sets than is specified by this value, then a database error will be generated.

From 5WD-02-Foundation-2002-12
11.50 <SQL-invoked routine>
<dynamic result sets characteristic> ::= DYNAMIC RESULT SETS <maximum dynamic result sets>
