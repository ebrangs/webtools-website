4.23 SQL-invoked routines

An SQL-invoked routine is described by a routine descriptor. A routine descriptor contains (among other things):
- If the SQL-invoked routine is an SQL routine, then the SQL routine body of the SQL-invoked routine.
