4.7 User-defined types

A user-defined type is a schema object, identified by a <user-defined type name>.

A user-defined type is described by a user-defined type descriptor. A user-defined type descriptor contains:
 - The name of the user-defined type (<user-defined type name> ). This is the type designator of that type, used in type precedence lists (see Subclause 9.5, "Type precedence list determination").
 - An indication of whether the user-defined type is a structured type or a distinct type.
 - The ordering form for the user-defined type (EQUALS , FULL , or NONE ).
 - The ordering category for the user-defined type (RELATIVE , MAP , or STATE ).
