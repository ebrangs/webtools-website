package org.eclipse.wst.sse.examples.multipage.tabletree;

public class ElementCountInfo {
	private String fName;
	private int fCount;
	
	public ElementCountInfo(String name, int count) {
		fName = name;
		fCount = count;
	}
	public String getName(){
		return fName;
	}
	public int getCount() {
		return fCount;
	}
}
