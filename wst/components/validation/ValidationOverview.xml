<?xml version="1.0" encoding="UTF-8"?>
<?xml-stylesheet type="text/xsl" href="../../../wtp.xsl"?>
<html>
	<head>
		<meta name="root" content="../../../../" />
		<title>overview</title>
	</head>

   <body>
      <h1>wst validation</h1>
      <br/>
      <h1>Validation Framework Overview</h1>

      <h2>Introduction</h2>

      <p>The Validation Component in the wst sub project provides an extendable framework that is used to develop new custom validators to validate resources in the workbench. The resources can be an eclipse Resource (IResource) or an EMF Resource (EObject). The framework provides a set of interface and api to control how, when and what type of valdation is launched on the resources in the workbench. The framework puts out problems found during validation as problem markers in the Problems View of the workbench. The framework provides a set of function and the details of each of this functions are discussed in the following section.</p>
	  <br />
      <h2>Functional Overview</h2>

      <p>This section provides a functional overview of the validation framework in the validation component.</p>
	  <br /> 	
      <h4>Validation Registration</h4>
	  
      <p>All custom validators developed extending the framework are registered on a IProject, when registered the project nature type is used for registration. The registration ensures that the custom validator runs on only on projects with that project nature. This registration is done in the custom validator extension implementation.</p>
      <br />
      <h4>Enabling/Disabling Validation</h4>
      <p>All custom validators developed can enabled/disabled at 2 levels, one at the project level and one globally. The project level has the option to enable it even though it is diabled globally. The global enablement/disablement is available thorugh the Validation Perferences page (Window -&gt; Preferences -&gt; Validation). The project level enablement/disablement is available through the project proerties page (Right Click on IProject -&gt; Properties -&gt; Validation). By default all custom validators are enabled by default at project level and global level. There are more options on enablement Global Validation Preferences pagen and the Project Validation properties page that are self explaintory.</p>
      <br />
      <h4>Manual Validation</h4>
      <p>The framework provides validation to be triggered manually on resources using the "Run Validation" pop up menu item that is available on resources. The Run Validation runs all enabled validators registered on the resource. The manual validation provides an progress dialog to display the status of the validation.</p>
      <br />
      <h4>Incremental Validation</h4>
      <p>Incremental validation is provided by the framework using the core Incremental Builder technology. The framework defines a validation builder that is of a incremental builder type. The Valdiation Builder is added to the build spec of the project. The builder triggers all enabled custom validators on a Auto Build in the workbench. The validation builder uses the core delta that is maintained by the eclipse Build Manager to determin the kind validation to be triggered on the resources that have custom valdators registered.</p>
      <br />
      <h4>Dependency Validation</h4>
      <p>The dependency validation works on IProjects. This dependency validation when set to true in the extension implementation of a custom validator validate all project referencing the current project being validated. This dependency check is recurrsive and goes down the chain of project referencing the referenced projects.</p>
      <br />
      <h4>Validation Filters</h4>
      <p>A custom validator developed extending the framework has the option to provide file filters to be validated. These filter can specified as file type or even specific files to be validated. These resource defined in the filters are only validated when validation is run manually or incrementally. An example of defining a filter is shown in the SampleValidation extension below.</p>
      <br />
      <h4>Validation Markers</h4>
      <p>The framework extends the core eclipse problemMarker extension point and has a Validation marker type. The problem found during validation are put out as Validation Markers in the problems view of the workbench. The validation markers provides 3 levels of severity (Errors, Warning and Informational) for a custom validator to display problem found during validation. It is the resposiblity of the custom validator implementor to cleanup the problem markers that it has put out on each run of the validator. The framework provides api to cleanup existing validation problem markers.</p>
      <br />
      <h4>Extension points defined</h4>
      <p>The framework extension points are defined in the org.eclipse.wst.validation plugin. The extensions point are listed below.
      <br />
      <pre>&lt;extension-point id="validator" name="Validator" schema="xsds/validatorExtSchema.exsd" /&gt;</pre>
      <br />
      Every validator must extend  org.eclipse.wst.validation.validator. Provided by the validation framework, this extension point is packaged in the  org.eclipse.wst.validation plugin. point must always have a value of  org.eclipse.wst.validation.validator 
      <br />
      <pre>&lt;extension-point id="referencialFileValidator" name="ReferencialFileValidator" schema="xsds/referencialFileExtSchema.exsd"/&gt;</pre>
      <br />
      This extension point is used to referenced file validation where the implementer of the this extension point gives a flattened list of referenced file to be validated for a given file. By extending this extension point custom file validators can validate files referencing the current file being valdiated.
	  </p>
	  <br />
      <h4>SampleValidator Extension</h4>
      <p>
      The extension below shows an implmentation of the custom EJB Validator extending the validator extension point from the framework.
      </p> 
	<pre>	
		&lt;extension
	         id="EJBValidator"
	         name="%EJB_VALIDATOR"
	         point="org.eclipse.wst.validation.validator"&gt;
	      	&lt;validator&gt;
		         &lt;projectNature
		               id="org.eclipse.wst.common.modulecore.ModuleCoreNature"&gt;
		         &lt;/projectNature&gt;
		         &lt;filter
		               objectClass="org.eclipse.core.resources.IFile"
		               nameFilter="ejb-jar.xml"&gt;
		         &lt;/filter&gt;
		         &lt;filter
		               objectClass="org.eclipse.core.resources.IFile"
		               nameFilter="*.class"&gt;
		         &lt;/filter&gt;
		         &lt;filter
		               objectClass="org.eclipse.core.resources.IFile"
		               nameFilter="*.java"&gt;
		         &lt;/filtergt;
		         &lt;helper
		               class="org.eclipse.jst.j2ee.internal.ejb.workbench.validation.EJBHelper"&gt;
		         &lt;/helper&gt;
		         &lt;dependentValidator
		               depValValue="true"&gt;
		         &lt;/dependentValidator&gt;
		         &lt;markerId
		               markerIdValue="EJBValidatorMarker"&gt;
		         &lt;/markerId&gt;
		         &lt;run
		               class="org.eclipse.jst.j2ee.internal.ejb.workbench.validation.UIEjbValidator"&gt;
		         &lt;/run&gt;
	      	&lt;/validator&gt;
	   &lt;/extension&gt;
	  </pre>	
      <br />
      
      <h4>Custom Marker Support (New in M3)</h4>
      <p>
       A new feature has been added to the validation framework where custom validator developers can define their own problem marker for their validator. The user defined marker Id is used to add problem markers for that validator. The customer marker support gives the flexiblity of filtering the problem markers for a validator from the filters dialog, these filters filter the problem markers in the problems view. To get this functinalaity the the custom validator needs to be define a <b>markerId</b> element in the validator extension and a problem marker extension as shown below.
      </p>
      <pre>
      &lt;markerId
            markerIdValue="EJBValidatorMarker"&gt;
      &lt;/markerId>
      </pre>
      <p>
      A problem marker extension extending the core problem marker needs to be defined with the same id of the markerId as shown below.
      </p>
      <pre>
      &lt;extension
         id="EJBValidatorMarker"
         name="%EJB_VALIDATION_PROBLEMMARKER_NAME"
         point="org.eclipse.core.resources.markers"&gt;
      &lt;super
            type="org.eclipse.wst.validation.problemmarker"&gt;
      &lt;/super&gt;
      &lt;persistent
            value="true"&gt;
      &lt;/persistent>
      &lt;attribute
            name="owner">
      &lt;/attribute>
      &lt;attribute
            name="validationSeverity"&gt;
      &lt;/attribute>
      &lt;attribute
            name="targetObject"&gt;
      &lt;/attribute>
      &lt;attribute
            name="groupName"&gt;
      &lt;/attribute>
      &lt;attribute
            name="messageId"&gt;
      &lt;/attribute>
     &lt;/extension>
      </pre>
     
      <h2>API Overview</h2>

      <p>The following document provides an overview of the API available in the framework.
      <br />
	  <a href="ValidationFrameworkAPI.html">Validation Framework Api</a>
	  </p>

      <h2>Component Plugins and Dependency</h2>

      <p>The following picture depicts ths plugin dependencies of plugins that are in the validation component.</p>
      <br/>
      <img border="0" src="images/ValidationPluginDependency.jpg" />
   	  <br/>
     
   </body>
</html>

