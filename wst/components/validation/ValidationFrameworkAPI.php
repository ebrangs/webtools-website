<?php                                                                                                                                                                                                                                           require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");   require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");  $App    = new App();    $Nav    = new Nav();    $Menu   = new Menu();           include($App->getProjectCommon());    # All on the same line to unclutter the user's desktop'
$pageTitle = "Common Tools Validation Component Overview";
$pageKeywords = "Eclipse WTP webtools";
$pageAuthor ="Ugur Yildirim @ Eteration A.S.";

$root = $_SERVER['DOCUMENT_ROOT'];
require_once ($root . '/webtools/common.php');

# Add page-specific Nav bars here
# Format is Link text, link URL (can be http://www.someothersite.com/), target (_self, _blank), level (1, 2 or 3)
# $Nav->addNavSeparator("My Page Links",        "downloads.php");
# $Nav->addCustomNav("My Link", "mypage.php", "_self", 3);
# $Nav->addCustomNav("Google", "http://www.google.com/", "_blank", 3);

$html = <<<EOHTML
$wtpTopButtons
<link type="text/css" href="/webtools/default_style.css" rel="stylesheet">
<div id="midcolumn" style="width: 95% !important; margin-left: 1.5em;">
<span style="float:right;"><a href="/webtools/wst/components/validation/ValidationOverview.php">Back</a></span>
<table border="0" cellpadding="2" cellspacing="5" width="100%">
<tbody>
<tr>
<td align="left" width="100%"><font class="indextop">wst validation</font>
<br>
<font class="indexsub">validation api overview</font></td>
</tr>
</tbody>
</table>
<blockquote><b>Vijay Bhadriraju</b><br />
IBM Rational<br />
January 22, 2005</blockquote>
<table border="0" cellpadding="2" cellspacing="5" width="100%">
<col width="16">
<col width="*">
<tbody>
<tr>
<td colspan="2" align="left" bgcolor="#0080c0" valign="top"><b><font color="#ffffff" face="Arial,Helvetica">Intoduction</font></b></td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<p>
		The following document provides a overview of the validation interfaces and the classes used in  WTP.
		
		</p>
</td>
</tr>
<tr>
<td colspan="2" align="left" bgcolor="#0080c0" valign="top"><b><font color="#ffffff" face="Arial,Helvetica">Overview</font></b></td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<br>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   
<ol>
      
<li>
<a href="#IValidator"><code>IValidator</code></a>
</li>
      
<li>
<a href="#IReporter"><code>IReporter</code></a>
</li>
      
<li>
<a href="#IHelper"><code>IHelper</code></a>
</li>
      
<li>
<a href="#IMessage"><code>IMessage</code></a>
</li>
      
<li>
<a href="#IFileDelta"><code>IFileDelta</code></a>
</li>
      
<li>
<a href="#ValidationException"><code>ValidationException</code></a>
</li>
      
<li>
<a href="#MessageLimitException"><code>MessageLimitException</code></a>
</li>
   
</ol>

</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<DL>
   
<DT>
<code>IValidator</code>
</DT>
   
<DD>
      An implementation of this interface validates a model. 
      <a href="#IValidator">[Details]</a>
   
</DD>
   
   
<DT>
<code>IReporter</code>
</DT>
   
<DD>
      The <code>IReporter</code> is the interface through which the 
      <code>IValidator</code> interacts with the user to report problems,
      report status, and receive cancellation requests.
      <a href="#IReporter">[Details]</a>
   
</DD>
   
   
<DT>
<code>IHelper</code>
</DT>
   
<DD>
      
<a name="resp_helper"></a>
      An <code>IHelper</code> is a loading mechanism for an <code>IValidator</code>.
      The <code>IHelper</code> separates the <code>IValidator</code> from its 
      environment, every model that the <code>IValidator</code>
      validates is retrieved by the <code>IHelper</code> implementation.
      <a href="#IHelper">[Details]</a>
   
</DD>
   
   
<DT>
<code>IMessage</code>
</DT>
   
<DD>
      The <code>IMessage</code> is the Locale-independent message that 
      the <code>IValidator</code> uses to report a problem to the user.
      <a href="#IMessage">[Details]</a>
   
</DD>
   
   
<DT>
<code>IFileDelta</code>
</DT>
   
<DD>
      When the validation is launched, the framework directs validators 
      to perform full or incremental validation through <code>IFileDelta</code>
      instances. An <code>IFileDelta</code> instance represents an object, 
      usually a file, that has changed on the user's system and needs to be
      revalidated. If no <code>IFileDelta</code> instances are passed to the 
      <code>IValidator</code>, the validator should revalidate everything.
      <a href="#IFileDelta">[Details]</a>
   
</DD>
   
   
<DT>
<code>ValidationException</code>
</DT>
   
<DD>
      A <code>ValidationException</code> is one of two types of exceptions that
      the <code>IValidator</code> may propagate to the framework. A 
      <code>ValidationException</code> contains an <code>IMessage</code> instance
      that will be reported to the user, and this exception may wrap another exception.
      <a href="#ValidationException">[Details]</a>
   
</DD>
   
   
<DT>
<code>MessageLimitException</code>
</DT>
   
<DD>
      A <code>MessageException</code> is one of two types of exceptions that
      the <code>IValidator</code> may propagate to the framework. A 
      <code>MessageLimitException</code> is thrown by the framework if the
      <code>IValidator</code> attempts to report a message when the maximum
      number of messages has already been reported. <code>MessageLimitException</code>
      does not contain an <code>IMessage</code> to be reported to the user.
      <a href="#MessageLimitException">[Details]</a>
   
</DD>

</DL>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top"><a href="#Convenience Classes">Convenience Classes</a></td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="IValidator">IValidator</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   
<code>IValidator</code> is an interface that, when implemented, validates a model. 
   A validator loads its model through an <code>IHelper</code>, traverses the model itself, 
   and communicates with the user through the <code>IReporter</code>.

   <p>
   There is no more than one instance of a validator. Because each validator
   instance performs on the same type of model input, and performs the
   same rule checks against that input, no more than one instance of a validator
   is needed.
	</p>
	
   
<p>
   A validator can perform full or incremental validation. The caller's
   preferred type of validation is indicated by the value of the <code>IFileDelta[]</code>
   passed into the <code>validate</code> method. If the <code>IFileDelta[]</code> is null 
   or empty, the caller wants full validation performed. Otherwise, the caller 
   wants only those files named in the array to be validated. But a validator 
   is not required to support incremental validation; it can ignore the 
   <code>IFileDelta[]</code> if it wants to. 
	</p>
   
<p>
   Because each validator must be environment independent, each file name
   must be independent of the environment. Since nothing can be done without
   some knowledge of the environment, the environment specific Helpers are
   used to convert the generic file names into environment specific objects,
   as necessary. But all of that is shielded from the validators.
	</p>
   
<p>
   When a validator adds a validation message, it identifies itself through
   a unique id; thus, when a particular's file's validation messages need
   to be removed (before incremental validation of that file begins), all
   messages associated with that file, by that validator, are removed. This
   ensures that validator A cannot modify or delete messages entered by
   validator B.
	</p>
	
   
<p>
   Every message shown to the user, whether it's the message in a <code>ValidationException</code>,
   or a validation error, will be displayed to the user. Validators do not display 
   messages to the user other than validation messages and subtask messages. This
   is necessary for Locale-neutrality and environment-neutrality.
   If a catastrophic error occurs, from which the validator cannot recover, the 
   validator should throw a <code>ValidationException</code>.
   </p>

</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="IReporter">IReporter</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   The <code>IReporter</code> is an interface that is used for interaction with the
   user. <code>IReporter</code> can show validation messages, remove validation messages,
   display subtask messages, and get cancellation input from the user. Every
   validator uses an <code>IReporter</code>, and an <code>IReporter</code> can be used by many <code>IValidator</code>
   implementations.

   <p>
   Each <code>IReporter</code> implementation is customized for a particular environment;
   in   Web Tools Project the <code>IReporter</code> reports validation messages via the task list.
   Generally, only one implementation of an <code>IReporter</code> is needed in an environment,
   unless the framework wishes to choose different optimization techniques based
   on the user's actions. For example, an incremental <code>IReporter</code> may need to cache 
   messages, while a full <code>IReporter</code> can immediately send the output to the user. 
   If a full validation is about to be performed, pass in the full <code>IReporter</code> for 
   savings in memory and faster performance. If incremental validation
   is about to be performed, the framework can pass in the incremental <code>IReporter</code> 
   to support addition and deletion of messages. Which type of reporter, full or 
   incremental, is hidden from the <code>IValidator</code>. These optimizations are 
   environment-specific and are handled by the framework.</p>

</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="IHelper">IHelper</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   The <code>IHelper</code> interface that is the loading mechanism for an <code>IValidator</code>.
   This interface defines the method(s) that will load and initialize a particular type
   of model for an <code>IValidator</code>. A model is any object
   or group of objects that is either validated, or whose meta-data is required to
   validate another object or group of objects.

   <p>The <code>IHelper</code>
   separates the <code>IValidator</code> from its environment (UI verses Headless); the <code>IValidator</code>
   asks the <code>IHelper</code> to retrieve an object, and the <code>IHelper</code>
   implementation knows how to load the object for a particular type of environment.
   If running in WebSphere Studio, for example, the <code>IHelper</code> implementation should know
   how to convert file names into <code>IFile</code> types; if running outside of WebSphere Studio, 
   a different <code>IHelper</code> implementation would convert the file name into a 
   <code>java.io.File</code> type. Different <code>IHelper</code> implementations are shipped
   with each environment.</p>

   
<p>
   There need not be more than one instance of an <code>IHelper</code> because the mechanism
   for loading a type of model should not vary in a given environment.
	</p>
   
<p>
   
<code>IHelper</code>s are not tied to a particular validator. Rather, <code>IHelper</code>s
   are mechanisms which can load a particular type of object in a particular environment.
   When <code>IHelper</code>s are independent of <code>IValidator</code>s, then that <code>IHelper</code> can be reused
   by any validator which needs to load that particular model.
   </p>

</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="IMessage">IMessage</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   
<code>IMessage</code> represents a locale independent validation message. If an
   <code>IValidator</code> is running on a server, and the client is running on a different
   <code>java.util.Locale</code> than the server, then the validation messages must be presented
   in the client's locale, not the server's. To enable the message text to
   be presented in the client's locale, instead of adding text to the <code>IReporter</code>
   directly, the <code>IValidator</code> passes an <code>IMessage</code> to the <code>IReporter</code>.

   <p>
   The <code>IMessage</code> contains some required data and may contain some optional data.<br>
   
<table BORDER="BORDER" WIDTH="100%">
      
<tr>
         
<td WIDTH="15%">Data</td>
         <td WIDTH="20%">Required (R) or <br>Optional (O)</td>
         <td WIDTH="65%">Description</td>
      
</tr>

      
<tr>
         
<td>bundle</td>
         <td>R</td>
         <td>
            The name of the resource bundle category which contains the string.
            (A resource bundle category identifies the base name of the .properties
            file, e.g. "mymessages.properties", "mymessages_en_US.properties" both
            have the same resource bundle category: "mymessages"
         </td>
      
</tr>

      
<tr>
         
<td>messageId</td>
         <td>R</td>
         <td>
            The id of the message in the resource bundle; used to extract the message
            from the bundle.
         </td>
      
</tr>

      
<tr>
         
<td>parms</td>
         <td>O</td>
         <td>
            If the text needs runtime parameters substituted in, this array of
            strings contains the parameters. Otherwise, this array is null.
         </td>
      
</tr>

      
<tr>
         
<td>target object</td>
         <td>O</td>
         <td>
            If the message applies to a particular object (e.g. a file), this parameter
            identifies the object. Otherwise, this value is null.
         </td>
      
</tr>

      
<tr>
         
<td>severity</td>
         <td>R</td>
         <td>
            Identifies the severity of the message. This flag's value is one of
            the SeverityEnum constants.
         </td>
      
</tr>
   
</table>
   
</p>

</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="IFileDelta">IFileDelta</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   
<code>IFileDelta</code> is used for incremental validation. An array of instances of this 
   type is passed into the <code>IValidator</code>'s validate method for processing. 
   An <code>IFileDelta</code> contains two pieces of information:
   the name of the file which has changed, and an integer flag identifying
   the type of change (ADDED, CHANGED, DELETED). Given an <code>IFileDelta</code>, an <code>IValidator</code>
   can perform validation on just the file which has changed, and perform
   the type of validation necessary, as indicated by the <code>IFileDelta</code>'s flag.
</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="ValidationException">ValidationException</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   The only exception that an <code>IValidator</code> may create and throw is a <code>ValidatorException</code>. When the 
   framework catches a <code>ValidationException</code>, the exception's <code>IMessage</code> will be extracted
   and shown to the user. If an <code>IValidator</code> catches an unexpected exception, instead of 
   propagating that exception up to the framework, the exception should be wrapped in a
   <code>ValidatorException</code> and the <code>ValidatorException</code> should be propagated up to the framework.
</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="MessageLimitException">MessageLimitException</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   If the framework imposes a limit on the number of validation messages which can be reported,
   then when that limit is reached, a <code>MessageLimitException</code> is thrown by the framework. <code>IValidator</code>s
   should not catch the <code>MessageLimitException</code>; instead, they should let the exception propagate
   up to the framework and the framework will do whatever cleanup is necessary.
</div>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<h4>
<a NAME="Convenience Classes">Convenience Classes</a>
</h4>
</td>
</tr>
<tr>
<td align="right" valign="top">&nbsp;</td><td valign="top">
<div class="compact">
   The validation framework provides, in addition to interfaces, some
   simple implementations of three interfaces: <code>IFileDelta</code>, 
   <code>IMessage</code>, and <code>IHelper</code>.
   These classes are provided for convenience and it is not required that
   they be used.
   <div class="compact">
      
<ol>
         
<li>
<code>FileDelta</code>
</li>
         
<li>
<code>Message</code>
</li>
         
<li>
<code>PassthruHelper</code>
</li>
      
</ol>
   
</div>

   The <code>FileDelta</code> class is provided as a simple way for frameworks to create
   the <code>IFileDelta[]</code> for the <code>IValidator</code>.

   <p>
   
<code>Message</code> is provided so that <code>IValidator</code>s can add validation messages,
   subtask messages, and <code>ValidationException</code> messages easily.</p>

   
<p>
   
<code>PassthruHelper</code> is provided for environments which already have a EMF 
   model loaded. Instead of implementing a loadModel method, the EMF object
   is set on the <code>PassthruHelper</code>, and its loadModel method merely returns the
   object with which it was initialized. Note that <code>IValidator</code>s are not permitted
   to assume that a <code>PassthruHelper</code> is being passed to them; an <code>IValidator</code>
   must pass in a unique name of a model into the loadModel method, so that
   <code>IHelper</code>s which are not <code>PassthruHelper</code>s can identify the type of model which
   needs to be loaded. The <code>PassthruHelper</code> can be used only in the WAS environment.</p>

</div>
</td>
</tr>
</tbody>
</table>
</div>
EOHTML;

$App->generatePage($theme, $Menu, $Nav, $pageAuthor, $pageKeywords, $pageTitle, $html);
?>