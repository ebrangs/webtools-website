<?php
require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";

$debug_count = 0;
$uniqueNames = array();
$uniqueCount = array();

$dbc 	= new DBConnectionBugs();
$dbh 	= $dbc->connect();
$rs = null;


function checkProject($projectNumber) {

    global $dbc;
    global $dbh;
    global $rs;
    global $debug_count;
    global $committerList;
    global $uniqueCount;
    global $uniqueNames;

    $buglist = array ();
    // Product IDs:
    //   20 Web Tools
    //   46 Java Server Faces
    //   47 Dali JPA Tools

    $sql_info = "SELECT  bugs.bug_id as bug_id,
		bugs.bug_status as bug_status,
		bugs.resolution as bug_resolution,
		bugs.target_milestone as bug_target_milestone,
		attachments.filename as filename,
		attachments.ispatch as ispatch,
        attachments.creation_ts as timestamp,
		profiles.login_name as attachment_login_name,
        profiles.realname as attachment_real_name,
        attach_data.thedata as thedata,
        bugs.keywords as bug_keywords
    FROM bugs,attachments,profiles,attach_data
      WHERE bugs.bug_id = '192785'
          AND char_length(bugs.keywords) > 0
          AND position('contributed' in bugs.keywords) > 0  
		  AND attachments.bug_id = bugs.bug_id
          AND attachments.ispatch = 1
          AND attachments.isobsolete = 0
          AND attachments.creation_ts > '2007-01-01 00:00:00'
          AND (bugs.bug_status = 'RESOLVED'
            OR bugs.bug_status = 'CLOSED'
            OR bugs.bug_status = 'VERIFIED')
          AND bugs.resolution = 'FIXED'
          AND bugs.product_id = $projectNumber
		  AND attachments.submitter_id = profiles.userid
          AND  attach_data.id = attachments.attach_id 
	  ORDER BY bugs.bug_id";


    //echo "sql_info: " . $sql_info . "<br />";
    //flush();

    $rs = mysql_query($sql_info, $dbh);

    echo "<table border='1' cellpadding='2' align='center' width='90%'>";
    echo "<tr><th>Count</th><th>Bug Number</th><th>Target Milestone</th><th>Id</th><th>Name</th><th>Total Lines</th><th>Added Lines</th><th>Keywords</th></tr>";

    while( ($debug_count < 1000) && ($myrow  = mysql_fetch_assoc($rs)) ) {
                echo "<tr>";
                $debug_count++;
                echo "<td>" . $debug_count . "</td>";
                //echo "   ";
                echo "<td>" . "<a href=\"https://bugs.eclipse.org/bugs/show_bug.cgi?id=" . $myrow['bug_id'] . "\">" . $myrow['bug_id'] . "</a>" . "</td>";
                $buglist[] = $myrow['bug_id'];
                //echo "   ";
                //echo $myrow['bug_status'];
                //echo ",";
                //echo $myrow['bug_resolution'];
                //echo ",";
                echo "<td>" . $myrow['bug_target_milestone'] . "</td>";
                //echo "   ";
                //echo $myrow['filename'];
                //echo ",";
                //echo $myrow['timestamp'];
                //echo ",";
                echo "<td>" . str_replace("@","{at}", $myrow['attachment_login_name']) . "</td>";
                //echo "   ";
                echo "<td>" . $myrow['attachment_real_name'] . "</td>";

                 echo "<td>" . "9999" . "</td>";
                 echo "<td>" . "9" . "</td>";
                 echo "<td>" . $myrow['bug_keywords'] . "</td>";
                 echo "</tr>";

         
    }
    echo "</table>";

    $cslist = "";
    foreach ($buglist as $bug) {
        $cslist = $cslist . $bug . ",";
    }
    echo "<p>Tip: You can use this <a href=\"https://bugs.eclipse.org/bugs/buglist.cgi?bug_id=" . $cslist ."\">bugzilla single list</a> for above table to first list all bugs in the table, and then narrow or sort the result how ever you would like.</p>";
}


ini_set("display_errors", "true");
error_reporting (E_ALL);

echo "<h2>test page to experiment with keywords</h2>";


echo "<h3>Web tools</h3>";
checkProject(20);

$dbc->disconnect();

$rs 		= null;
$dbh 		= null;
$dbc 		= null;

exit();
?>
