<?php
require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";

function test_connection( ) {
    $ds = ldap_connect();

    ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, 3);

    if ($ds) {
        $r = ldap_bind($ds); # anonymous bind
        if( $r ) {
            $sr=ldap_search($ds, "ou=people,dc=eclipse,dc=org", "(mail=david_williams@us.ibm.com)");
            $sr2=ldap_search($ds, "cn=webtools.sourceediting-dev,ou=group,dc=eclipse,dc=org", "(member=*)");

            $info = ldap_get_entries($ds, $sr);
            $info2 = ldap_get_entries($ds, $sr2);

            $uid = "DOESNOTEXIST";

            echo "<br />";
		    echo "info1 <br />";
            echo "<br />";
		    $i = 0;
            for ($ii=0; $ii<$info[$i]["count"]; $ii++){
                $data = $info[$i][$ii];
                echo $data."($i.$ii):&nbsp;&nbsp;".$info[$i][$data][0]."<br>";
                if( $data == "uid" ) {
                    $uid = $info[$i][$data][0];
                }
            }

            echo "<br />";
		    echo "info2 <br />";
            echo "<br />";
            $i = 0;
            for ($ii=0; $ii<$info2[$i]["count"]; $ii++){
                $data = $info2[$i][$ii];
                echo $data."($i.$ii):&nbsp;&nbsp;".$info2[$i][$data][0]."<br>";
                if( $data == "member" ) {
                    echo "in member<br>";
                    for($j = 0; $j<$info2[$i][$data]["count"]; $j++ ) {
                        $attr = $info2[$i][$data][$j];
                        echo "&nbsp;&nbsp;&nbsp;". $attr . "<br>";
                        if( strstr( $attr, $uid ) ) {
                            echo "FOUND<br>";
                        }
                    }
                }
            }


        }
        else {
            echo "did not access<br />";
            echo "ldap_result: " . ldap_error($ds) . "<br />";
        }
    }
    ldap_close($ds);
}

ini_set("display_errors", "true");
error_reporting (E_ALL);
echo "Committers Query Test Page <br />";
test_connection();

exit();
?>
