<?php

ini_set("display_errors", "true");
error_reporting (E_ALL);

include("standardStuff.php");

standardHTMLHeader("WTP IP Tools");

?>
<h1>WTP IP Tools</h1>
<p>These are some handy links to help manage and prepare IP Information for WTP</p>
<p>Note: it would be best to not link to this page, directly, from other web content, so that "blind" gophers, indexers,
etc., will not drill down, causing needless bugzilla queries, etc.</p>
<h2>Explore contributions</h2>
<p>Project Leads, and others, should examine these <a href="contributionExplore.php">potential contributions</a> from
time to time to see if any bugzillas should be marked with 'contributed' keyword.</p>
<h2>Contributed</h2>
<p>From time to time, run this <a href="contributed.php">contributed query</a>, and save the page to <code>/webtools/iprelated/contributions.html</code>
in order to have a static (unchanging) page for any particular IP Log.</p>
<h2>Committers</h2>
<p>From time to time, run this <a href="committerList.php">committers query</a>, and save the page to <code>/webtools/iprelated/committers.html</code>
in order to have a static (unchanging) page for any particular IP Log.</p>
<p>Use this <a href="notificationList.php">notification list</a> to produce a propeties file for ${BUILD_HOME}/notification/notifyMapper.properties</p>
<h2>The current IP Log</h2>
<p>See the <a href="http://www.eclipse.org/webtools/iprelated/ip_log.php">current IP Log</a></p>
<?php
standardHTMLFooter();
?>