<?php

ini_set("display_errors", "true");
error_reporting (E_ALL);

require_once "/home/data/httpd/eclipse-php-classes/system/dbconnection_bugs_ro.class.php";
include("standardStuff.php");

$committerList = array("Amy Wu","Angel Vera","15022010","Bradley Childs","Brian Vosburgh","Cameron Bateman","Carl Anderson","Chris Brealey","Chuck Bridgham","Craig Salter","David Lauzon","David M Williams","Der_Ping Chou","Dirk Le Roux","Gary Karasiuk","Gerry Kessler","Gorkem Ercan","Ian Trimble","Jason Sholl","John Lanuti","Kaloyan Raev","Karen Moore","Kate Price","Kathy Chan","Keith Chong","Konstantin Komissarchik","Lahiru Sandakith","Larry Dunnell","Larry Isaacs","Lawrence Mandel","Max Rydahl Andersen","Naci Dai","Neil Hauge","Nitin Dahyabhai","Paul Fullbright","Peter Moogk","Phil Berkland","Raghu Srinivasan","Richard Mah","Rick Sapir","Rob Frost","Shaun Smith","Sheila Sholars","Tim deBoer","Tran Le","Valentin Baciu", "kiril mitov", "Dimitar Giormov", "Dave Carver");
$webtoolsExcludeList = array("2.0.2 M202", "2.0.1 M201", "1.5.5 M155", "1.5.1 M151", "1.5.2 M152", "1.5.3 M153", "1.5.4 M154", "1.5.5 P", "2.0 RC0","2.0 RC1","2.0 RC2","2.0 RC3","2.0 RC4","2.0 M6","2.0 M5","2.0 M4","2.0");
$daliExcludeList = array("1.0.1");
$jsfExcludeList = array("2.0.1 M201", "2.0.2 M202", "2.0 RC0", "2.0 RC2", "2.0 RC1");

$debug_count = 0;
$uniqueNames = array();
$uniqueCount = array();

$dbc 	= new DBConnectionBugs();
$dbh 	= $dbc->connect();
$rs = null;

//
// David M Williams
// Tim deBoer
// Raghu Srinivasan
// Phil Berkland
// kiril mitov

function count_addedlines( $data ) {
    //$count = substr_count( $data, "\n+" );
    $count = preg_match_all ('/\n\+/', $data, $matches );
    return $count;
}
function count_newlines( $data ) {
    $count = substr_count( $data, "\n" );
    //$count = preg_match_all ('/\n\+/', $data, $matches );
    return $count;
}
function countNewLines($myrow) {

    $result = 0;
    $count1 = preg_match( '/patch/', $myrow['filename'] );
    if( $count1 > 0 ) {
        $result = count_newlines($myrow['thedata']);
    }
    else
    {
        $count2 = preg_match( '/RCS file:/', $myrow['thedata'] );
        if( $count2 > 0 ) {
            $result = count_newlines($myrow['thedata']);
        }
    }
    return $result;
}
function countAddedLines($myrow) {

    $result = 0;
    $count1 = preg_match( '/patch/', $myrow['filename'] );
    if( $count1 > 0 ) {
        $result = count_addedlines($myrow['thedata']);
    }
    else
    {
        $count2 = preg_match( '/RCS file:/', $myrow['thedata'] );
        if( $count2 > 0 ) {
            $result = count_addedlines($myrow['thedata']);
        }
    }
    return $result;
}
function checkProject($projectNumber, $excludes) {

    global $dbc;
    global $dbh;
    global $rs;
    global $debug_count;
    global $committerList;
    global $uniqueCount;
    global $uniqueNames;

    $buglist = array ();
    // Product IDs:
    //   20 Web Tools
    //   46 Java Server Faces
    //   47 Dali JPA Tools

    $sql_info = "SELECT  bugs.bug_id as bug_id,
		bugs.bug_status as bug_status,
		bugs.resolution as bug_resolution,
		bugs.target_milestone as bug_target_milestone,
		attachments.filename as filename,
		attachments.ispatch as ispatch,
        attachments.creation_ts as timestamp,
		profiles.login_name as attachment_login_name,
        profiles.realname as attachment_real_name,
        attach_data.thedata as thedata
    FROM bugs,attachments,profiles,attach_data
      WHERE attachments.bug_id = bugs.bug_id
          AND (char_length(bugs.keywords) = 0
            OR position('contributed' in bugs.keywords) = 0)  
          AND attachments.ispatch = 1
          AND attachments.isobsolete = 0
          AND attachments.creation_ts > '2007-01-01 00:00:00'
          AND (bugs.bug_status = 'RESOLVED'
            OR bugs.bug_status = 'CLOSED'
            OR bugs.bug_status = 'VERIFIED')
          AND bugs.resolution = 'FIXED'
          AND bugs.product_id = $projectNumber
		  AND attachments.submitter_id = profiles.userid
          AND  attach_data.id = attachments.attach_id 
	  ORDER BY bugs.bug_id";


    //echo "sql_info: " . $sql_info . "<br />";
    //flush();

    $rs = mysql_query($sql_info, $dbh);

    echo "<table border='1' cellpadding='2' align='center' width='90%'>";
    echo "<tr><th>Count</th><th>Bug Number</th><th>Target Milestone</th><th>Id</th><th>Name</th><th>Total Lines</th><th>Added Lines</th></tr>";

    while( ($debug_count < 1000) && ($myrow  = mysql_fetch_assoc($rs)) ) {
        if( !in_array($myrow['attachment_real_name'], $committerList ) ) {
            if (!in_array($myrow['bug_target_milestone'],$excludes)) {
                echo "<tr>";
                $debug_count++;
                echo "<td>" . $debug_count . "</td>";
                //echo "   ";
                echo "<td>" . "<a href=\"https://bugs.eclipse.org/bugs/show_bug.cgi?id=" . $myrow['bug_id'] . "\">" . $myrow['bug_id'] . "</a>" . "</td>";
                $buglist[] = $myrow['bug_id'];
                //echo "   ";
                //echo $myrow['bug_status'];
                //echo ",";
                //echo $myrow['bug_resolution'];
                //echo ",";
                echo "<td>" . $myrow['bug_target_milestone'] . "</td>";
                //echo "   ";
                //echo $myrow['filename'];
                //echo ",";
                //echo $myrow['timestamp'];
                //echo ",";
                echo "<td>" . str_replace("@","{at}", $myrow['attachment_login_name']) . "</td>";
                //echo "   ";
                echo "<td>" . $myrow['attachment_real_name'] . "</td>";

                if( !in_array($myrow['attachment_real_name'], $committerList ) ) {
                    // echo "NOT_WTP_COMMITTER";
                    if( !in_array($myrow['attachment_login_name'], $uniqueNames ) ) {
                        $uniqueNames[$myrow['attachment_login_name']] = $myrow['attachment_real_name'];
                        if (key_exists ($myrow['attachment_login_name'], $uniqueCount)) {
                            $uniqueCount[$myrow['attachment_login_name']] = $uniqueCount[$myrow['attachment_login_name']] + 1;
                        }
                        else {
                            $uniqueCount[$myrow['attachment_login_name']] = 1;
                        }
                    }
                }
                echo "<td>" . countNewLines($myrow) . "</td>";
                echo "<td>" . countAddedLines($myrow) . "</td>";
                echo "</tr>";
            }
        }

         
    }
    echo "</table>";

    $cslist = "";
    foreach ($buglist as $bug) {
        $cslist = $cslist . $bug . ",";
    }
    if (strlen($cslist) > 0) {
        echo "<p>Tip: You can use this <a href=\"https://bugs.eclipse.org/bugs/buglist.cgi?bug_id=" . $cslist ."\">bugzilla single list</a> for above table to first list all bugs in the table, and then narrow or sort the result how ever you would like.</p>";
    }
}

standardHTMLHeader("Explore Contributions");
echo "<h1>Potential Contributions</h1>";

echo "<p>These bugzilla entries are those marked fixed within this release, " .
"with a patch attached, from those that are not committers, " .
"and which have not been marked as 'contributed'. " .
"Please examine them to see if they should have the keywork 'contributed' added to them.</p>";
newLineNote();

echo "<p>Date of this Query: " . date(DATE_RFC822) . "</p>";

echo "<h3>Web tools</h3>";
checkProject(20, $webtoolsExcludeList);

echo "<h3>JSF</h3>";
checkProject(46, $jsfExcludeList);

echo "<h3>Dali</h3>";
checkProject(47, $daliExcludeList);

echo "<h3>WTP Incubator</h3>";
checkProject(117, array());

//echo "<h3>WTP ServerTools</h3>";
//checkProject(118, array());
//
//echo "<h3>WTP Common Tools</h3>";
//checkProject(119, array());
//
//echo "<h3>WTP Source Editing</h3>";
//checkProject(120, array());
//
//echo "<h3>WTP Webservices</h3>";
//checkProject(121, array());
//
//echo "<h3>WTP Java EE Tools</h3>";
//checkProject(122, array());
//
//echo "<h3>WTP EJB Tools</h3>";
//checkProject(123, array());
//echo "<h3>WTP Releng</h3>";
//checkProject(124, array());
//echo "<h3>WTP Website</h3>";
//checkProject(125, array());
//echo "<h3>WTP Datatools</h3>";
//checkProject(126, array());


$dbc->disconnect();

$rs 		= null;
$dbh 		= null;
$dbc 		= null;

echo "<h2>Summary, by Contributors</h2>";
echo "<table border='1' cellpadding='2' align='center' width='90%'>";
echo "<tr><th>Id</th><th>Name</th><th>Number of patches</th></tr>";
array_multisort($uniqueCount, SORT_DESC, SORT_NUMERIC);
foreach (array_keys($uniqueCount) as $key) {
     
    echo "<tr>";
    echo "<td>" . str_replace("@","{at}", $key) . "</td><td>" . $uniqueNames[$key] . "</td><td>" . $uniqueCount[$key] . "</td>";
    echo "</tr>";
}
echo "</table>";
standardHTMLFooter();
?>
