<?php
ini_set("display_errors", "true");
error_reporting (E_ALL);
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/app.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/nav.class.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/eclipse.org-common/system/menu.class.php");


$Nav   = new Nav();




// ----------------------------------------------------------------------------
// PHOENIX STUFF

$App 	= new App();
$Menu 	= new Menu();
include($App->getProjectCommon());

$pageTitle 		= "Release Train Information - ";
$pageAuthor		= "Karl Matthias";

$release = 'Indigo';

$pageKeywords="utility";

ob_start();
?>

<div id="maincontent">
<div id="midcolumn">
<h1>Projects Self-Assigning as Part of the <?= $release ?> Release</h1>
<p>
<?php

$result = $App->eclipse_sql("
	SELECT * FROM ProjectInfo, ProjectInfoValues
		WHERE ProjectInfo.ProjectInfoID = ProjectInfoValues.ProjectInfoID
			AND MainKey = 'simultaneousrelease'
			AND SubKey = LOWER('$release')
			AND Value = 1
		ORDER BY ProjectID
");

echo "<table>";
while($row = mysql_fetch_object($result)) {
	echo "<tr><td>$row->ProjectID</td></tr>";
	var_dump($row);
}
echo "</table>";
?>
</p>
</div>
</div>

<?php
$html = ob_get_contents();
	ob_end_clean();

	# Generate the web page
	$App->generatePage($theme, $Menu, NULL , $pageAuthor, $pageKeywords, $pageTitle, $html);

?>