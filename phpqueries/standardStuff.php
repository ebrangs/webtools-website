<?php

function standardHTMLHeader ($title) {
    
    if (isset($title)) {
        $pagetitle = $title;
    }
    else {
        $pagetitle="WTP Page";
    }
    echo "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n";
    echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">\n";
    echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n";
    echo "<head>\n";
    echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=ISO-8859-1\" />\n";
    echo "<title>" . $pagetitle . " </title>\n";
    echo "</head>\n";
    echo "<body>\n";
}
function standardHTMLFooter () {
    echo "</body>\n";
    echo "</html>\n";
}
function newLineNote () {
    echo "<p>Note: the 'Total Lines' is literally the number of new line characters in the patch. The 'Added Lines' (often more significant when it comes to matters of IP) is the number of lines with a '+' in front of them, \n" .
    "which is the tell tale sign used by patch formats to indicate new lines in the patch. In both cases, take these numbers as quick approximations. There are a number of cases that are known to be \n" .
    "inaccurate with the current algorithm, such as cases were some lines are 'moved', thus resulting in a lot of '-' and +' signs, even though not that much as changed.</p> \n";

}

?>