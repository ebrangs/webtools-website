<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0" xmlns:xalan="http://xml.apache.org/xalan" exclude-result-prefixes="xalan">
<xsl:include href="wtp-common.xsl" />
<xsl:include href="news.xsl"/>
<!-- xsl:variable name="news" select="document('news.xml')/news" /-->
<xsl:preserve-space elements="span"/>
<xsl:variable name="root">
	<xsl:choose>
		<xsl:when test="/sections/meta[@name='root']/@content">
 			<xsl:value-of select="/sections/meta[@name='root']/@content"/>
 		</xsl:when>
 		<xsl:otherwise>
 			<xsl:text>../..</xsl:text>
 		</xsl:otherwise>
 	</xsl:choose>
 </xsl:variable>

<xsl:template match="/">
	<xsl:apply-templates mode="pageinfo">
      <xsl:with-param name="root" select="$root"/>
    </xsl:apply-templates>
</xsl:template>

<xsl:template match="sections" mode="pageinfo">
  <xsl:call-template name="page">
    <xsl:with-param name="element" select="."/>
    <xsl:with-param name="title" select="@title"/>
    <xsl:with-param name="subtitle" select="@subtitle"/>
    <xsl:with-param name="root" select="$root"/>
  </xsl:call-template>
</xsl:template>
<xsl:template match="sections">
            <br />
			<table border="0" cellpadding="2" cellspacing="5" width="100%">
  				<tbody>			
					<xsl:apply-templates select="." mode="body"/>
				</tbody>
			</table>
            <p></p>
            <p>Please see our <a href="http://www.eclipse.org/legal/privacy.html">privacy 
            policy</a> and website <a href="http://www.eclipse.org/legal/termsofuse.html">terms 
            of use</a>. For problems with the eclipse.org site, please contact the 
            <a href="mailto:webmaster@eclipse.org">webmaster</a> or read the 
            <a href="{$root}/webmaster/index.html">webmaster FAQ</a> for answers to common questions!</p>
</xsl:template>

<xsl:template match="h1" mode="banner">
			<table border="0" cellpadding="2" cellspacing="5" width="100%">
  				<tbody>			
    <tr>
      <td align="left" width="60%"><font class="indextop"><xsl:value-of select="."/></font><br />
      <font class="indexsub"><xsl:value-of select="/html/head/title"/></font></td>
      <td width="40%"> <img src="{$root}/images/Idea.jpg" align="middle" height="86" hspace="50" width="120" /></td>
    </tr>
    </tbody>
			</table>
</xsl:template>

<xsl:template match="h1" mode="body"/>

<xsl:template match="h2" mode="body">
    <tr>
      <td colspan="2" align="left" bgcolor="#0080c0" valign="top"><b>
      <font color="#ffffff" face="Arial,Helvetica"><xsl:apply-templates/></font></b></td>
    </tr>
</xsl:template>

<xsl:template match="h3" mode="body">
    <tr>
      <td align="right" valign="top"><img src="{$root}/images/Adarrow.gif" border="0" height="16" width="16" /></td>
      <td><xsl:apply-templates/></td>
    </tr>
</xsl:template>
<xsl:template match="sections" mode="body">
<table>
 <tr>
   <td valign="top">
     <xsl:apply-templates select="section[@class='main']" mode="body"/>
   </td>
   <td valign="top">
     <xsl:for-each select="section[@class='infobox' or @class='list']">
    <xsl:variable name="items">
      <xsl:apply-templates select="document(@source)/*" mode="listcreation"/>
      <xsl:apply-templates select="item" mode="body"/>
    </xsl:variable>
   <xsl:choose>
     <xsl:when test="@class='infobox'">
   <xsl:call-template name="infobox">
		<xsl:with-param name="title" select="@name"></xsl:with-param>
        <xsl:with-param name="root" select="$root"/>
		<!--xsl:with-param name="description" select="" /-->
		<xsl:with-param name="items" select="xalan:nodeset($items)" />
        <xsl:with-param name="icon" select="@icon"/>
        <xsl:with-param name="description" select="description"/>
	</xsl:call-template>
</xsl:when>
<xsl:when test="@class='list'">
list
</xsl:when>
</xsl:choose>
    <br />
    </xsl:for-each>
   </td>
 </tr>
</table>
</xsl:template>
<xsl:template match="section[@class='main']" mode="body">
<table width="100%">
  <tr>
    <td valign="top">
      <table BORDER="0" CELLSPACING="5" CELLPADDING="2" WIDTH="100%" >
        <tr> 
          <td ALIGN="LEFT" VALIGN="TOP" COLSPAN="2" BGCOLOR="#0080C0">
            <b><font color="#FFFFFF" face="Arial,Helvetica"><xsl:value-of select="attribute::name"/></font></b>
          </td>
        </tr>
      </table>
      <table border="0" cellspacing="5" cellpadding="2" width="100%">
        <tr>
	      <td valign="TOP"><xsl:apply-templates select="description" mode="body"/></td>
        </tr>
      </table>
      <table border="0" cellspacing="5" cellpadding="2" width="100%">
        <xsl:for-each select="section[not(@class)]">
          <xsl:if test="(position() mod 2)=1">
            <!-- xsl:apply-templates select="." mode="body"/-->
            <xsl:text disable-output-escaping="yes">
              &lt;tr&gt;
            </xsl:text>
          </xsl:if>
          <td align="right" valign="top" width="4%">
            <xsl:if test="@icon">
            <img border="0">
              <xsl:attribute name="src">
                <xsl:value-of select="attribute::icon" />
              </xsl:attribute>
            </img>
            </xsl:if>
          </td>
          <td valign="top" width="46%">
            <b>
              <xsl:choose>
                <xsl:when test="@link">
                  <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="attribute::link" />
                  </xsl:attribute>
                  <xsl:value-of select="attribute::name"/>
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="attribute::name"/>
                </xsl:otherwise>
              </xsl:choose>
            </b>
            <br />
            <xsl:apply-templates select="*|@*|text()" mode="body"/>
          </td>
          <xsl:if test="(position() mod 2)=0">
            <xsl:text disable-output-escaping="yes">
              &lt;/tr&gt;
            </xsl:text>
          </xsl:if>
          <xsl:if test="(position()=last()) and ((position() mod 2)=1)">
            <xsl:text disable-output-escaping="yes">
              &lt;td width="4%"&gt;&lt;/td&gt;
              &lt;td width="46%"&gt;&lt;/td&gt;
              &lt;/tr&gt;
            </xsl:text>
          </xsl:if>
        </xsl:for-each>
      </table>
      <table border="0" cellspacing="5" cellpadding="2" width="100%">
        <xsl:for-each select="doublesection[not(@class)]">
          <tr>
          <td align="right" valign="top" width="4%">
            <xsl:if test="@icon">
            <img border="0">
              <xsl:attribute name="src">
                <xsl:value-of select="attribute::icon" />
              </xsl:attribute>
            </img>
            </xsl:if>
          </td>
          <td valign="top" width="96%" colspan="3">
            <b>
              <xsl:choose>
                <xsl:when test="@link">
                  <a>
                  <xsl:attribute name="href">
                    <xsl:value-of select="attribute::link" />
                  </xsl:attribute>
                  <xsl:value-of select="attribute::name"/>
                  </a>
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="attribute::name"/>
                </xsl:otherwise>
              </xsl:choose>
            </b>
            <br />
            <xsl:apply-templates select="*|@*|text()" mode="body"/>
          </td>
        </tr>
        </xsl:for-each>
      </table>
    </td>
    <td valign="top">
    <!-- Infoboxes -->
    
    
    <xsl:for-each select="section[@class='infobox' or @class='list']">
    <xsl:variable name="items">
      <xsl:apply-templates select="document(@source)/*" mode="listcreation"/>
      <xsl:apply-templates select="item" mode="body"/>
    </xsl:variable>
   <xsl:choose>
     <xsl:when test="@class='infobox'">
   <xsl:call-template name="infobox">
		<xsl:with-param name="title" select="@name"></xsl:with-param>
        <xsl:with-param name="root" select="$root"/>
		<!--xsl:with-param name="description" select="" /-->
		<xsl:with-param name="items" select="xalan:nodeset($items)" />
        <xsl:with-param name="icon" select="@icon"/>
	</xsl:call-template>
</xsl:when>
<xsl:when test="@class='list'">
list
</xsl:when>
</xsl:choose>
    <br />
    </xsl:for-each>
    </td>
  </tr>
</table>
</xsl:template>

<xsl:template match="description" mode="body">
<xsl:apply-templates select="*|text()" mode="body"/>
</xsl:template>

<xsl:template match="section" mode="body"/>

<xsl:template match="*|@*|text()">
	<!-- xsl:copy>
		<xsl:apply-templates select="*|@*|text()"/>
	</xsl:copy-->
</xsl:template>

</xsl:stylesheet>
